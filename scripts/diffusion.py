# 2015 June 30    Lisa Felberg
# calculate diffusion coeff in pore from traj file
#   time : input ps ;
#   position : input A ;
#   D_out : A^2/ps
import numpy as np
import matplotlib.pyplot as plt
import subprocess

moltype,x,y,z = [],[],[],[]
edge=100.0

"""
Distance function to calculate the distance between two points
"""
def distance(p1, p2, xyzmx):
    total = 0
    for i, (a, b) in enumerate(zip(p1, p2)):
        delta = abs(b - a)
        total += delta ** 2
    return total ** 0.5

"""
Function to plot trajectories of a ion in polymer
"""
def pltTraj(msd, outFile):
    fig = plt.figure(3, figsize = (7, 7)) #second arg sets dimensions
    ax = fig.add_subplot(1,1,1)
    
    for traj in range(len(msd)):
        if len(msd[traj]) < 120:
            ax.plot( np.arange( 0.0, 200*(len(msd[traj])), 200 ), msd[traj]) #np.arange( 0.0, 100.0*(len(msd[traj])-1), 100.0 )
    
    ax.set_xlabel('Time (ps)', fontsize=12)
    ax.set_ylabel('$| \Delta$r$ | $', fontsize=12)
    
    if outFile != None:
        plt.savefig(outFile+'.pdf',bbox_inches='tight')
        plt.close()

'''
retrieves data from an xyz file
'''
def getData(fileName, edge):
    init,ct =  [],0
    has_dt = -1
    lines = open(fileName).readlines()
    trajlen = len(lines)/3
    msd = np.zeros(trajlen)
    for line in lines:
        temp = line.split()
        if  ((has_dt == -1) and (temp[0] == 'Atom')):
            if (len(temp) == 1):
                has_dt = 0
            else:
                has_dt = 1
        
        if len(temp) == 5:
            if init == []:
                init = np.array( (float(temp[1]), float(temp[2]), float(temp[3])) )
            msd[ct] = distance(init, (float(temp[1]), float(temp[2]), float(temp[3])), edge)           
            ct+=1
    if (has_dt == 0):
        return(msd[:-1])
    else:
        return(msd)

shapes = ['cyl', 'pore', 'lam']
edges = [76.0 , 100.0,100.0]
charges = [150,250,350]
ions = ['hyd','zun','eig']
iondirs = ['','zundel','eigen']
dirnames=['01', '02', '03', '04', '05', '06', '07', '08', 
                         '09', '10']
output = 0

fileDir = '/Users/lfelberg/Desktop/pbsam_dat/'

for shp in range(len(shapes)):
    for chg in range(len(charges)):
        for ion in range(len(ions)):
            print shapes[shp]+"   "+str(charges[chg])+"  "+iondirs[ion]
            for dirs in range(len(dirnames)):
                output = 0
                trajFile =  fileDir+shapes[shp]+"/c"+str(charges[chg])+"/"+iondirs[ion]+"/"+dirnames[dirs]+ "/"
                command = "ls " + trajFile + "traj*.xyz > names.txt"
                try:
                    subprocess.check_output(command, shell = True,stderr=subprocess.STDOUT)
                    names_file = open('names.txt')
                    names = names_file.readlines()
                    subprocess.call(['rm','names.txt'])
                    msd = []
                    count = np.zeros(len(names))
                    for i in range(len(names)):
                        msd = msd + [getData(names[i][:-1], edges[shp])]
                except subprocess.CalledProcessError as e:
                    output = -1
           
            if output == 0:
                pltTraj(msd, fileDir+shapes[shp]+'_Q'+str(charges[chg])+'_'+ions[ion])
            
                
            
    
