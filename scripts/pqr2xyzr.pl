#!/usr/bin/perl -w
use strict;

my $atom = 1; 
my $atomname; # = "O";
my $aaname; # = "CEN";
my $aa, my $group;
my $charge, my $chain;
my $x, my $y, my $z, my $r;

open(READ, "$ARGV[0]") || die;

while(my $line = <READ>)
{

    ($atomname, $atom, $aaname, $group, $aa, $x, $y, $z, $charge, $r) = split(/\s+/, $line);
   
    printf "%8.3f %8.3f %8.3f %8.4f\n", $x, $y, $z, $r;

}
