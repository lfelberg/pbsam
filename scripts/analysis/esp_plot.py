import numpy as np
import matplotlib.pyplot as plt

'''
Program to plot a 2D version of the ESP from PB-SAM
'''

fileName='pot_z0.out'
outFile='pot_z0.pdf'
#fileName='/Users/lfelberg/Desktop/pot_z95.out'
#outFile='/Users/lfelberg/Desktop/lam_z95.pdf'

fact = 7.189; # converts to kT

#-----------------------------------------------------------------------

def FileOpen(fileName):
    lines = open(fileName).readlines()
    moltype,x,y,z = [],[],[],[]
    ct = 0
    first_line = 1

    for line in lines:
        temp = [float(x) for x in line.split()]
#        print temp
        if first_line == 1 :
            lx = temp[0]
            ly = temp[1]
            lz = temp[2]
            first_line = 0
        elif ct == 0:
            pot_arr = np.zeros((len(temp),len(temp)))
            pot_arr[ct] = temp
            ct+=1
        else :
            pot_arr[ct] = temp
            ct+=1

    boxl = [ lx, ly, lz ]

    return(pot_arr, boxl)

def dispPlot( boxl, count, potential, title = '', 
                xlab = r'$X \AA$', ylab = r'$Y \, (\AA)$',
                lege = '', outFile = None ):
    fig = plt.figure(1, figsize = (4, 4)); ax = fig.add_subplot(1,1,1)

    #X, Y = np.meshgrid(np.arange(0, 100, 1), np.arange(0, 100, 1))
    #levels = np.arange(-1,1,0.1)

    #plt.contourf(X, Y, potential, levels)
    plt.pcolor(potential, vmin=-.10, vmax=.10)
    plt.colorbar()

    xran = [0,boxl[0]]
    yran = [0,boxl[1]]
    if len(xran)!=0:
        ax.set_xlim(xran)
    if len(yran)!=0:
        ax.set_ylim(yran)
    plt.title(title, fontsize = 13);
    ax.set_ylabel(ylab, fontsize = 10); ax.set_xlabel(xlab, fontsize = 10)
    for tick in ax.xaxis.get_major_ticks():
        tick.label.set_fontsize(8)
    for tick in ax.yaxis.get_major_ticks():
        tick.label.set_fontsize(8)
    if outFile != None:
        plt.savefig(outFile,bbox_inches='tight')
    plt.close()

#--------------------------------------------------------------------------------
# main

system = FileOpen(fileName)
esp = system[0]
boxl = system[1]

esp*=fact
espT = np.transpose(esp)

dispPlot( boxl, len(esp[0]), espT, 
                xlab = r'$X \, (\AA)$', ylab = r'$Y \, (\AA)$',
                outFile=outFile)



