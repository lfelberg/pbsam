# 2015 June 17    MSoniat
# calculate diffusion coeff in pore from traj file
# Units :
#   time : input ps ;
#   position : input A ;
#   D_out : A^2/ps


import numpy as np

infile = open("traj_0.xyz","r")

# Initialize
# first 3 lines are initial position
infile.readline()
infile.readline()
pos_init = infile.readline()
pos_init = pos_init.split()
pos_init = np.array([ float(pos_init[1]) , float(pos_init[2]) , float(pos_init[3]) ])
list_dt = [0.0]
list_pos = [pos_init]
DT = 0
MSD = 0
DR2 = 0


# get data from traj file
for line in infile :
    line = line.split()
    if line[0] == "Atom" :
        dt = float(line[-1])
        DT += dt
        list_dt.append(DT)
    elif line[0] == "8" :
        pos_now = np.array([ float(line[1]) , float(line[2]) , float(line[3]) ])
        list_pos.append(pos_now)
    # ignore lines starting with 1

np.asarray(list_dt)
np.asarray(list_pos)

#print(len(list_dt), DT)
#print(list_dt[0:len(list_dt)])
list_tuple = []
list_DT = []
list_DR2 = []
list_DT2 = []
list_DR22 = []
list_varDT = []
list_varDR2 = []
members = len(list_dt)

# calculate dt and mean square distance for slices of the data
# calculation of variance not working correctly yet
#   -- should have very small varDT and large varDR2
for i in range(members, 1, -1) : # decrement by 1 until reaching 2
    DT = 0
    DT2 = 0
    DR2 = 0
    DR22 = 0
    slices = members+1-i
    for j in range(0, slices) :
        arr_dt = list_dt[0+j:i+j]
        arr_pos = list_pos[0+j:i+j]
        dt = arr_dt[-1] - arr_dt[0]
        dr2 = np.sum( np.square( arr_pos[-1] - arr_pos[0] ) )
        DT += dt
        DT2 += dt*dt
        DR2 += dr2
        DR22 += dr2*dr2
    list_DT.append(DT/slices)
    list_DR2.append(DR2/slices)
#    if slices != 1 :
#        list_varDT.append( slices*(DT2 - DT*DT)/(slices*(slices-1)) )
#        list_varDR2.append( slices*( DR22 - DR2*DR2 )/(slices*(slices-1)) )
#    elif slices == 1 :
#        list_varDT.append(0)
#        list_varDR2.append(0)


# fit data for diffusion coeff calc.
# 1D or 3D ??? assuming 3D for now
# using dt from 3 to 15 ns
#   -- should make this depend on the data itself
dim = 3

list_y = []
# add directory for i/o
# reverse order s.t. smallest dt is first
list_DT = list_DT[::-1]
list_DR2 = list_DR2[::-1]

fit = np.polyfit(list_DT[1:6], list_DR2[1:6], 1, full=True)
D_out = fit[0][0] / ( 2 * dim )     # units of A^2/ps
print(D_out)
print(fit)
for x in list_DT :
    y = fit[0][0] * x + fit[0][1]
    list_y.append(y)

# printing as text
col1 = list_DT
col2 = list_varDT
col3 = list_DR2
col4 = list_varDR2
col5 = list_y
output = '\n'.join('\t'.join(map(str,row)) for row in zip(col1,col3,col5))
with open('diffusion.txt', 'w') as f:
    f.write(output)

# for plots
'''
import matplotlib.pyplot as plt
from matplotlib.font_manager import FontProperties
from pylab import *

fig = plt.figure()
ax = fig.add_subplot(111)
fontP = FontProperties()
fontP.set_size(18)

line_data = ax.plot(list_DT, list_DR2, 'x', color='r', label="data")
line_fit = ax.plot(list_DT[1:6], list_y[1:6], '-', color='b', label="fit")

ax = plt.gca()
gcf().subplots_adjust(bottom=0.20, left=0.20)
for tick in ax.xaxis.get_major_ticks():
  tick.label.set_fontsize(18)
for tick in ax.yaxis.get_major_ticks():
  tick.label.set_fontsize(18)

ax.legend(prop=fontP)
ax.set_xlabel('dt', fontsize=24)
ax.set_ylabel('$(\Delta$r$)^2$', fontsize=24)
ax.set_title('')

plt.setp(ax.get_xticklabels())    #, rotation=-50)
#plt.show()
plt.savefig("diffusion.pdf")
'''

exit()

