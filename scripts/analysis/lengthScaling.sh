#!/bin/bash

rm timeList.out

runs=$( ls -d Run* | wc | awk '{print $1}' )
for ((j=0;j<=${runs};j++)) ; do
  max=$( ls Run${j}/traj_*.xyz | wc | awk '{print $1}' )
  for ((i=0;i<$max;i++)) ; do
    grep " 8 " Run${j}/traj_${i}.xyz | awk '{print $4}' > Run${j}/t${i}.zlist
    python lengthScaling.py Run${j}/t${i}.zlist >> timeList.out
  done
done

awk '{print $1}' < timeList.out | cut -d"[" -f2 | cut -d"," -f1 > d10.txt
for ((k=2;k<10;k++)) ; do
    kk=`expr 10 \* $k`
    awk -v c=$k '{print $c}' < timeList.out | cut -d"," -f1 > d${kk}.txt
done
awk '{print $10}' < timeList.out | cut -d"]" -f1 > d100.txt
awk '{print $11}' < timeList.out | cut -d")" -f1 > d102.txt
