
# 2015 June 30 Lisa Felberg, using log norm calcs from MSoniat
# statistics based on log-normal distribution
#

import math
import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import lognorm
from matplotlib.backends.backend_pdf import PdfPages


def getData(fileName):
    '''
    retrieves data from a manually formatted .dat file 
    '''
    lines = open(fileName).readlines()
    mfpt = np.zeros(1)
    for line in lines:
            vals = line.split()
            if ((float(vals[6]) < 5.99) and (vals[3] != 'stuck')):
                mfpt = np.append(mfpt, float(vals[6]))
    return(mfpt)
    
def logNormCalc(ln_data,outFile):
    mu = np.mean(ln_data)
    scale_param = math.exp(mu)
    var = np.var(ln_data)
    s = math.sqrt(var)    # aka sigma, shape_param
    #print(mu,scale_param,var,s)
    
    # Calculate a few first moments:
    mean, var, skew, kurt = lognorm.stats(s, scale = scale_param, moments='mvsk')
    print("%s \t  %6.4f \t  %6.4f \t  %d" % (outFile,mean,math.sqrt(var), len(ln_data)) ) # ,skew,kurt)
    
    return(scale_param,s)

def PlotMFPT(mfpt, scale_par, shape_par, outFile):
    # For plotting
    fig, ax = plt.subplots(1, 1)
    
    # Histogram of data
    width=0.1
    #n, bins, patches = P.hist(mfpt, 50, normed=1, histtype='stepfilled', label='raw data')
    n, bins = np.histogram(mfpt, bins=np.arange(0.0,10.0, width))
    n = n/(float(sum(n))*width)
       
    # Display the probability density function (pdf):
    x = np.arange(0.0,10-width, width)
    
    ax.bar(x,n, width, label='Data')
    x = np.arange(0.0,9.8, width/5.0)
    ax.plot(x, lognorm.pdf(x, shape_par, scale=scale_par), 'r-', lw=3, alpha=0.6, label='LogNorm PDF')
    plt.legend()
    
    #ax.set_ylim(0,0.2)
    ax.set_xlim(0,6)
    plt.title(outFile,fontsize = 12);
    if outFile != None:
        plt.savefig(outFile+'.pdf',bbox_inches='tight')
        plt.close()

shapes = ['pore', 'cyl', 'lam'] #, 'pore']
charges =  ['diff'] # [150,250,350] #
ions = ['hyd','zun','eig']

fileDir = '/Users/lfelberg/Desktop/pbsam_dat/'

print( 'File \t\t\t\t\t Mean \t Sigma: \t Nstep: ')

for shp in range(len(shapes)):
    for chg in range(len(charges)):
        for ion in range(len(ions)):
            filname = shapes[shp]+'_diff'+'_'+ions[ion]+'_complete'
            #filname = shapes[shp]+'_Q' + str(charges[chg])+'_'+ions[ion]+'_complete'
            fname = fileDir+filname
            pass_time = getData(fname+'.dat')
            if len(pass_time)  == 1:
                print ("%s: \t %5.3f" % (filname, pass_time[0]))
            else:
                pass_time = pass_time[1:]
                parms = logNormCalc(np.log(pass_time), filname)
                PlotMFPT(pass_time, parms[0], parms[1], fname)


