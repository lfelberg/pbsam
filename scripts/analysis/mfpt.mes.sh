#!/bin/bash

#module load mkl
#module load python/3.4-anaconda

#---------------------------------------------------------------------------------
# Variables

setupdir="$PWD"
startdir="/project/projectdirs/m1876/MES/JCAP/Setup/Pore/BulkQ"
#startdir="$PWD"
echo "Files will be in the directory $startdir"

syst="cyl"      # system type options: cyl, pch, lam
ncyl=1          # number of cylinders ; choose 1 or 9
distrib="Rand"  # charge distribution
charge="Pos"    # sign of charge
samples=1       # number of random distribution samples
smrad=0         # radius of channels, only necessary for syst="pch"

run_start=1     # runID for starting run
runs=4        # number of simultaneous BD runs

rad_min=10      # cylinder radius, pore radius, or lamellar spacing in Angstroms
rad_max=10
rad_incr=5

nq_min=100       # number of charges, or larger "atoms"
nq_max=100
nq_incr=5

pole_min=30     # number of multipoles
pole_max=30
pole_incr=10

salt_min=0.10   # salt concentration in molarity
salt_max=0.10
salt_incr=0.25

tolsp=4.5       # sphere tolerance - max in Angstroms
tolip=1         # sphere tolerance - min
makecontact=0   # 1 ==> makecontacts TRUE ; 0 ==> makecontacts FALSE
contactfile="dummy.pqr" # contact pqr file if makecontact = 1



#--------------------------------------------------------------------
# Salt concenration increment conversion

min=$(echo $(bc<<<"$salt_min * 10000"))
max=$(echo $(bc<<<"$salt_max * 10000"))
inc=$(echo $(bc<<<"$salt_incr * 10000"))
min=$(printf "%.0f" $min)
max=$(printf "%.0f" $max)
inc=$(printf "%.0f" $inc)


#--------------------------------------------------------------------------------------
# main loop

for ((rad=rad_min;rad<=rad_max;rad+=rad_incr)); do
    for ((q=nq_min;q<=nq_max;q+=nq_incr)) ; do
        for ((d=1;d<=${samples};d++)) ; do

            sphdir="${startdir}/R${rad}/${charge}/Q${q}/${distrib}${d}"
            setup="R${rad}_${charge}_Q${q}_${distrib}${d}"
            runname="${syst}_${setup}_max${tolsp}"

            for ((p=pole_min;p<=pole_max;p+=pole_incr)) ; do
                for ((s=min;s<=max;s+=inc)) ; do
                    salt=$(echo $(bc<<<"scale=4; $s / 10000"))

                    moldir="${startdir}/R${rad}/${charge}/Q${q}/${distrib}${d}/P${p}/S0${salt}"
                    matdir="${moldir}/Imat"
                    poldir="${moldir}/Selfpol"

                    bicarbdir="${moldir}/Exp/Bicarbonate"
                    carbdir="${moldir}/Exp/Carbonate"
                    formdir="${moldir}/Exp/Formate"
                    methdir="${moldir}/NoES/Methanol"

                    for ((run=run_start;run<=run_start+runs;run++)) ; do
#                        echo $bicarbdir/Run$run
#                        cat $bicarbdir/Run$run/complete_traj.run1.dat
#                        echo $carbdir/Run${run}
#                        cat $carbdir/Run${run}/complete_traj.run1.dat
#                        echo $formdir/Run$run
#                        cat $formdir/Run$run/complete_traj.run1.dat
#                        echo $methdir/Run$run
                        cat $methdir/Run$run/complete_traj.run1.dat
                    done

#                    cd ${setupdir}

                done
            done
        done
    done
done

exit 0


