# 2015 Sept 25    Marielle Soniat
# This script re-analyzes trajectories
# for FPT when d = 10, 20, ... A
# (instead of d = L/2 which is when simulation stops running)
# generate file with list of z positions of solute by
# $ grep " 8 " traj_0.xyz | awk '{print $4}' > t0.zlist


import numpy as np
import sys

# get list of z(solute)
file = sys.argv[1]
zlist = np.genfromtxt(file)

finalTime = 0.002 * float(5*len(zlist))

bin_list = []
count_list = []
item_list = []
time_list = []
count = 0
bin = 10
for item in zlist :
    if item > float(bin) :
        bin_list.append(bin)
        count_list.append(count)
        item_list.append(item)
        bin += 10
    count += 1
for c in count_list :
  time_list.append( 0.002 * float(5*(c+1)) )

if len(time_list) == 10 :
    print(time_list,finalTime)

