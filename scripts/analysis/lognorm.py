# 2015 June 23  MSoniat
# statistics based on log-normal distribution
#

import numpy as np
import math
import sys
from scipy.stats import lognorm

file=sys.argv[1]
raw_data = np.genfromtxt(file)
#raw_data = np.genfromtxt("fpt.txt")
ln_data = np.log(raw_data)
#print(raw_data, ln_data)

mu = np.mean(ln_data)
scale_param = math.exp(mu)
var = np.var(ln_data)
shape_param = math.sqrt(var)    # aka sigma
print(mu,scale_param,var,shape_param)
print(math.exp(mu),math.exp(math.sqrt(var)))

# Calculate a few first moments:
mean, var, skew, kurt = lognorm.stats(s=shape_param, scale=scale_param, moments='mvsk')
print(mean,math.sqrt(var))  # ,skew,kurt)

# For plotting
import matplotlib.pyplot as plt
fig, ax = plt.subplots(1, 1)
import pylab as P

# Histogram of data
n, bins, patches = P.hist(raw_data, 50, normed=1, histtype='stepfilled', label='raw data')

# Display the probability density function (pdf):
x = np.linspace(0.01, np.max(raw_data))
ax.plot(x, lognorm.pdf(x, shape_param, scale=scale_param), 'r-', lw=5, alpha=0.6, label='lognorm pdf')

#ax.set_ylim(0,0.2)
ax.legend()
#plt.show()


