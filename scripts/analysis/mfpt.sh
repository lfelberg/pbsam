#!/bin/bash

#module load mkl
#module load python/3.4-anaconda

#---------------------------------------------------------------------------------
# Variables

setupdir="$PWD"
#startdir="/project/projectdirs/m1876/MES/JCAP/Setup/Pore/BulkQ"
startdir="/project/projectdirs/m1698/pbsam/nafion/lamellar/slim"
#startdir="/project/projectdirs/m1698/pbsam/nafion/eightpore_center/slim"
#startdir="$PWD"
echo "Files will be in the directory $startdir"

syst="lam"      # system type options: cyl, pch, lam
ncyl=1          # number of cylinders ; choose 1 or 9
distrib="Rand"  # charge distribution
charge="Pos"    # sign of charge

run_start=1     # runID for starting run
runs=10        # number of simultaneous BD runs

rad_min=10      # cylinder radius, pore radius, or lamellar spacing in Angstroms
rad_max=10
rad_incr=5

nq_min=150       # number of charges, or larger "atoms"
nq_max=350
nq_incr=100

pole_min=30     # number of multipoles
pole_max=30
pole_incr=10

salt_min=0.15   # salt concentration in molarity
salt_max=0.15
salt_incr=0.25

tolsp=4.5       # sphere tolerance - max in Angstroms

#--------------------------------------------------------------------
# Salt concenration increment conversion

min=$(echo $(bc<<<"$salt_min * 10000"))
max=$(echo $(bc<<<"$salt_max * 10000"))
inc=$(echo $(bc<<<"$salt_incr * 10000"))
min=$(printf "%.0f" $min)
max=$(printf "%.0f" $max)
inc=$(printf "%.0f" $inc)

#--------------------------------------------------------------------------------------
# main loop

#for ((rad=rad_min;rad<=rad_max;rad+=rad_incr)); do
    for ((q=$nq_min;q<=$nq_max;q+=$nq_incr)) ; do
	#for q in {$nq_min..$nq_max..$nq_incr} ; do

            #sphdir="${startdir}/R${rad}/${charge}/Q${q}/${distrib}${d}"
            sphdir="${startdir}/Q${q}/"
            setup="Q${q}"
            runname="${syst}_${setup}_max${tolsp}"

            for ((p=$pole_min;p<=$pole_max;p+=$pole_incr)) ; do
			#for p in {$pole_min..$pole_max..$pole_incr} ; do
                for ((s=$min;s<=$max;s+=$inc)) ; do
				#for s in {$min..$max..$inc} ; do
                    salt=$(echo $(bc<<<"scale=4; $s / 10000"))

                    moldir="${startdir}/Q${q}/P${p}/S0${salt}"
                    #moldir="${startdir}/R${rad}/${charge}/Q${q}/${distrib}${d}/P${p}/S0${salt}"
                    matdir="${moldir}/Imat"
                    poldir="${moldir}/Selfpol"

                    hyddir="${moldir}/BD"
                    zundir="${moldir}/BD/zundel"
                    eigdir="${moldir}/BD/eigen"
                    #bicarbdir="${moldir}/Exp/Bicarbonate"
                    #carbdir="${moldir}/Exp/Carbonate"
                    #formdir="${moldir}/Exp/Formate"
                    #methdir="${moldir}/Exp/Methanol"
				
					touch ${moldir}/BD/${syst}_Q${q}_hyd_complete.dat
					touch ${moldir}/BD/${syst}_Q${q}_zun_complete.dat
					touch ${moldir}/BD/${syst}_Q${q}_eig_complete.dat

                    for ((run=$run_start;run<=$runs;run++)) ; do
                    #for run in {$run_start..$runs} ; do
						digit=9
						if [ $run -le $digit ]
						then
							runnm="0$run"
						else
							runnm="$run"
						fi
                        echo $hyddir/$runnm
						cat $hyddir/$runnm/complete_traj.${syst}_i0${salt}_c${q}_hyd.dat >> ${moldir}/BD/${syst}_Q${q}_hyd_complete.dat
						#cat $hyddir/$runnm/complete_traj.${syst}_i0${salt}_hyd1.dat >> ${moldir}/BD/${syst}_Q${q}_hyd_complete.dat
                        echo $zundir/$runnm
						cat $zundir/$runnm/complete_traj.${syst}_i0${salt}_c${q}_zun.dat >> ${moldir}/BD/${syst}_Q${q}_zun_complete.dat
						#cat $zundir/$runnm/complete_traj.${syst}_i0${salt}_zun1.dat >> ${moldir}/BD/${syst}_Q${q}_zun_complete.dat
                        echo $eigdir/$runnm
						cat $eigdir/$runnm/complete_traj.${syst}_i0${salt}_c${q}_eig.dat >> ${moldir}/BD/${syst}_Q${q}_eig_complete.dat
						#cat $eigdir/$runnm/complete_traj.${syst}_i0${salt}_eig1.dat >> ${moldir}/BD/${syst}_Q${q}_eig_complete.dat
#                       echo $bicarbdir/Run$run
#                       cat $bicarbdir/Run$run/complete_traj.run1.dat
#                       echo $carbdir/Run$run
#                       cat $carbdir/Run$run/complete_traj.run1.dat
#                       echo $formdir/Run$run
#                       cat $formdir/Run$run/complete_traj.run1.dat
#                       echo $methdir/Run$run
#                       cat $methdir/Run$run/complete_traj.run1.dat
                    done

#                    cd ${setupdir}
                done
            done
        done
    #done
#done

exit 0


