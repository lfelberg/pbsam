import numpy as np
import matplotlib.pyplot as plt
import heapq

fileName='/Users/lfelberg/Downloads/hp_water/hp_water_data/waterp_'

frange = range(140,163)
edge, Nat=9.8652, 97
Nhyd = 2*(Nat - 1)/3 + 1

"""
Distance function to calculate the distance between two points
with periodic bcs
"""
def distance(p1, p2, xyzmx):
    total = 0.0
    for i in range(len(p1)):
        delta = abs(p1[i] - p2[i])
        delta -= xyzmx*round(delta/xyzmx)
        total += pow(delta, 2.0)
    return pow(total,0.5)

"""
Open a *movie file and return atom type, xyz coords
"""
def FileOpen(fileName):
    lines = open(fileName).readlines()
    moltype,x,y,z = [],[],[],[]
    
    for line in lines:
        temp = line.split()
        moltype.append(int(temp[3]))
        x.append(float(temp[0]))
        y.append(float(temp[1]))
        z.append(float(temp[2]))
       
    return(moltype, x,y,z)


"""
MAIN code section
"""
clus_max = 5
allfiles = []

for cluster in range(clus_max):
    temp = open('test'+str(cluster)+'.xyz', 'w')
    allfiles.append(temp)
    

for i in range(len(frange)):
    attype, xx, yy, zz = FileOpen(fileName + str(frange[i]) + '.movie')
    nsteps = len(attype)/Nat

    for stp in range(nsteps):
        hydronium, hydrogen_pos, oxygen_pos = -1, [], []
        atstep = attype[stp*Nat:(stp+1)*Nat]
        xstep = xx[stp*Nat:(stp+1)*Nat]
        ystep = yy[stp*Nat:(stp+1)*Nat]
        zstep = zz[stp*Nat:(stp+1)*Nat]
        obond = np.zeros(Nhyd)
        for Hat in range(Nhyd):
            dist = []
            for Oat in range(Nat - Nhyd):
                 pO = (xstep[Oat], ystep[Oat], zstep[Oat] )
                 pH = (xstep[Nat - Nhyd+Hat], ystep[Nat - Nhyd+Hat], zstep[Nat - Nhyd+Hat] )
                 dist.append(distance(pO, pH, edge))
                 if Hat == 0:
                     oxygen_pos.append(pO)                       
            # Identifying the excess proton and associated oxygen
            obond[Hat] = dist.index(min(dist))
            hydrogen_pos.append(pH)
            
        for Oat in range(Nat - Nhyd):
            nhy_o = (obond == Oat).sum()
            if nhy_o == 3:
                # this is the extra proton water!
                hydronium = Oat
                pHyd = (xstep[hydronium], ystep[hydronium], zstep[hydronium] )
                hyd_h_pos=[]
                for x,z in enumerate(obond):
                    if z == Oat:
                        hyd_h_pos.append(hydrogen_pos[x])
                break

        # calculate the distance between the hydronium and the other waters
        dist_OO = []
        if hydronium != -1:
            for Oat in range(Nat - Nhyd):  
                if Oat != hydronium:
                    pO = (xstep[Oat], ystep[Oat], zstep[Oat] )
                    dist_OO.append(distance(pO, pHyd, edge))
        closest_waters = heapq.nsmallest(clus_max, ((k, m) for m, k in enumerate(dist_OO)))
        
        for cluster in range(clus_max):
            
            # printing out the hydronium ion
            allfiles[cluster].write("%d\n" % (3*(cluster)+4))
            allfiles[cluster].write("Atom\n")
            allfiles[cluster].write("%2d  %7.4f  %7.4f  %7.4f \n"  % (8,  pHyd[0], pHyd[1], pHyd[2]))
            allfiles[cluster].write("%2d  %7.4f  %7.4f  %7.4f \n"  % (1,  hyd_h_pos[0][0], hyd_h_pos[0][1], hyd_h_pos[0][2]))                
            allfiles[cluster].write("%2d  %7.4f  %7.4f  %7.4f \n"  % (1,  hyd_h_pos[1][0], hyd_h_pos[1][1],hyd_h_pos[1][2]))                
            allfiles[cluster].write("%2d  %7.4f  %7.4f  %7.4f \n"  % (1,  hyd_h_pos[2][0], hyd_h_pos[2][1],hyd_h_pos[2][2])) 
                        
            for grp in range(cluster):
                clus_h_pos = []
                for x,z in enumerate(obond):
                    if z == closest_waters[grp][1]:
                        clus_h_pos.append(hydrogen_pos[x])        
                allfiles[cluster].write("%2d  %7.4f  %7.4f  %7.4f \n"  % (8,  oxygen_pos[closest_waters[grp][1]][0], 
                                                    oxygen_pos[closest_waters[grp][1]][1], oxygen_pos[closest_waters[grp][1]][2]))
                allfiles[cluster].write("%2d  %7.4f  %7.4f  %7.4f \n"  % (1,  clus_h_pos[0][0], clus_h_pos[0][1], clus_h_pos[0][2]))                
                allfiles[cluster].write("%2d  %7.4f  %7.4f  %7.4f \n"  % (1,  clus_h_pos[1][0], clus_h_pos[1][1],clus_h_pos[1][2]))   
        
        
        
for cluster in range(clus_max):
    allfiles[cluster].close()      
        
        