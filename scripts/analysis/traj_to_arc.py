#    2015 Nov    version 1.0
#    Richard Xu, Marielle Soniat
# uses Python 3.*
# converts traj_*.xyz output from PB-SAM
# to .arc files for Tinker diffuse.x


import numpy as np
import sys
import glob

boxl = 76.0
dir = "/project/projectdirs/m1876/MES/JCAP/Setup/"
dir = dir+"Pore/BulkQ/R10/Pos/Q10/Rand1/P30/S0.1000/Exp/Methanol/Run*/"
dt = 2    # ps
writeFrq = 5
dt = dt * writeFrq


def traj_to_arc(outputfile):
    outputfile = open(outputfile, 'w')
    inputfiles = []
    for xyz in glob.glob(dir+"*.xyz"):
        inputfiles.append(open(xyz, 'r').readlines()[2::3])    # reads every third line
    length = str(len(inputfiles))    # length = number of traj files

# Find minimum traj length
    min = 999999999
    for item in inputfiles :
        if len(item) < min :
            min = len(item)

# Make all traj the same length
    steps = min
    print(steps)
    longtraj = []
    for item in inputfiles :
        longtraj.append(item[0:steps])
    inputfiles = longtraj
    length = str(len(inputfiles))
#    print(length)

#    dimensions = "    ",boxl,boxl,boxl,"90.000000   90.000000   90.000000\n"
#    dimensions = str(dimensions)

#    for index in range(len(inputfiles[0])):
    for index in range(0,steps) :
        outputfile.write(length + "\n")
        outputfile.write("    %3f  %3f  %3f  90.0    90.0   90.0\n" %(boxl,boxl,boxl) )
        i = 1
        for f in inputfiles:
            temp = f[index].split(' ')
            if len(temp) > 1 and temp[0] != 'Atom':
                # "     1  O     %1.6f    %1.6f    %1.6f     %1d     %1d     %1d"
                first = temp[5]
                second = temp[8]
                third = temp[11]

                outputfile.write("   %3d  O    %9s   %9s   %9s     1          \n"
                    % (i, first, second, third))
                i+=1


    # for lines in zip(inputfiles):
    #     for line_ in lines:
    #         for line in line_:
    #             temp = line.split(' ')
    #             temp = filter(None, temp)
    #             if len(temp) > 1 and temp[0] != 'Atom':
    #                 # "     1  O     %1.6f    %1.6f    %1.6f     %1d     %1d     %1d"
    #                 first = temp[1][:9]
    #                 while len(first) < 9:
    #                     first = first + "0"
    #                 second = temp[2][:9]
    #                 while len(second) < 9:
    #                     second = second + "0"
    #                 third = temp[3][:9]
    #                 while len(third) < 9:
    #                     third = third + "0"

    #                 outputfile.write("     1  O    %9s   %9s   %9s               \n"
    #                     % (first, second, third))

if __name__ == "__main__":
    outputfile = sys.argv[1]
    traj_to_arc(outputfile)







        # if len(temp) > 1 and temp[0] != 'Atom':
        #     # "     1  O     %1.6f    %1.6f    %1.6f     %1d     %1d     %1d"
        #     first = float(temp[1])
        #     if first > 0:
        #         prefirst = "     "
        #     else:
        #         prefirst = "    -"
        #     first = abs(first)
        #     second = float(temp[2])
        #     if second > 0:
        #         presecond = "    "
        #     else:
        #         presecond = "   -"
        #     second = abs(second)
        #     third = float(temp[3])
        #     if third > 0:
        #         prethird = "    "
        #     else:
        #         prethird = "   -"
        #     third = abs(third)

        #     outputfile.write("     1  O%5s%8f%4s%8f%4s%8f               \n"
        #         % (prefirst, first, presecond,second, prethird, third) )
