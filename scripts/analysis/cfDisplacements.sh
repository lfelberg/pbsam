#!/bin/bash


a=$(grep -n "sim time for this traj in NS" bdtest.out | tail -2 | head -1 | cut -d':' -f1 )
b=$(grep -n "sim time for this traj in NS" bdtest.out | tail -1 | cut -d':' -f1 )
sed -n "${a},${b}p" < bdtest.out > bdtest.tmp
grep "all" bdtest.tmp | awk '{print $6, $7, $8}' > dispES.out
grep "random" bdtest.tmp | awk '{print $3, $4, $5}' > dispRand.out
rm bdtest.tmp

