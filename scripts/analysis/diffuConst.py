# Marielle Soniat    2015 Sept v2
# calculate diffusion constant of solute in pore
# based on first passage time (FPT) histogram
#
# equation
# P(x,t)dx = ( 1 / sqrt( 4 * pi * D * t ) ) * exp( -X^2 / ( 4 * D * t ) ) * dx
# X_0 = half pore length
#      L = box length in Angstroms
# t = FPT in ps
# P(t) = probability of t
# D = diffusion constant
#      This will be treated as a parameter to be fit,
#      not solved for.
#
# output units in cm^2/s
#

import numpy as np
from scipy.optimize import curve_fit
from scipy.optimize import leastsq
import math


L = 76.0
x0 = 0.5 * L

# defines log(P(t))
def fit_func(D, maskedTime) :
    time = maskedTime
    return -0.5 * np.log( 4 * math.pi * D * time ) - ( x0*x0 / 4*D*time ) + math.log(x0)

# defines residual
def res(D, logProb, maskedTime) :
    time = maskedTime
    err = logProb - ( -0.5 * np.log( 4 * math.pi * D * time ) - ( x0*x0 / 4*D*time )  + math.log(x0))
    return err


# load in data
time, count = np.genfromtxt("histogramFPT.txt", skip_header=1, unpack=True)

# remove if count = 0 ( because log(0) = -inf )
toremove = []
for i in range(len(count)) :
    if ( int(count[i]) == 0 ) :
        toremove.append(i)

maskedCount = np.delete(count, toremove)
maskedTime = np.delete(time, toremove)
totalCount = np.sum(maskedCount)

# calculate probability from count, then take log
probability = maskedCount / totalCount
logProb = np.log(probability)
#print(logProb)

# least-squares fit to get D
D = 1.0    # initial guess
plsq = leastsq(res, D, args=(logProb, maskedTime))
#print(plsq)
D = plsq[0]

print(D)



