#!/bin/bash

thisdir=$(echo $PWD)

for ((i=1;i<=20;i++)) ; do

  mypath=/global/project/projectdirs/m1876/MES/JCAP/Setup/Pore/BulkQ/R10/Pos/Q10/Rand1/P30/S0.1000/Exp/Bicarb/Run${i}

  lastTraj=$(ls ${mypath}/traj_0.xyz)
  echo $lastTraj
  lastPos=$(tail -3 $lastTraj | grep " 8 " | awk '{print $2, $3, $4}' )
  echo $lastPos > $mypath/restart.xyz

  prevJob="$mypath/Start"
  mkdir $prevJob
  cp $mypath/bdtest.out $prevJob
  cp $mypath/comp*.dat $prevJob
  cp $mypath/initial.pqr $prevJob
  cp $mypath/jcap.o* $prevJob
  cp $mypath/run.inp $prevJob
  cp $mypath/traj*.xyz $prevJob

  sed 's/xyz 2 .*/xyz 2 restart.xyz/' < $prevJob/run.inp > $mypath/run.inp

  cd $mypath
  qsub qsub.bd
  cd $thisdir

done

