#!/bin/bash

# polymer variables
morphology="cyl"        # cyl, pch, or lam
radius=10
boxl=76
salt=0.1
charge=10
qtype="Pos"             # Pos, Neg, or NoQ  -- must use this capitalization
distrib="bulk"          # surf or bulk
smRad=5                 # only for pch
sample=1


# solute
# Options: Methanol, Carbonate, Bicarb, Formate
solute="Bicarb"


# system variables
runtype="sim"           # sim, pot, imat or spol
ntraj=1
maxtime=1000000000
pbc=3
runStart=81
runEnd=100


# probably leave as defaults
system="pem"
runname="pem"
poles=30
idiel=4.0
sdiel=78.0
omp=16
attypes=2
cutoff=1000.0
temp=353.0



#------------------------------------------------------

salt=$(echo $(bc<<<"$salt * 10000"))
salt=$(printf "%.0f" $salt)
salt=$(echo $(bc<<<"scale=4; $salt / 10000"))

#------------------------------------------------------


# set up polymer paths
startdir="/project/projectdirs/m1876/MES/JCAP/Setup"

if [ -n "$(echo $morphology | grep cyl)" ] ; then
  morph="Pore"
fi

figdir="$startdir/$morph"

if [ -n "$(echo $distrib | grep bulk)" ] ; then
    figdir="$figdir/BulkQ"
fi

figdir="$figdir/R${radius}/$qtype/Q${charge}/Rand${sample}"
polpqr=$(ls $figdir/${morphology}_*.pqr )

poldir="$figdir/P${poles}/S0${salt}"
polmat="$poldir/Imat"
polpol="$poldir/Selfpol"

polexp=$( ls $polpol/*.exp | head -1 )
end='.0.'
polexp=$(echo "$polexp" | sed -e 's/\.0\.*.*.exp/\.0/g')
polexp=$(echo "$polexp" | sed -e 's/.*Selfpol\///g')



#------------------------------------------------------

# look up solute paths

solpath="/project/projectdirs/m1876/MES/JCAP/Setup/Solutes/Molecules"
solxyz="$solpath/sol.xyz"

if [ -n "$(echo $solute | grep Methanol)" ] ; then
  diffu=0.17
  solpqr="$solpath/MeOH/Config/sol.pqr"
  solmat="$solpath/MeOH/Imat"
  solpol="$solpath/MeOH/Selfpol"
  solexp="sol_meoh.0"

elif [ -n "$(echo $solute | grep Formate)" ] ; then
  diffu=0.15
  solpqr="$solpath/HCOO/Config/sol.pqr"
  solmat="$solpath/HCOO/Imat"
  solpol="$solpath/HCOO/Selfpol"
  solexp="sol_hcoo_p20_s0.15.0"

elif [ -n "$(echo $solute | grep Bicarb)" ] ; then
  diffu=0.12
  solpqr="$solpath/HCO3/Config/sol.pqr"
  solmat="$solpath/HCO3/Imat"
  solpol="$solpath/HCO3/Selfpol"
  solexp="sol_hco3_p20_s0.15.0"

elif [ -n "$(echo $solute | grep Carbonate)" ] ; then
  diffu=0.09
  solpqr="$solpath/CO3/Config/sol.pqr"
  solmat="$solpath/CO3/Imat"
  solpol="$solpath/CO3/Selfpol"
  solexp="sol_co3_p20_s0.15.0"

fi


#------------------------------------------------------

echo "
system ${system}
runname ${runname}

omp ${omp}

runtype $runtype $ntraj $maxtime

salt $salt
temp $temp
idiel $idiel
sdiel $sdiel

pbc $pbc $boxl
cutoffs $cutoff $cutoff $cutoff
attypes $attypes

type 1 1 stat 0.0 0.0
xyz 1 $figdir
pqr 1 $polpqr
imat 1 $polmat
spolDir 1 $polpol
spolExt 1 $polexp


type 2 1 move $diffu 0.0
xyz 2 $solxyz
pqr 2 $solpqr
imat 2 $solmat
spolDir 2 $solpol
spolExt 2 $solexp


" > run.inp



expdir="$poldir/Exp/$solute"
mkdir $expdir

scriptdir=$(echo $PWD)

for ((i=${runStart};i<=${runEnd};i+=1)) ; do
  rundir="$expdir/Run${i}"
  mkdir $rundir
  echo $rundir
  cp run.inp $rundir
  cp qsub.bd $rundir
  cd $rundir
  qsub qsub.bd
  sleep 5

  cd $scriptdir
done
