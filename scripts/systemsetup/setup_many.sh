#!/bin/bash

#module load mkl
#module load python/3.4-anaconda

#---------------------------------------------------------------------------------
# Variables

setupdir="$PWD"
startdir="/project/projectdirs/m1876/MES/JCAP/Setup/Pore"
#startdir="$PWD"
echo "Files will be in the directory $startdir"
machine="hopper"
echo "Running with executables for ${machine}"

syst="cyl"	# system type options: cyl, pch, lam
ncyl=1		# number of cylinders ; choose 1 or 9
distrib="Rand"  # charge distribution
charge="Pos"	# sign of charge
samples=1	# number of random distribution samples
smrad=5		# radius of channels, only necessary for syst="pch"

rad_min=10 	# cylinder radius, pore radius, or lamellar spacing in Angstroms
rad_max=10
rad_incr=5

nq_min=100 	# number of charges, or larger "atoms"
nq_max=100
nq_incr=5

pole_min=60	# number of multipoles
pole_max=60
pole_incr=10

salt_min=0.10	# salt concentration in molarity
salt_max=0.10
salt_incr=0.25

tolsp=4.5	# sphere tolerance - max in Angstroms
tolip=1		# sphere tolerance - min
makecontact=0   # 1 ==> makecontacts TRUE ; 0 ==> makecontacts FALSE
contactfile="dummy.pqr"	# contact pqr file if makecontact = 1


#-----------------------------------------------------------------------------------------
# Functions to write qsub scripts


write_qsubMakesphere (  )
{
echo "#!/bin/bash" > ${sphdir}/qsub.makesphere
echo "#PBS -q serial" >> ${sphdir}/qsub.makesphere
echo "#PBS -l walltime=48:00:00" >> ${sphdir}/qsub.makesphere
echo "#PBS -j oe ${sphdir}/makesphere.out" >> ${sphdir}/qsub.makesphere
echo "#PBS -N sphere" >> ${sphdir}/qsub.makesphere
echo "" >> ${sphdir}/qsub.makesphere

echo "EXEC=\$PBSAMHOME/CODE_MOLSPHERE/bin/makesphere" >> ${sphdir}/qsub.makesphere
echo "MOLPATH=${sphdir}" >> ${sphdir}/qsub.makesphere
echo "TOLSP=${tolsp}" >> ${sphdir}/qsub.makesphere
echo "TOLIP=${tolip}" >> ${sphdir}/qsub.makesphere

echo "PQRFILE=${pqrfile}" >> ${sphdir}/qsub.makesphere
echo "VERTFILE=${vertfile}" >> ${sphdir}/qsub.makesphere
echo "CONTACTPQRFILE=${contactfile}" >> ${sphdir}/qsub.makesphere
echo "RUNNAME=${runname}" >> ${sphdir}/qsub.makesphere
echo "OUTCENTERFILE=\$MOLPATH/centers_${runname}.cen" >> ${sphdir}/qsub.makesphere
echo "LOGFILE=out_${runname}.log" >> ${sphdir}/qsub.makesphere
echo "" >> ${sphdir}/qsub.makesphere

echo "\$EXEC \$PQRFILE \$VERTFILE \$OUTCENTERFILE \$TOLSP ${makecontact} \$TOLIP \$CONTACTPQRFILE > \$MOLPATH/\$LOGFILE" >> ${sphdir}/qsub.makesphere
echo "\$PBSAMSCRIPT/cen2pqr.pl \$OUTCENTERFILE > \$MOLPATH/centers_\$RUNNAME.pqr" >> ${sphdir}/qsub.makesphere
echo "cat \$MOLPATH/centers_\$RUNNAME.pqr \$PQRFILE > \$MOLPATH/\$RUNNAME.pqr" >> ${sphdir}/qsub.makesphere
}


write_qsubImat ()
{
echo "#!/bin/bash -l" > ${matdir}/qsub.imat
echo "#PBS -q debug" >> ${matdir}/qsub.imat
echo "#PBS -l walltime=00:30:00" >> ${matdir}/qsub.imat
echo "#PBS -l mppwidth=16" >> ${matdir}/qsub.imat
echo "#PBS -j oe ${matdir}/makemat.out" >> ${matdir}/qsub.imat
echo "#PBS -N imat" >> ${matdir}/qsub.imat
echo "" >> ${matdir}/qsub.imat

#echo "module load mkl" >> ${matdir}/qsub.imat
#echo "module swap PrgEnv-intel PrgEnv-gnu" >> ${matdir}/qsub.imat
echo "cd \$PBS_O_WORKDIR" >> ${matdir}/qsub.imat

echo "export OMP_NUM_THREADS=16" >> ${matdir}/qsub.imat
echo "THREADS=16" >> ${matdir}/qsub.imat
echo "" >> ${matdir}/qsub.imat

echo "EXEC=\$PBSAMHOME/CODE_PBSAM/bin/pbsam_${machine}_p${p}" >> ${matdir}/qsub.imat
echo "SALT_CONC=${salt}" >> ${matdir}/qsub.imat
echo "PQRCENFILE=${sphdir}/${runname}.pqr" >> ${matdir}/qsub.imat
echo "LOGFILE=${matdir}/makeMat_solute.log" >> ${matdir}/qsub.imat
echo "aprun -n 1 -N 1 -d 16 \$EXEC mat \$THREADS \$SALT_CONC  \$PQRCENFILE > \$LOGFILE" >> ${matdir}/qsub.imat
}

write_qsubSelfpol ()
{
echo "#!/bin/bash -l" > ${poldir}/qsub.selfpol
echo "#PBS -q regular" >> ${poldir}/qsub.selfpol
echo "#PBS -l walltime=10:30:00" >> ${poldir}/qsub.selfpol
echo "#PBS -j oe ${poldir}/selfpol.out" >> ${poldir}/qsub.selfpol
echo "#PBS -l mppwidth=16" >> ${poldir}/qsub.selfpol
echo "#PBS -N selfpol" >> ${poldir}/qsub.selfpol
echo "" >> ${poldir}/qsub.selfpol

#echo "module load mkl" >> ${poldir}/qsub.selfpol
#echo "module swap PrgEnv-intel PrgEnv-gnu" >> ${poldir}/qsub.selfpol
echo "cd \$PBS_O_WORKDIR" >> ${poldir}/qsub.selfpol
echo "" >> ${poldir}/qsub.selfpol

echo "SALT_CONC=${salt}" >> ${poldir}/qsub.selfpol
echo "THREAD=16" >> ${poldir}/qsub.selfpol
echo "" >> ${poldir}/qsub.selfpol

echo "EXEC=\$PBSAMHOME/CODE_PBSAM/bin/pbsam_${machine}_p${p}" >> ${poldir}/qsub.selfpol
echo "IMATPATH=${matdir}" >> ${poldir}/qsub.selfpol
echo "PQRFILE=${sphdir}/${runname}.pqr" >> ${poldir}/qsub.selfpol
echo "LOGFILE=${poldir}/selfpol_${runname}.log" >> ${poldir}/qsub.selfpol
echo "aprun -n 1 -N 1 -d 16 \$EXEC spol \$THREAD \$SALT_CONC \$PQRFILE \$IMATPATH ${runname} > \$LOGFILE 2>&1" >> ${poldir}/qsub.selfpol
}



#------------------------------------------------------------------------------
# Input check

if (( $makecontact == 0 )) ; then
  echo "No contacts."
elif (( $makecontact == 1 )) ; then
  echo "Contact will be made with ${contactfile}"
else
  echo "Not a valid makecontact option. Choose 1 for makecontact OR 0 for no makecontact."
  exit 1
fi


#--------------------------------------------------------------------------------------
# Salt concenration increment conversion

min=$(echo $(bc<<<"$salt_min * 10000"))
max=$(echo $(bc<<<"$salt_max * 10000"))
inc=$(echo $(bc<<<"$salt_incr * 10000"))
min=$(printf "%.0f" $min)
max=$(printf "%.0f" $max)
inc=$(printf "%.0f" $inc)


#--------------------------------------------------------------------------------------
# main loop

for ((rad=rad_min;rad<=rad_max;rad+=rad_incr)); do

  mkdir ${startdir}/R${rad}

  for ((q=nq_min;q<=nq_max;q+=nq_incr)) ; do

    mkdir ${startdir}/R${rad}/${charge}
    mkdir ${startdir}/R${rad}/${charge}/Q${q}

    for ((d=1;d<=${samples};d++)) ; do

      mkdir ${startdir}/R${rad}/${charge}/Q${q}/${distrib}${d}
      sphdir="${startdir}/R${rad}/${charge}/Q${q}/${distrib}${d}"

      setup="R${rad}_${charge}_Q${q}_${distrib}${d}"
      runname="${syst}_${setup}_max${tolsp}"

      #--------------------------------------------------------------
      # to make the pore and run coarse graining
<<COMMENT1

      python make_cy_lam_pore.py ${syst} ${q} ${rad} ${smrad} ${ncyl}

      pqrfile="${sphdir}/${syst}.pqr"
      vertfile="${sphdir}/${syst}.std.vert"
      cp ${syst}.pqr $pqrfile
      awk '{print $6, $7, $8, $10}' < ${syst}.pqr > ${sphdir}/${syst}.xyzr
      /global/project/projectdirs/m1876/MES/MSMS/msms.x86_64Linux2.2.6.1 -if ${sphdir}/${syst}.xyzr -probe_radius 1.5 -density 3.0 -hdensity 3.0 -no_area -of ${sphdir}/${syst}.std
      # typical probe_radius 1.5, density 3.0

      write_qsubMakesphere
      cd ${sphdir}
      JOB1=$( qsub.serial ${sphdir}/qsub.makesphere )
      echo $JOB1
      sleep 5
      cd ${setupdir}

COMMENT1
      # finish CG
      #--------------------------------------------------------------
      # for Imat and Selfpol
#<<COMMENT2

      for ((p=pole_min;p<=pole_max;p+=pole_incr)) ; do

        mkdir ${startdir}/R${rad}/${charge}/Q${q}/${distrib}${d}/P${p}

        for ((s=min;s<=max;s+=inc)) ; do

          salt=$(echo $(bc<<<"scale=4; $s / 10000"))

          moldir="${startdir}/R${rad}/${charge}/Q${q}/${distrib}${d}/P${p}/S0${salt}"
          echo "Directory: ${moldir}"
          mkdir ${moldir}
          matdir="${moldir}/Imat"
          mkdir ${matdir}
          poldir="${moldir}/Selfpol"
          mkdir ${poldir}

          write_qsubImat
          cd ${matdir}
          JOB2=$( qsub ${matdir}/qsub.imat )
          echo $JOB2
          sleep 5

          write_qsubSelfpol
          cd ${poldir}
          JOB3=$( qsub -W depend=afterok:$JOB2 ${poldir}/qsub.selfpol )
          echo $JOB3
          sleep 5

          cd ${setupdir}

        done
      done

#COMMENT2
      # finish Imat and Selfpol loops
      #--------------------------------------------------------------
    done
  done
done

#-------------------------------------------------------------------------------------

exit 0
