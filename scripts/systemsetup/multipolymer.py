import numpy as np
import sys

""" Program to make a idealized model of nafion """
# requires Numpy 1.7.0 or later
# 	for np.random.choice
# print statements are in Python 3.x.x format


# To do:
#       positive charge groups instead of sulfonate
#       clustered and even charge distributions



#sys_type = sys.argv[1]  # Choices: lam, pore, cyl
#total_charge = float(sys.argv[2])
#radius = float(sys.argv[3])
#smrad = float(sys.argv[4])
#ncyl = int(sys.argv[5]) 	# only 1 cyl allowed

#sys_type = cyl
# MES : right now every box has same amount of charge
#   change so that charge is distributed randomly through total system
total_charge = 8    # must be divisible by 8
box_charge = total_charge / 8
radius = 10
ncyl = 1

at_rad = 1.2
size = 76.0	# = L for cubic box
#	L = 204 ==> 85 "atoms" on a side


# filen = ''+sys_type+'.pqr'


#atom_list = ["O" , "S"]
#radii = [ 2.015 , 1.7023 ]
#charges = [ 1.0817 , -0.6939 ]

def choose_a(pos) :
    if ( pos[0] < size/2 ) :
        if ( pos[1] < size/2 ) :
            if ( pos[2] < size/2 ) :
                a = 0
            elif ( pos[2] > size/2 ) :
                a = 1
        elif ( pos[1] > size/2 ) :
            if ( pos[2] < size/2 ) :
                a = 2
            elif ( pos[2] > size/2 ) :
                a = 3
    elif ( pos[0] > size/2 ) :
        if ( pos[1] < size/2 ) :
            if ( pos[2] < size/2 ) :
                a = 4
            elif ( pos[2] > size/2 ) :
                a = 5
        elif ( pos[1] > size/2 ) :
            if ( pos[2] < size/2 ) :
                a = 6
            elif ( pos[2] > size/2 ) :
                a = 7
#    print(pos, a)
    return(a)


def cyl_straight_write(at_rad, total_charge):
    if ncyl == 9 :
      cyl_pos = ( (19.0,19.0),(19.0,57.0), (19.0,95.0),
              (57.0,19.0),(57.0,57.0), (57.0,95.0),
              (95.0,19.0),(95.0,57.0), (95.0,95.0))
    elif ncyl == 1 :
      cyl_pos = ( ( size/2 , size/2 ) )
    cyl_rad = radius # Angstroms
    # accepted_atoms,cyl_neigh = [], []
    acp_at = [[] for _ in range(8)]
    cyl_neigh = [[] for _ in range(8)]
    charge_list = [[] for _ in range(8)]

    for i in np.arange(at_rad, size+at_rad, at_rad*2.0):
        for j in np.arange(at_rad, size+at_rad, at_rad*2.0):
            for k in np.arange(at_rad, size+at_rad, at_rad*2.0):
                nearcyl,cyl_count = 0,0; pos = np.array( (i,j,k) )
                a = choose_a(pos)   # assign atom to different lists based on position

                # Checking for overlap with cylinders
                for cyls in range(len(cyl_pos)):
                    cyl_temp = np.array(cyl_pos[cyls])
                    dist = np.linalg.norm(pos[0:2]-cyl_temp)
                    if( dist < at_rad+cyl_rad ):
                        cyl_count +=1
                    elif( dist < at_rad*2.1+cyl_rad ):
                        nearcyl=1

                # If no overlaps
                if (cyl_count==0):
                    # accepted_atoms.append( pos )
                    acp_at[a].append(pos)
                    cyl_neigh[a].append( nearcyl )

    for a in range(0,8) :
        cyl_neigh[a] = np.array(cyl_neigh[a])
        charge_list[a] = np.random.choice(np.where(cyl_neigh[a] == 1)[0], size=box_charge, replace=False)
#    charge_list = np.random.choice(np.where(cyl_neigh != 2)[0], size=total_charge, replace=False)
#    return(accepted_atoms,charge_list)
    return(acp_at,charge_list)


def cyl_sine_write(at_rad, total_charge):
    cyl_pos = ( ( size/2 , size/2 ) ) # pore starts at center of box (for straight cylinder)
                                         # (for sin fnc cylinder)
#    cyl_pos = ( ( size/4 , size/4 ) )
    amp = size / 4

    cyl_rad = radius # Angstroms
    accepted_atoms,cyl_neigh = [], []

    for i in np.arange(at_rad, size+at_rad, at_rad*2.0):
        for j in np.arange(at_rad, size+at_rad, at_rad*2.0):
            for k in np.arange(at_rad, size+at_rad, at_rad*2.0):
                nearcyl,cyl_count = 0,0; pos = np.array( (i,j,k) )
                rad = (k/size)*2*np.pi
                line = amp * np.sin(rad) + cyl_pos[0]
#                print(k, pos, line)

                # Checking for overlap with cylinders
# for sine function
                cyl_temp = np.array(( cyl_pos[0], line, k ))
                dist = np.linalg.norm(pos[0:2]-cyl_temp[0:2])

                if( dist < at_rad+cyl_rad ):
                    cyl_count +=1
                elif( dist < at_rad*2.1+cyl_rad ):
                    nearcyl=1

                # If no overlaps
                if (cyl_count==0):
                    accepted_atoms.append( pos )
                    cyl_neigh.append( nearcyl )

#    print(len(accepted_atoms))
    cyl_neigh = np.array(cyl_neigh)
#    print( "Number of surface atoms = %d" % len(np.where(cyl_neigh == 1)[0]) )
# to select atoms on the surface only
#    charge_list = np.random.choice(np.where(cyl_neigh == 1)[0], size=total_charge, replace=False)
# to select atoms throughout the material
    charge_list = np.random.choice(np.where(cyl_neigh != 2)[0], size=total_charge, replace=False)
    return(accepted_atoms,charge_list)


'''
def so3_group(x, y, z) :
  pos_q = np.array([ x , y , z ])
# Sulfonate group stats
  s_rad=2.015; o_rad=1.7023;
  PO = ((1.381903198,-0.099221642,-0.490626808),
        (-0.642848081,-1.227488959,-0.442011737),
        (-0.642848081,1.227488959,-0.442011737));
#  PO = ( (0,0,0) , (0,0,0) , (0,0,0) ) 	# This puts all the atoms in the same spot, so a single pt charge
# PO = position of oxygen relative to sulfur
# stores which "atoms" are on surface
# replace that atom with sulfur
# can change so that oxygens point into cavity
  return(PO)
'''

def nMe3_group_c( x , y , z ) :
    Pme_c = ( ( 0.2697 ,  0.6756 ,  1.3078 ) , ( -1.4603 , -0.3077 , -0.1107 ) ,
              ( 0.7849 , -1.2718 , -0.0756 ) , (  0.4057 ,  0.9039 , -1.1215 ) )
    return(Pme_c)

def nMe3_group_h( x , y , z ) :
    Pme_h = ( ( -0.3059 ,  1.6019 ,  1.3552 ) , (  1.3361 ,  0.8964 ,  1.3809 ) , ( -0.0288 ,  0.0108 ,  2.1201 ) ,
              ( -2.0257 ,  0.6243 , -0.0546 ) , ( -1.7482 , -0.9671 ,  0.7102 ) , ( -1.6487 , -0.7999 , -1.0665 ) ,
              (  1.8481 , -1.0388 ,  0.0058 ) , (  0.5830 , -1.7586 , -1.0316 ) , (  0.4835 , -1.9254 ,  0.7450 ) ,
              ( -0.1707 ,  1.8287 , -1.0594 ) , (  0.2060 ,  0.4042 , -2.0711 ) , (  1.4713 ,  1.1235 , -1.0338 ) )
    return(Pme_h)


def write_8pqr( acc_at, chg_lst ):
    # Writing out "atoms"
    # for tetramethyl ammonium
    for a in range(0,8) :
        accepted_at = acc_at[a]
        charge_list = chg_lst[a]
        pqr = open("%s.pqr" % a , 'w')
        n_rad = 1.63 ; n_charge = 0.4300 ;
        c_rad = 1.70 ; c_charge = -0.2200 ;
        h_rad = 1.00 ; h_charge = 0.1210 ;
        ch_rad = 2.00 ; ch_charge = 0.1430 ;
        for i in range(len(accepted_at)):
            if i in charge_list:
                Pme_c = nMe3_group_c( accepted_at[i][0], accepted_at[i][1], accepted_at[i][2] )
                pqr.write("ATOM %6d  N    MEM %-6d    %7.2f  %7.2f  %7.2f   %6.4f %4.2f\n"
                    % (i, i, accepted_at[i][0], accepted_at[i][1], accepted_at[i][2], n_charge, n_rad))
                for c in range(4):
                    pqr.write("ATOM %6d  C    MEM %-6d    %7.2f  %7.2f  %7.2f   %6.4f %4.2f\n"
                        % (i, i, accepted_at[i][0]+Pme_c[c][0], accepted_at[i][1]+Pme_c[c][1],
                                accepted_at[i][2]+Pme_c[c][2], ch_charge, ch_rad))
            else:
                pqr.write("ATOM %6d  R    MEM %-6d    %7.2f  %7.2f  %7.2f  0.0000 %4.2f\n"
                        % (i, i, accepted_at[i][0], accepted_at[i][1], accepted_at[i][2], at_rad))
        pqr.close()

#
'''
def write_pqr( filename, accepted_at, chg_lst ):
    # Writing out "atoms"
    # for tetramethyl ammonium
    pqr = open(filename, 'w')
    n_rad = 1.63 ; n_charge = 0.4300 ;
    c_rad = 1.70 ; c_charge = -0.2200 ;
    h_rad = 1.00 ; h_charge = 0.1210 ;
    ch_rad = 2.00 ; ch_charge = 0.1430 ;
    for i in range(len(accepted_at)):
        if i in chg_lst:
            Pme_c = nMe3_group_c( accepted_at[i][0], accepted_at[i][1], accepted_at[i][2] )
            pqr.write("ATOM %6d  N    MEM %-6d    %7.2f  %7.2f  %7.2f   %6.4f %4.2f\n"
                % (i, i, accepted_at[i][0], accepted_at[i][1], accepted_at[i][2], n_charge, n_rad))
            for c in range(4):
                pqr.write("ATOM %6d  C    MEM %-6d    %7.2f  %7.2f  %7.2f  %6.4f %4.2f\n"
                    % (i, i, accepted_at[i][0]+Pme_c[c][0], accepted_at[i][1]+Pme_c[c][1],
                            accepted_at[i][2]+Pme_c[c][2], ch_charge, ch_rad))
        else:
            pqr.write("ATOM %6d  R    MEM %-6d    %7.2f  %7.2f  %7.2f  0.0000 %4.2f\n"
                    % (i, i, accepted_at[i][0], accepted_at[i][1], accepted_at[i][2], at_rad))
    pqr.close()
'''
#
'''
def write_pqr( filename, accepted_at, chg_lst ):
    # Writing out "atoms"
    # for sulfonate
    pqr = open(filename, 'w')
    s_rad=2.015; o_rad=1.7023;
    for i in range(len(accepted_at)):
        if i in chg_lst:
            PO = so3_group( accepted_at[i][0], accepted_at[i][1], accepted_at[i][2] )
            pqr.write("ATOM %6d  S    MEM %-6d    %7.2f  %7.2f  %7.2f   1.0817 %4.2f\n"
                    % (i, i, accepted_at[i][0], accepted_at[i][1], accepted_at[i][2], s_rad))
#            for o in range(3):
#                pqr.write("ATOM %6d  O    MEM %-6d    %7.2f  %7.2f  %7.2f  -0.6939 %4.2f\n"
#                    % (i, i, accepted_at[i][0]+PO[o][0], accepted_at[i][1]+PO[o][1],
#                            accepted_at[i][2]+PO[o][2], o_rad))
#        else:
#            pqr.write("ATOM %6d  R    MEM %-6d    %7.2f  %7.2f  %7.2f  0.0000 %4.2f\n"
#                    % (i, i, accepted_at[i][0], accepted_at[i][1], accepted_at[i][2], at_rad))
#    pqr.close()
'''

# ------------------------------------------------------------------------
# main

acc_at, cg_lst = cyl_straight_write(at_rad, total_charge)

#print(acc_at[3])
print(cg_lst, len(cg_lst))

write_8pqr( acc_at, cg_lst )
