import numpy as np
import sys

""" Program to make a idealized model of nafion """
# requires Numpy 1.7.0 or later


# To do:
#       clustered and even charge distributions

#--------------------------------------------------------------------
# global variables

sys_type = sys.argv[1]  # Choices: lam, pch, cyl
at_rad = 1.2
total_charge = float(sys.argv[2])
radius = float(sys.argv[3])
smrad = float(sys.argv[4])
ncyl = int(sys.argv[5])

boxl = 114     # length for cubic box
sine = False    # True ==> sine wave channel; False ==> straight channel
bulk = False   # True ==> distribute charge groups throughout the bulk
                # False ==> place charge groups only on the surface
fnc_grp = 'tma'   # Choices : tma, so3

#filen = '/Users/lfelberg/Desktop/'+sys_type+'_r'+ str(at_rad)+'_c'+str(total_charge)+'.pqr'
filen = ''+sys_type+'.pqr'

#--------------------------------------------------------------------
# picks atoms to convert to charge groups

def charges(neigh) :
    if bulk :
        list = np.random.choice(np.where(neigh != 2)[0], size=total_charge, replace=False)
    elif not bulk :
        list = np.random.choice(np.where(neigh == 1)[0], size=total_charge, replace=False)
    return list

#--------------------------------------------------------------------
# set up function for each system type

def pore_write(at_rad, total_charge):
#    pore_pos = ( (25.0,25.0,25.0), (75.0,25.0,25.0), (25.0,75.25,25.0), (25.0,25.0,75.0),
#                (75.0,75.0,25.0), (75.0,25.0,75.0), (25.0,75.0,75.0), (75.0,75.0,75.0))
    pore_rad, chan_rad = radius, smrad # Angstroms
    accepted_atoms,pore_neigh = [], []
#    channel_locs = np.array((0.,50.))
    count = 0

    pore_pos = ( ( 0.25*boxl , 0.25*boxl , 0.25*boxl ) , ( 0.75*boxl , 0.25*boxl , 0.25*boxl ) ,
                 ( 0.25*boxl , 0.75*boxl , 0.25*boxl ) , ( 0.25*boxl , 0.25*boxl , 0.75*boxl ) ,
                 ( 0.75*boxl , 0.75*boxl , 0.25*boxl ) , ( 0.75*boxl , 0.25*boxl , 0.75*boxl ) ,
                 ( 0.25*boxl , 0.75*boxl , 0.75*boxl ) , ( 0.75*boxl , 0.75*boxl , 0.75*boxl ) )
    channel_locs = np.array( ( 0.25*boxl, 0.75*boxl ) )

#    for i in np.arange(-25, 75, at_rad*2.0):
#        for j in np.arange(-25, 75, at_rad*2.0):
#            for k in np.arange(-25, 75, at_rad*2.0):

    for i in np.arange(0, boxl, at_rad*2.0):
        for j in np.arange(0, boxl, at_rad*2.0):
            for k in np.arange(0, boxl, at_rad*2.0):

                chan_count, nearpore = 0, 0; pos = np.array( (i,j,k) )
                pore_count=0

                # Checking for overlap with channels
                for xyz in range(3):
                    overlap = sum(abs(channel_locs - pos[xyz]) < (at_rad+chan_rad))
                    if overlap > 0:
                        chan_count+=1

                # Checking for overlap with pores
                for pores in range(len(pore_pos)):
                    pore_temp = np.array(pore_pos[pores])
                    dist = np.linalg.norm(pos-pore_temp)
                    if( dist < at_rad+pore_rad ):
                        pore_count +=1
                    elif( dist < at_rad*1.15+pore_rad ):
                        count += 1
                        nearpore=1

                # If overlaps with neither pore nor channel
                if ((pore_count==0) and (chan_count < 2)):
                    accepted_atoms.append( pos )
                    pore_neigh.append( nearpore )

    pore_neigh = np.array(pore_neigh)
    print(len(pore_neigh),count, pore_neigh)
#    charge_list = np.random.choice(np.where(pore_neigh == 1)[0], size=total_charge, replace=False)
    charge_list = charges(pore_neigh)
    return(accepted_atoms,charge_list)





def lam_write(at_rad, total_charge):
#    lam_pos = ( 25.0, 75.0 )
    lam_rad = radius # Angstroms
#    lam_pos = ( 0.5*boxl-lam_rad , 0.5*boxl+lam_rad )
    lam_pos = ( boxl/2 )
    print(radius, lam_pos, boxl)
    accepted_atoms,lam_neigh = [],[]
    count = 0

    for i in np.arange(at_rad, boxl+at_rad, at_rad*2.0):
        for j in np.arange(at_rad, boxl+at_rad, at_rad*2.0):
            for k in np.arange(at_rad, boxl+at_rad, at_rad*2.0):
                nearlam,lam_count = 0,0; pos = np.array( (i,j,k) )

                # Checking for overlap with laminar layers
                #for lams in range(len(lam_pos)):
                lam_temp = lam_pos #[lams]
                dist = np.linalg.norm(pos[2]-lam_temp)
#                print(lam_temp,pos,dist)

                if( dist < at_rad+lam_rad ):
                    lam_count +=1
#                elif( dist < at_rad*2.4+lam_rad ):
                elif( dist < at_rad*3.0+lam_rad ):
                        nearlam=1
                        count += 1

                # If no overlaps
                if (lam_count==0):
                    accepted_atoms.append( pos )
                    lam_neigh.append( nearlam )

    lam_neigh = np.array(lam_neigh)
    print(len(lam_neigh),lam_neigh)
    print(count)
#    charge_list = np.random.choice(np.where(lam_neigh == 1)[0], size=total_charge, replace=False)
    charge_list = charges(lam_neigh)
    return(accepted_atoms,charge_list)




def cyl_write(at_rad, total_charge):
    if ncyl == 9 :
      cyl_pos = ( (19.0,19.0),(19.0,57.0), (19.0,95.0),
              (57.0,19.0),(57.0,57.0), (57.0,95.0),
              (95.0,19.0),(95.0,57.0), (95.0,95.0))
    elif ncyl == 1 :
      cyl_pos = ( ( boxl/2 , boxl/2 ) )
    cyl_rad = radius # Angstroms
    accepted_atoms,cyl_neigh = [], []

    for i in np.arange(at_rad, boxl+at_rad, at_rad*2.0):
        for j in np.arange(at_rad, boxl+at_rad, at_rad*2.0):
            for k in np.arange(at_rad, boxl+at_rad, at_rad*2.0):
                nearcyl,cyl_count = 0,0; pos = np.array( (i,j,k) )

                # Checking for overlap with cylinders
                for cyls in range(len(cyl_pos)):
                    cyl_temp = np.array(cyl_pos[cyls])
                    dist = np.linalg.norm(pos[0:2]-cyl_temp)
                    if( dist < at_rad+cyl_rad ):
                        cyl_count +=1
                    elif( dist < at_rad*2.1+cyl_rad ):
                        nearcyl=1

                # If no overlaps
                if (cyl_count==0):
                    accepted_atoms.append( pos )
                    cyl_neigh.append( nearcyl )

    cyl_neigh = np.array(cyl_neigh)
    charge_list = charges(cyl_neigh)
    return(accepted_atoms,charge_list)

def cyl_sine_write(at_rad, total_charge):
    cyl_pos = ( ( boxl/2 , boxl/2 ) ) # pore starts at center of box
    amp = boxl / 4

    cyl_rad = radius # Angstroms
    accepted_atoms,cyl_neigh = [], []

    for i in np.arange(at_rad, boxl+at_rad, at_rad*2.0):
        for j in np.arange(at_rad, boxl+at_rad, at_rad*2.0):
            for k in np.arange(at_rad, boxl+at_rad, at_rad*2.0):
                nearcyl,cyl_count = 0,0; pos = np.array( (i,j,k) )
                rad = (k/boxl)*2*np.pi
                line = amp * np.sin(rad) + cyl_pos[0]

                # Checking for overlap with cylinders
                # for sine function
                cyl_temp = np.array(( cyl_pos[0], line, k ))
                dist = np.linalg.norm(pos[0:2]-cyl_temp[0:2])

                if( dist < at_rad+cyl_rad ):
                    cyl_count +=1
                elif( dist < at_rad*2.1+cyl_rad ):
                    nearcyl=1

                # If no overlaps
                if (cyl_count==0):
                    accepted_atoms.append( pos )
                    cyl_neigh.append( nearcyl )

    cyl_neigh = np.array(cyl_neigh)
    charge_list = charges(cyl_neigh)
    return(accepted_atoms,charge_list)


#--------------------------------------------------------------------
# set up functional group positions


def so3_group(x, y, z) :
    PO = ( (  1.381903198 , -0.099221642 , -0.490626808 ) ,
           ( -0.642848081 , -1.227488959 , -0.442011737 ) ,
           ( -0.642848081 ,  1.227488959 , -0.442011737 ) ) ;
    return(PO)



def nMe3_group_c( x , y , z ) :
    Pme_c = ( ( 0.2697 ,  0.6756 ,  1.3078 ) , ( -1.4603 , -0.3077 , -0.1107 ) ,
              ( 0.7849 , -1.2718 , -0.0756 ) , (  0.4057 ,  0.9039 , -1.1215 ) )
    return(Pme_c)


#--------------------------------------------------------------------
# write pqr file with appropriate functional group


def write_sulf_pqr( filename, accepted_at, chg_lst ):
    # Writing out "atoms"
    s_rad=2.015; o_rad=1.7023;
    pqr = open(filename, 'w')
    for i in range(len(accepted_at)):
        if i in chg_lst:
            PO = so3_group( accepted_at[i][0], accepted_at[i][1], accepted_at[i][2] )
            pqr.write("ATOM %6d  S    MEM %-6d    %7.2f  %7.2f  %7.2f   1.0817 %4.2f\n"
                    % (i, i, accepted_at[i][0], accepted_at[i][1], accepted_at[i][2], s_rad))
            for o in range(3):
                pqr.write("ATOM %6d  O    MEM %-6d    %7.2f  %7.2f  %7.2f  -0.6939 %4.2f\n"
                    % (i, i, accepted_at[i][0]+PO[o][0], accepted_at[i][1]+PO[o][1],
                            accepted_at[i][2]+PO[o][2], o_rad))
        else:
            pqr.write("ATOM %6d  R    MEM %-6d    %7.2f  %7.2f  %7.2f  0.0000 %4.2f\n"
                    % (i, i, accepted_at[i][0], accepted_at[i][1], accepted_at[i][2], at_rad))
    pqr.close()


def write_tma_pqr( filename, accepted_at, chg_lst ):
    # for tetramethyl ammonium with united atom approach
    pqr = open(filename, 'w')
    n_rad = 1.63 ; n_charge = 0.4300 ;
    ch_rad = 2.00 ; ch_charge = 0.1430 ;
    for i in range(len(accepted_at)):
        if i in chg_lst:
            Pme_c = nMe3_group_c( accepted_at[i][0], accepted_at[i][1], accepted_at[i][2] )
            pqr.write("ATOM %6d  N    MEM %-6d    %7.2f  %7.2f  %7.2f   %6.4f %4.2f\n"
                % (i, i, accepted_at[i][0], accepted_at[i][1], accepted_at[i][2], n_charge, n_rad))
            for c in range(4):
                pqr.write("ATOM %6d  C    MEM %-6d    %7.2f  %7.2f  %7.2f  %6.4f %4.2f\n"
                    % (i, i, accepted_at[i][0]+Pme_c[c][0], accepted_at[i][1]+Pme_c[c][1],
                            accepted_at[i][2]+Pme_c[c][2], ch_charge, ch_rad))
        else:
            pqr.write("ATOM %6d  R    MEM %-6d    %7.2f  %7.2f  %7.2f  0.0000 %4.2f\n"
                    % (i, i, accepted_at[i][0], accepted_at[i][1], accepted_at[i][2], at_rad))
    pqr.close()



#--------------------------------------------------------------------
# Main()

if sys_type== 'lam':
    acc_at, cg_lst = lam_write(at_rad, total_charge)
elif sys_type=='pch':
    acc_at, cg_lst = pore_write(at_rad, total_charge)
elif sys_type=='cyl':
    if sine :
        acc_at, cg_lst = cyl_sine_write(at_rad, total_charge)
    elif not sine :
        acc_at, cg_lst = cyl_write(at_rad, total_charge)
else:
    print("Name not recognized")


if fnc_grp == 'tma' :
    write_tma_pqr( filen, acc_at, cg_lst )
elif fnc_grp == 'so3' :
    write_sulf_pqr( filen, acc_at, cg_lst )
else :
    print("Functional group not recognized.")
