import numpy as np
import sys

""" Program to make a idealized model of nafion """

#sys_type = 'pore'  # Choices: lam, pore, cyl
#total_charge = 150
sys_type = sys.argv[1]  # Choices: lam, pore, cyl
total_charge = int(sys.argv[2])
radius = float(sys.argv[3])
smrad = float(sys.argv[4])

filen = ''+sys_type+'.pqr'
at_rad = 1.10

def pore_write(at_rad, radi, small_rad, total_charge):
    pore_pos = ( (25.0,25.0,25.0), (75.0,25.0,25.0), (25.0,75.25,25.0), (25.0,25.0,75.0),
                (75.0,75.0,25.0), (75.0,25.0,75.0), (25.0,75.0,75.0), (75.0,75.0,75.0))
                
    pore_rad, chan_rad = radi, small_rad # Angstroms
    accepted_atoms,pore_neigh = [], []
    channel_locs = np.array((25.,75.))

    for i in np.arange(at_rad, 100.+at_rad, at_rad*2.0):
        for j in np.arange(at_rad, 100.+at_rad, at_rad*2.0):
            for k in np.arange(at_rad, 100.+at_rad, at_rad*2.0):
                chan_count, nearpore = 0, 0; pos = np.array( (i,j,k) )
                pore_count=0
                # Checking for overlap with channels
                for xyz in range(3):
                    overlap = sum(abs(channel_locs - pos[xyz]) < (at_rad+chan_rad))
                    if overlap > 0:
                        chan_count+=1
                
                # Checking for overlap with pores
                for pores in range(len(pore_pos)):
                    pore_temp = np.array(pore_pos[pores])
                    dist = np.linalg.norm(pos-pore_temp)
                    if( dist < at_rad+pore_rad ):
                        pore_count +=1
                    elif( dist < at_rad*2.15+pore_rad ):
                        nearpore=1
                
                # If overlaps with neither pore nor channel
                if ((pore_count==0) and (chan_count < 2)):
                    accepted_atoms.append( pos )
                    pore_neigh.append( nearpore )
    
    pore_neigh = np.array(pore_neigh)
    charge_list = np.random.choice(np.where(pore_neigh == 1)[0], size=total_charge, replace=False)
    return(accepted_atoms,charge_list)

def lam_write(at_rad, radi, total_charge):
    lam_pos = ( 25.0, 75.0 )      
    lam_rad = radi # Angstroms
    accepted_atoms,lam_neigh = [],[]
    
    for i in np.arange(at_rad, 100.-at_rad, at_rad*2.0):
        for j in np.arange(at_rad, 100.+at_rad, at_rad*2.0):
            for k in np.arange(at_rad, 100.+at_rad, at_rad*2.0):
                nearlam,lam_count = 0,0; pos = np.array( (i,j,k) )
            
                # Checking for overlap with laminar layers
                for lams in range(len(lam_pos)):
                    lam_temp = lam_pos[lams]
                    dist = np.linalg.norm(pos[0]-lam_temp)
                    
                    if (dist < at_rad+lam_rad):
                        lam_count +=1
                    elif( dist < at_rad*3.5+lam_rad ):
                        nearlam=1
                    
                    if( (abs(j - at_rad) < 0.05 ) and (abs(k - at_rad) < 0.05 )):
                        lam_count=0
                
                # If no overlaps
                if (lam_count==0):
                    accepted_atoms.append( pos )
                    lam_neigh.append( nearlam )
    
    lam_neigh = np.array(lam_neigh)
    charge_list = np.random.choice(np.where(lam_neigh == 1)[0], size=total_charge, replace=False)
    return(accepted_atoms,charge_list)

def cyl_write(at_rad, radi, total_charge):
    cyl_pos = ( (19.0,19.0),(19.0,57.0),
              (57.0,19.0),(57.0,57.0),
              (95.0,19.0),(95.0,57.0))
              # (19.0,95.0), (57.0,95.0), (95.0,95.0))

    cyl_rad, edge = radi, 76. # Angstroms
    accepted_atoms,cyl_neigh = [], []
    
    for i in np.arange(at_rad, edge+at_rad, at_rad*2.0):
        for j in np.arange(at_rad, edge+at_rad, at_rad*2.0):
            for k in np.arange(at_rad, edge+at_rad, at_rad*2.0):
                nearcyl,cyl_count = 0,0; pos = np.array( (i,j,k) )
            
                # Checking for overlap with cylinders
                for cyls in range(len(cyl_pos)):
                    cyl_temp = np.array(cyl_pos[cyls])
                    dist = np.linalg.norm(pos[0:2]-cyl_temp)
                    if( dist < at_rad+cyl_rad ):
                        cyl_count +=1
                    elif( dist < at_rad*2.1+cyl_rad ):
                        nearcyl=1
                
                # If no overlaps
                if (cyl_count==0):
                    accepted_atoms.append( pos )
                    cyl_neigh.append( nearcyl )
    
    cyl_neigh = np.array(cyl_neigh)
    charge_list = np.random.choice(np.where(cyl_neigh == 1)[0], size=total_charge, replace=False)
    return(accepted_atoms,charge_list)

# Sulfonate group stats
s_rad=2.015; o_rad=1.7023; h_rad = 1.09
PO = ((1.381903198,-0.099221642,-0.490626808),
        (-0.642848081,-1.227488959,-0.442011737),
        (-0.642848081,1.227488959,-0.442011737));
H = (2.381903198,-0.099221642,-0.490626808)

def write_pqr( filename, accepted_at, chg_lst ):
    # Writing out "atoms"
    pqr = open(filename, 'w')
    for i in range(len(accepted_at)): 
        if i in chg_lst:
            pqr.write("ATOM %6d  S    MEM %-6d    %7.2f  %7.2f  %7.2f   1.0817 %4.2f\n" 
                    % (i, i, accepted_at[i][0], accepted_at[i][1], 
                         accepted_at[i][2], s_rad))      
            for o in range(3):
                pqr.write("ATOM %6d  O    MEM %-6d    %7.2f  %7.2f  %7.2f  -0.6939 %4.2f\n" 
                    % (i, i, accepted_at[i][0]+PO[o][0], accepted_at[i][1]+PO[o][1], 
                            accepted_at[i][2]+PO[o][2], o_rad))   
            pqr.write("ATOM %6d  H    MEM %-6d    %7.2f  %7.2f  %7.2f   1.0000 %4.2f\n" 
                    % (i, i, accepted_at[i][0]+H[0], accepted_at[i][1]+H[1], 
                            accepted_at[i][2]+H[2], h_rad)) 
        else:             
            pqr.write("ATOM %6d  R    MEM %-6d    %7.2f  %7.2f  %7.2f  0.0000 %4.2f\n" 
                    % (i, i, accepted_at[i][0], accepted_at[i][1], accepted_at[i][2], at_rad))
    pqr.close()

if sys_type== 'lam':
    acc_at, cg_lst = lam_write(at_rad, radius, total_charge)
elif sys_type=='pore':
    acc_at, cg_lst = pore_write(at_rad, radius, smrad, total_charge)
elif sys_type=='cyl':
    acc_at, cg_lst = cyl_write(at_rad, radius, total_charge)  
else:
    print "Name not recognized"  
    
write_pqr( filen, acc_at, cg_lst )
