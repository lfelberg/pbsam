program buildcylinder_pqr
!program for building an idealized cylinder system pqr file
!created by S. Liu (02/23/2013)

implicit none

character*5, parameter :: hpname='ATOM '
character*4, parameter :: resname = 'MEM '
character*4, parameter :: neutralname = 'NEU '
character*4, parameter :: sulf_name = 'S   '
character*4, parameter :: Ox_name = 'O   '
character*50 :: output_fname
character*50 :: xyzr_fname
real*8, parameter :: rpore=12.d0
real*8, parameter :: dpp=38.d0 !distance between pores
real*8, parameter :: pi=3.141592653589793d0
integer :: nside,npore,nring
integer :: atom_num
integer, parameter :: output_unit=11
integer, parameter :: xyzr_unit=12
real*8 :: BoxLength,angleinternal
real*8 :: deltaphi, phi,deltaz
real*8 :: xpcenter,ypcenter,zpcenter
real*8 :: z1,z2,z3
real*8 :: xxxsulf,yyysulf
real*8 :: xxxO1,yyyO1,zzzO1
real*8 :: xxxO2,yyyO2,zzzO2
real*8 :: xxxO3,yyyO3,zzzO3
real*8, allocatable :: center(:,:,:)
real*8 :: sulfden
real*8, parameter :: rsulf=2.015d0,qsulf=1.0817d0
real*8, parameter :: rO=1.7023d0, qO=-0.6142d0
real*8, parameter :: bondSO=1.48d0
real*8, parameter :: qneutral = 0.d0
real*8, parameter :: rsphere=1.d0   !radius for spheres in the hydrophobic regions
real*8, parameter :: zring=8.d0   !distances between adjacent rings on each pore
real*8, parameter :: AngleSOS=115.5d0   !S-O-S bond angle
integer, parameter :: nsulfring = 8 !number of sulfonate groups per ring
integer :: i,j,k,pside,ll
logical :: ifoutthepore
logical :: outsidepore
real*8, allocatable :: xsulf(:),ysulf(:)
real*8, allocatable :: xO1(:),xO2(:),xO3(:)
real*8, allocatable :: yO1(:),yO2(:),yO3(:)
real*8, allocatable :: zzz(:)

write(*,*) "Name of the output PQR file"
read(*,*)  output_fname
write(*,*) "Name of the output xyzr file"
read(*,*) xyzr_fname

nside=3
npore = nside*nside*nside
angleinternal=AngleSOS/180.d0*pi

BoxLength=dpp*dble(nside)

allocate(center(nside,nside,2))

allocate(xsulf(nsulfring))
allocate(ysulf(nsulfring))

allocate(xO1(nsulfring))
allocate(yO1(nsulfring))
allocate(xO2(nsulfring))
allocate(yO2(nsulfring))
allocate(xO3(nsulfring))
allocate(yO3(nsulfring))

nring = int(BoxLength/zring)
allocate(zzz(nring))

do i=1,nside
   do j=1,nside
      center(i,j,1)=dble(i-1)*dpp - BoxLength/2.d0 + dpp/2.d0 
      center(i,j,2)=dble(j-1)*dpp - BoxLength/2.d0 + dpp/2.d0
   end do
end do

pside = int(BoxLength/rsphere/2.d0)

!write non-polar part to the pqr file
open(unit=output_unit,file=output_fname,status='replace')
open(unit=xyzr_unit,file=xyzr_fname,status='replace')
atom_num = 0
do i=1,pside
   xpcenter=-BoxLength/2.d0 + rsphere*dble(2*i+1)
   do j=1,pside
      ypcenter=-BoxLength/2.d0 + rsphere*dble(2*j+1)
      do k=1,pside
         zpcenter=-BoxLength/2.d0 + rsphere*dble(2*k+1) 
         outsidepore = ifoutthepore(xpcenter,ypcenter,center,nside,rpore,rsphere)
         if(outsidepore) then
            atom_num = atom_num + 1
            write(output_unit, '(a5,1x,i6,1x,a4,1x,a4,9x,3f8.3,2f8.4)') hpname, atom_num, &
              neutralname, resname, xpcenter, ypcenter, zpcenter,qneutral, rsphere
            write(xyzr_unit,'(f8.3,1x,f8.3,1x,f8.3,1x,f5.2)') xpcenter,ypcenter,zpcenter,rsphere
         end if 
       end do
    end do
 end do

!generate x,y coordinate for sulfur and oxygen atoms relative to the center of each pore
 deltaphi=2.d0*pi/dble(nsulfring)
 do i=1,nsulfring
    phi = deltaphi*dble(i-1)
    xsulf(i)=rpore*cos(phi)
    ysulf(i)=rpore*sin(phi) 
    call getOxy(xO1(i),xO2(i),xO3(i),yO1(i),yO2(i),yO3(i),rpore,bondSO,angleinternal,phi) 
 end do

!generate z coordinate for oxygen atoms relative to the xy plane
call  getOz(z1,z2,z3,bondSO,angleinternal)

!write sulfonate group atoms tot the pqr file
deltaz = BoxLength/dble(nring)
do i=1,nside
   do j=1,nside
      do k=1,nring
         zzz(k) = -deltaz/2.d0 + deltaz*dble(k) - BoxLength/2.d0
         do ll=1,nsulfring
            atom_num = atom_num + 1
            xxxsulf = center(i,j,1) + xsulf(ll)
            yyysulf = center(i,j,2) + ysulf(ll)
            write(output_unit,'(a4,1x,i6,1x,a4,1x,a4,9x,3f8.3,2f8.4)') hpname,atom_num, sulf_name,resname, & 
         &  xxxsulf,yyysulf,zzz(k),qsulf,rsulf 
            write(xyzr_unit,'(f8.3,1x,f8.3,1x,f8.3,1x,f5.2)') xxxsulf,yyysulf,zzz(k),rsulf
            atom_num = atom_num + 1
            xxxO1 = center(i,j,1) + xO1(ll)
            yyyO1 = center(i,j,2) + yO1(ll)
            zzzO1 = zzz(k) + z1
            write(output_unit,'(a4,1x,i6,1x,a4,1x,a4,9x,3f8.3,2f8.4)') hpname,atom_num, Ox_name,resname, & 
         &  xxxO1,yyyO1,zzzO1,qO,rO 
            write(xyzr_unit,'(f8.3,1x,f8.3,1x,f8.3,1x,f5.2)') xxxO1,yyyO1,zzzO1,rO
            atom_num = atom_num + 1
            xxxO2 = center(i,j,1) + xO2(ll)
            yyyO2 = center(i,j,2) + yO2(ll)
            zzzO2 = zzz(k) + z2
            write(output_unit,'(a4,1x,i6,1x,a4,1x,a4,9x,3f8.3,2f8.4)') hpname,atom_num, Ox_name,resname, & 
         &  xxxO2,yyyO2,zzzO2,qO,rO 
            write(xyzr_unit,'(f8.3,1x,f8.3,1x,f8.3,1x,f5.2)') xxxO2,yyyO2,zzzO2,rO
            atom_num = atom_num + 1
            xxxO3 = center(i,j,1) + xO3(ll)
            yyyO3 = center(i,j,2) + yO3(ll)
            zzzO3 = zzz(k) + z3
            write(output_unit,'(a4,1x,i6,1x,a4,1x,a4,9x,3f8.3,2f8.4)') hpname,atom_num, Ox_name,resname, & 
         &  xxxO3,yyyO3,zzzO3,qO,rO
            write(xyzr_unit,'(f8.3,1x,f8.3,1x,f8.3,1x,f5.2)') xxxO3,yyyO3,zzzO3,rO
         end do
      end do
    end do
 end do

close(output_unit)
close(xyzr_unit) 
        
end program


function ifoutthepore(x, y, center,nside,rpore,rsphere)
logical ifoutthepore
real*8 :: x,y,rpore,rsphere,rrr
integer :: nside,i,j
real*8, dimension(nside,nside,2) :: center

ifoutthepore=.true.
loop1: do i=1,nside
  loop2: do j=1,nside
            rrr=sqrt((x-center(i,j,1))**2 + (y-center(i,j,2))**2)
            if(rrr.lt.(rpore+rsphere)) then
               ifoutthepore=.false.
               exit loop1
            end if
         end do loop2
       end do loop1
end function

subroutine getOxy(x1,x2,x3,y1,y2,y3,rpore,bondSO,angleinternal,phi)
real*8 :: x1,x2,x3,y1,y2,y3
real*8 :: x10,x20,x30,y10,y20,y30
real*8 :: rpore,bondSO,angleinternal,phi
real*8 :: rOO,rSP

rOO=2.d0*BondSO*sin(angleinteral/2.d0)
!calculate the distance between sulfur atom and point P
!where P is the center of the triangle of three O atoms
rSP = sqrt((BondSO*cos(angleinternal/2.d0))**2.d0 - (rOO/2.d0/sqrt(3.d0))**2.d0)

!for the case of phi =0
x10 = rpore - rSP
y10 = 0.d0
x20 = x10
y20 = rOO/2.d0
x30 = x10
y30 = -rOO/2.d0
!apply the rotation matrix
x1 = cos(phi)*x10 - sin(phi)*y10
y1 = sin(phi)*x10 + cos(phi)*y10
x2 = cos(phi)*x20 - sin(phi)*y20
y2 = sin(phi)*x20 + cos(phi)*y20
x3 = cos(phi)*x30 - sin(phi)*y30
y3 = sin(phi)*x30 + cos(phi)*y30

end subroutine

subroutine getOz(z1,z2,z3,bondSO,angleinternal)
real*8 :: z1,z2,z3
real*8 :: bondSO, angleinternal
real*8 :: rOO

rOO=2.d0*BondSO*sin(angleinteral/2.d0)
z2 = -rOO/2.d0/sqrt(3.d0)
z3 = z2
z1 = rOO/sqrt(3.d0)

end subroutine
