#!/usr/bin/perl -w
use strict;

my $atom = 1; 
my $atomname = "X";
my $aaname;
my ($type, $index, $charge, $atomn);
my $aa;
my $x, my $y, my $z, my $r;
my $chain;

open(READ, "$ARGV[0]") || die;

while(my $line = <READ>)
{

    #($type, $index, $atomn, $aaname, $x,$y,$z, $charge, $r) = split(/\s+/, $line);

# ATOM      1  C71 DRG A   1      67.390  75.980  40.240     -0.274492  1.70

    ($type, $index, $atomname, $chain, $aa, $x,$y,$z, $charge, $r) = split(/\s+/, $line);
   
    $aa = 1;

	printf "%-5s  %4d  %-4s %3s   %3d    %8.3f %8.3f %8.3f %8.5f %7.2f\n",
           "ATOM",$atom, $atomname, "MEM", $aa, $x,  $y,  $z, $charge ,  $r;

    $atom++;

}
