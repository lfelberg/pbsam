#include "BD.h"
#ifdef __LEAK
#include "leaker.h"
#endif

using namespace std;
/*!########################################################################
 * #
 * # File: BD_main.cpp
 * # Date: June 2015
 * # Description: This file runs any system in the BD simulation
 * # Author: Felberg
 * # Copyright ( c )
 * #
 * ########################################################################*/

void runSim( CSetup setupVals, CPnt boxl, CBD & bd );
void finishSim( double start, int esc_ct, int ntraj, CBD & bd);

/*#########################################################*/
/*#########################################################*/
/* Main body to run bd and setup */
/*#########################################################*/
/*#########################################################*/
int bd_main( CSetup initAll, CPnt boxl )
{
  //create BD system
  CBD bd = CBD( initAll );
  runSim( initAll, boxl, bd);
  bd.cleanupCBD() ;
}  // end bd_main


/*!#########################################################*/
/*#########################################################*/
// runSim
// Function runs BD
/*#########################################################*/
/*#########################################################*/
void runSim( CSetup setupVals, CPnt boxl, CBD & bd )
{
  //Start timer
  double start = read_timer(  ); int esc_ct = 0; string status;

  if( setupVals.getRunType() == "pot" )
    { setupVals.setMaxTime(1); setupVals.setNTraj(1); }

  // Files for output of complete traj stats
  string completeTraj = "complete_traj." + setupVals.getRunName() + ".dat";
  ofstream fout( completeTraj.c_str() );

  for( int i = 0 ; i < setupVals.getNTraj() ; i++ )
  {
    double starti = read_timer(  ); //!< Time each trajectory
    bd.setconfig( setupVals, boxl ); //!< Set the init config

    // simtime is simulation time in NS
    double simtime = bd.runBD( i, setupVals, boxl );
    double tmax = ((double)setupVals.getMaxTime()) * PS_TO_NS * 2.0 ;
    // print out the simulation time
    if ( simtime < tmax )
    {
      status="escaped";
      esc_ct++;
    }
    else
      status="stuck";
    fout << "TRAJ " <<i<<" is " << status << " at step \t"<< simtime
				 << "\t [ns]; computation time = " << read_timer()-starti << endl;
  }
  finishSim( start, esc_ct, setupVals.getNTraj(), bd);
  fout.close(  );
} // end runSim

/*!#########################################################*/
/*#########################################################*/
// finishSim
// Function reports statistics and cleans up system
/*#########################################################*/
/*#########################################################*/
void finishSim( double start, int esc_ct, int ntraj, CBD & bd)
{
  // Report the total docking percent
  cout << "Total Docked " << esc_ct << " out of "
       << ntraj << " trajectories" << endl ;

  // Measuring total simulation time
  double end = read_timer(  );

  // Timing reports
  cout << "Program runtime = " << end-start << " s" << endl ;
  cout << "Program completed successfully" << endl ;

  // Cleaning up
  CSystem::deleteConstants(  );
}// end finishSim

