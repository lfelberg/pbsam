#include "terminate.h"

using namespace std;
/*!########################################################################
 * #
 * # File: terminate.cpp
 * # Date: Sept 2015
 * # Description: Contains termination class functions
 * # Author: Felberg
 * # Copyright ( c )
 * #
 * ########################################################################*/

/*!#########################################################*/
/*#########################################################*/
// check the proton has travelled through the pore
/*#########################################################*/
/*#########################################################*/
inline bool CBD::Isthrough( CMolecule* ion, CPnt boxl ) const
//CBD::Isthrough( CMolecule* ion, double boxlength ) const
{
  CPnt cenmass = ion->getRCenUR();
  // used for system with COM at 0,0,0
  if( ( cenmass.x() > boxl.x()/2.0 )
      || ( cenmass.x() < -1.0*boxl.x()/2.0)
      || ( cenmass.y() > boxl.y()/2.0 )
      || ( cenmass.y() < -1.0*boxl.y()/2.0)
      || ( cenmass.z() > boxl.x()/2.0 )
      || ( cenmass.z() < -1.0*boxl.x()/2.0) )
  // using boxl.x() in z direction because we want when it exits the pore
  // assumes cubic polymer system
  {
    return true;
  }
  else
    return false;
}  //! end Isthrough


/*!#########################################################*/
/*#########################################################*/
// general, for proteins to bind multiple partners
/*!#########################################################*/
/*#########################################################*/
bool
CBDnam::IsDocked(CMolecule* mol1, CMolecule* mol2) const
{
  int m1 = mol1->getMolType();

  int nDockDef = MOLCONTACTLIST[m1].size();
  for(int h=0; h<nDockDef; h++)
  {
    CMolContact mcon = MOLCONTACTLIST[m1][h];

    if(mcon.getMol2Type()!= mol2->getMolType() )  continue;
    int ncon = 0;
    vector<CContact> clist = mcon.getContactList();
    for(int k=0; k<clist.size() && ncon < mcon.getNContact(); k++)
    {
      if( CMolecule::getSepDist(mol1, clist[k].getID1(), mol2, clist[k].getID2())
          <= clist[k].getDist() ) ncon++;
      if( ncon == mcon.getNContact()) return true;
    }
  } // end all definitions

  return false;
}

/*!#########################################################*/
/*#########################################################*/
// finishSim
// Function reports statistics and cleans up system
/*#########################################################*/
/*#########################################################*/
void finishSim( double start, int esc_ct, int ntraj, CBD & bd)
{
  // Report the total docking percent
  cout << "Total Docked " << esc_ct << " out of "
       << ntraj << " trajectories" << endl ;

  // Measuring total simulation time
  double end = read_timer(  );

  // Timing reports
  cout << "Program runtime = " << end-start << " s" << endl ;
  cout << "Program completed successfully" << endl ;

  // Cleaning up
  CSystem::deleteConstants(  );
}// end finishSim
