#ifndef _GETMEMORY_H_
#define _GETMEMORY_H_

#include <unistd.h>
#include <ios>
#include <iostream>
#include <fstream>
#include <string>

void procMemUsage(double& vm_usage, double& resident_set);
void printMem();

#endif
