#include "system.h"
#include "molecule.h"
#ifdef __LEAK
#include "leaker.h"
#endif

double CSystem::KAPPA;
double CSystem::m_sdiel;
double CSystem::m_temp;
CPnt CSystem::BOXL;
bool CSystem::bPBC=false;

void
CSystem::initConstants(double kappa, double sdiel, double temp,
        bool bPBC_, CPnt boxl)
{
  KAPPA = kappa;
  m_sdiel = sdiel;
  m_temp = temp;
  CMolecule::initConstants(kappa,sdiel);
  bPBC = bPBC_;
  BOXL = boxl ;
}

// deletes any static array on heap
void
CSystem::deleteConstants()
{
  CMolecule::deleteConstants();
  return;
}


// return everything to central box
CPnt
CSystem::pbcPos(CPnt p)
{
  if(!bPBC) return p;

  return CPnt(p.x()-BOXL.x()*round(p.x()/BOXL.x()),
        p.y()-BOXL.y()*round(p.y()/BOXL.y()),
        p.z()-BOXL.z()*round(p.z()/BOXL.z()));
}
