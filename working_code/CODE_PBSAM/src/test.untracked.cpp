#include <complex>
#include <cmath>
#include <cstdio>
#include <vector>
#include <cstdlib>
#include <iostream>
#include <string>
#include <sys/time.h>
#include <time.h>
#include <memory>

using namespace std;

class CmyC 
{

  public:
    CmyC() { mytest = "hi!";}

    string mytest;

   vector<vector<string> > CmyC::getMolFnames()
{
  // Initializing file locs to defaults
  // pqr fname, imat path, spol path, spol name
  const char * molfns[][5] = {{"../Config/test1.pqr", "../Imat/test1/", 
                      "../Selfpol/test1", "test1_p30.0", "../Config/test1.xyz"},
                      {"../Config/test2.pqr", "../Imat/test2/", 
                      "../Selfpol/test2", "test2_p30.0", "../Config/test2.xyz"}};
  vector<vector<string> > molfnames(2, vector<string>(5)) ;

  molfnames[0][0] = "../Config/test1.pqr";
  molfnames[0][1] = "../Imat/test1";
  molfnames[0][2] = "../Selfpol/test1";
  molfnames[0][3] = "test1_p30.0";
  molfnames[0][4] = "../Config/test1.xyz";

  molfnames[1][0] = "../Config/test2.pqr";
  molfnames[1][1] = "../Imat/test2";
  molfnames[1][2] = "../Selfpol/test2";
  molfnames[1][3] = "test2_p30.0";
  molfnames[1][4] = "../Config/test2.xyz";

  for (int i=0; i<m_nType; i++)
    for (int j=0; j<m_molfnames[i].size();j++)
    {
//      m_molfnames[i][j] = molfns[i][j];
      cout << m_molfnames[i][j] << endl;
    }  

}


};

void chgTest( CmyC & cinput )
{
  cout << "Old str: " << cinput.mytest << endl;
  cinput.mytest = "bye!";
  cout << "New str: " << cinput.mytest << endl;
}


int main()
{

  CmyC newC;
  
  cout << "Main: " << newC.mytest << endl;
  chgTest(newC);
  cout << "Main New: " << newC.mytest << endl;
 
  return 0;
}
