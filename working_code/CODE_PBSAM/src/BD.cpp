#include "BD.h"
#include "constants.h"

#ifdef __LEAK
#include "leaker.h"
#endif

/*!###############################################################
 * #
 * # File: BD.cpp
 * # Date: June 2015
 * # Description: contains CBD class and its functions
 * # Author: Felberg
 * #
 * ################################################################*/

REAL CBD::MINDIST, CBD::MAXDIST;
int CBD::m_nMolType;
using namespace std;

/*!#########################################################*/
/*#########################################################*/
// Initializing the constants for the system, mainly MAX and
// MINDIST and the contact lists
/*#########################################################*/
/*#########################################################*/
void CBD::initConstants( int nMolType )
{
  if( DISTCUTOFF_TIME > CMolecule::m_interactRcutoff  )
  {
    MAXDIST = DISTCUTOFF_TIME;
    MINDIST = CMolecule::m_interactRcutoff;
  }
  else
  {
    MAXDIST = CMolecule::m_interactRcutoff;
    MINDIST = DISTCUTOFF_TIME;
  }
  m_nMolType = nMolType;

}  //! end initConstants

/*!#########################################################*/
/*#########################################################*/
// CBD constructor
// construct from array placement with input configuration
/*#########################################################*/
/*#########################################################*/
CBD::CBD( CSetup setupVals )
{
  int nType = setupVals.getType();
  resizeVecs(nType);
  for (int i=0;i<nType; i++) m_np[i] = setupVals.getTypeNCount(i);

  initMolTypes( setupVals );
  initDiffusion( setupVals );

  m_memb_thick = setupVals.getMThick(); //! thickness of PEM in METERS

  //! Memory check
  printMem();
  //! Completing initialization
  cout <<"BD init complete for "<<m_mols.size( )<<" molecules." <<endl;

}  //! end CBD constructor

/*!#########################################################*/
/*#########################################################*/
// Initialize diffusion coefficients for all species
/*#########################################################*/
/*#########################################################*/
void CBD::initDiffusion( CSetup setupVals )
{
  //! initialize diffusion parameters
  m_Dtr.resize( m_nMolType ); m_Dr.resize( m_nMolType );

  //! Translational diff coeff for molecules from input file [A^2/ps]
  //! Rotational diff coeff for molecules from input file [rad^2/ps]
  for (int i = 0; i< m_nMolType; i++)
  {
    m_Dtr[i] = setupVals.getDtr(i);
    m_Dr[i] = setupVals.getDrot(i);
    cout << "Type: " << i << " has Dtr: " << m_Dtr[i]
      << " and Drot: " << m_Dr[i] << endl ;
  }
} // end initDiffusion

/*!#########################################################*/
/*#########################################################*/
// read in and precompute values for each species
/*#########################################################*/
/*#########################################################*/
void CBD::initMolTypes( CSetup setupVals )
{
  vector<vector< vector<int> > > intraPolLists_far(m_nMolType);
  m_mols.clear( ); double idiel = setupVals.getIDiel();

  for (int i=0; i<m_nMolType; i++)
  {
    string pqrfile  = setupVals.getTypeNPQR(i);
    string imatpath = setupVals.getTypeNMatDir(i);
    string exppath  = setupVals.getTypeNPolDir(i);
    string expname  = setupVals.getTypeNPolExp(i);

    //! read in molecule positions, charge + centers
    vector<CPnt> scen, POS;
    vector<double> srad, CHG;
    readpqr( pqrfile, POS, CHG, srad, scen );
    //! Initializing the centers
    int ncen = scen.size(  );
    //! Computing the center of mass for each molecule
    m_initialrcen[i] = computeAtomCenter( POS );

    //!////////////// read in solved expansions //////////////
    readMats( m_iMats[i], N_POLES, imatpath, ncen );
    REAL intraRcutoff_dum;
    readExpansionsP( m_iF[i], m_iH[i], N_POLES, exppath, expname,
                    ncen, intraRcutoff_dum );

    //!///////////// generate values for each moltype ///////////////
    //! What is intraRutoff ??
    const double intraRcutoff = 30;  //! Angstroms

    //! initiate molecules?
    CMolecule::generateAll( scen, srad, m_SPxes[i],
           m_nSPx[i], m_neighs[i], m_iF[i], m_iH[i],
           m_qSolvedF[i], m_qSolvedH[i], m_totalF[i], m_totalH[i],
           m_initialrcen[i], m_molcell_all[i], intraRcutoff,
           m_intraPolLists_near[i], intraPolLists_far[i], CHG, POS, idiel,
           m_LFs_intraSelf[i], m_LHs_intraSelf[i],
           m_LFs_intraSelf_far[i], m_LHs_intraSelf_far[i] );

    for( int j=0; j < m_np[i]; j++ )
    {
      m_mols.push_back(  new CMolecule( i, m_initialrcen[i], scen,  srad,
             CHG, POS, idiel, m_iMats[i], intraRcutoff,
             m_SPxes[i], m_nSPx[i], m_neighs[i], m_intraPolLists_near[i],
             m_iF[i], m_iH[i], m_LFs_intraSelf[i], m_LHs_intraSelf[i],
             m_LFs_intraSelf_far[i], m_LHs_intraSelf_far[i],
             m_qSolvedF[i], m_qSolvedH[i],
             m_totalF[i], m_totalH[i], m_molcell_all[i] ));
    }
  }
  //! check to make sure we have the correct system size
  int moltot = 0;
  for (int j=0;j<m_nMolType; j++) moltot+=m_np[j];
  assert( m_mols.size( ) == moltot );

  //! =========================================================
  const REAL interRCutoff = 10; //! <<<<<<<<<<<<<<<<<<<<<<<<
  const REAL interactRCutoff = 100; //! <<<<<<<<<<<<<<<<<<<<<<<<
  //! Initialize mutual constants
  CMolecule::initMutualConstants(m_mols,interRCutoff,interactRCutoff,true);
  cout << "interRcutoff = "<<interRCutoff<<endl;
  cout << "interactRcutoff = "<<interactRCutoff<<endl;
  //! =========================================================

  return;
}  //! end CBD::initMolTypes

/*!#########################################################*/
/*#########################################################*/
// A function that sets the configuration of the system
/*#########################################################*/
/*#########################################################*/
void CBD::setconfig( CSetup setupVals, CPnt edge )
{
  int mmolCount = 0;
  //! center the polymer
  for (int i = 0; i<m_nMolType; i++)
  {
    string config = setupVals.getTypeNXYZ(i); vector<CPnt> cen;
    cout << "Reading XYZ file: " << config << endl;
    readXYZ(config, m_np[i], cen);

    for( int j=0; j<m_np[i]; j++ )
    {
      m_mols[mmolCount]->setPos(  cen[j]  );
      m_mols[mmolCount]->setOrient(   CQuat( 1.0, CPnt(0.0, 0.0, 0.0 ))  );
      mmolCount++;
    }
  }

  //! check that nothing collides ( use direct method instead of cell )
  for( int i = 0; i<m_mols.size(); i++ )
    if(  m_mols[i]->isCollided( m_mols ) )
    {
      cout << "Error : initial config clashes " << endl;
      exit(0);
    }
  return;
}  //! end setconfig



//!<#########################################################
//!<#########################################################
//!< printPot
//!< reads in a list of z coordinates
//!< prints the ESP in the xy plane
//!<#########################################################
//!<#########################################################
// could make this more flexible for xz and yz axes
void CBD::printPot( CPnt boxl , string zlist )
{
  char az[] = "z";
  char potfile[MAX_CHARNUM];
  vector<double> zpos;


///*
  vector<CPnt> points;
  string potLst = "/home/lfelberg/PBSAM/working_code/CODE_PBSAM/test/devel/BD/pot.xyz";
  readXYZ(potLst, 50, points);
  sprintf(potfile, "pot_%d.out", 0); 
  ofstream potfiles;
  potfiles.open(potfile);
  for (int i=0; i<points.size();i++)
  {
    potfiles << points[i] << "   " <<CMolecule::computePotInSpace(m_mols,points[i]) << endl;
  }
  potfiles.close();
//*/
/*readZlist( zlist , zpos ) ;
  for ( int i = 0 ; i < zpos.size() ; i++ )
  {
    double z = zpos[i] ;
    sprintf( potfile, "pot_%s%d.out", az, (int)z); // MES change this to double
    ofstream potfile2;
    potfile2.open(potfile);
    potfile2 << boxl.x() << " " << boxl.y() << " " << boxl.z() << endl ;

    for( double x=-0.5*boxl.x(); x<0.5*boxl.x(); x=x+1.0 )
    {
      for( double y=-0.5*boxl.y(); y<0.5*boxl.y(); y=y+1.0 )
      {
        potfile2 << CMolecule::computePotInSpace(m_mols,CPnt(x,y,z))<<"\t" ;
      }
      potfile2  << endl;
    }
    potfile2.close();
  } // end i
*/
} // end  printPot

/*!#########################################################*/
/*#########################################################*/
// diffusionSim
// for a BD sim that is pure diffusion
/*#########################################################*/
/*#########################################################*/
void CBD::diffusionSim(int nmolecules,
                         vector<CPnt> & forc, vector<CPnt> & torq)
{
  for( int i=1; i<nmolecules; i++ )
  {
    forc[i] = CPnt ( 0.0, 0.0, 0.0 );
    torq[i] = CPnt ( 0.0, 0.0, 0.0 );
  }
} //end diffusionSim

/*!#########################################################*/
/*#########################################################*/
// computeFMpol
// Compute electrostatic forces due to mutual polarization
/*#########################################################*/
/*#########################################################*/
void CBD::computeFMpol(int nmols, bool bComputeForce,
                      vector<CPnt> & forc, vector<CPnt> & torq,
                      vector<double> & intEnergy )
{
  if( bComputeForce )
  {
    bool bBlown = CMolecule::computeForces( m_mols, forc, torq , intEnergy );
    if( !bBlown )
    {
      cout <<"died somewherein computeforces"<<endl;
      CMolecule::saveConfig( "died.config", m_mols );
      CMolecule::writeMolsPQR( "died.pqr", m_mols );
      exit( 1 );
    }
  }
} // end computeFMpol

/*!#########################################################*/
/*#########################################################*/
// computeFText
/*#########################################################*/
/*#########################################################*/
void CBD::computeFText(int nmols, double voltage_drop, double IKbT,
                      double fact2, vector<CPnt> & forc, vector<CPnt> & torq)
{
  //! Computing Efield from potential drop across membrane
  //Force on molecule in J/Ang
  double E_ext_x = ( ( voltage_drop/m_memb_thick )*ANGSTROM*ELECT_CHG );
//  cout << "External field : " << E_ext_x << endl ;
  CPnt E_ext = CPnt ( 0.0, 0.0, E_ext_x );    //! Field parallel to potential,
  //! only in Z direction [J/A]*[1/J]

  for(int i=1; i<nmols; i++)
  {
    CPnt torque_sum = CPnt( 0.0, 0.0, 0.0 );
    for( int j=0; j<m_mols[i]->getNKS( ); j++)
    {
      //! Extracting charge information and positions
      CSolExpCenter current_solexpcen = m_mols[i]->getKS( j );
      vector <CPnt> pos = current_solexpcen.getSPx( );
      const REAL*  charge = current_solexpcen.getQSolvedHself( );
      //! for each charge
      for( int k=0; k<pos.size( ); k++)
      {
        //! Reorienting old position to new and
        //! crossing that with the field force at that point
        //! UNITS:  [=] [A] x ( e*[J/A*e] ) [=] J/rad
        torque_sum +=  cross( m_mols[i]->getOrient()*pos[k],
                             ( ( *( charge+k )*E_ext )  ) );
      } //! end k
    } //! end j

    forc[i]+= (IKbT/fact2)*(m_mols[i]->getChgSum())*E_ext;
    torq[i]+=((1.0/COUL_K)*torque_sum);
  }
  return;
} // end computeFText

/*!#########################################################*/
/*#########################################################*/
// translate mols
/*#########################################################*/
/*#########################################################*/
bool CBD::translate( vector<CPnt> dR, double dt, CPnt & extra )
{
  bool bTranslated; CPnt dR_;
  int maxc = 500; //! stay put if it doesnt move after maxc trials

	//! the polymer is fixed, only move protons ( S. Liu 03/14/2013 )
	for( int i=1; i<m_mols.size( ); i++)
	{
		int c=0; int m = m_mols[i]->getMolType(  );
		bTranslated = false;
		c = 0;
		while( !bTranslated && c < maxc )
		{
			c++;
			dR_ = dR[i];
#ifndef __NODIFF
			extra = CBD::getRandVec( sqrt(2*dt*m_Dtr[m] ));
			dR_ += extra;
#endif
			m_mols[i]->translate( dR_ );

#ifdef __CELL__
			bool bCollided =  m_mols[i]->isCollided_cell( m_mols );
#else
			bool bCollided =  m_mols[i]->isCollided( m_mols );
#endif

			if( bCollided )
				m_mols[i]->untranslate(  );
			else
				bTranslated = true;
		}
  }
  return bTranslated;
} // end translate


/*!#########################################################*/
/*#########################################################*/
// rotate mols
/*#########################################################*/
/*#########################################################*/
bool CBD::rotate( vector<CPnt> dO, double dt  )
{
  bool bRotated; CPnt dO_; CQuat Q;
  int maxc = 500; //! stay put if it doesnt move after maxc trials

	//! the polymer is fixed, only rotate protons ( S. Liu 03/14/2013 )
	for( int i=1; i<m_mols.size( ); i++)
	{
    bRotated = false;
		int c=0; int m = m_mols[i]->getMolType(  );
    while( !bRotated && c < maxc )
    {
      c++;
      dO_ = dO[i] + CBD::getRandVec( sqrt(2*dt*m_Dr[m] ));
      Q = CQuat( dO_, dO_.norm( ));

#ifdef __CELL__
      if( ! m_mols[i]->willRotCollide_cell(m_mols, Q ) )
#else
      if( ! m_mols[i]->willRotCollide(m_mols, Q ) )
#endif
      {
        m_mols[i]->rotate( Q );
        bRotated = true;
      }
    }
  }
  return bRotated;
} // end rotate

/*!#########################################################*/
/*#########################################################*/
// bd run
/*#########################################################*/
/*#########################################################*/
double CBD::runBD( int traj_no, CSetup setupVals, CPnt boxl )
{
  double d_voltage = setupVals.getDVolt();
  int nmol = m_mols.size(  );//! number of molecules
  double t = 0.0; int istep = 0; bool btravel = false;

  //! Writing out initial configurations and starting
  //! the timer for the simulation run
  CMolecule::writeMolsPQR( "initial.pqr", m_mols );
  double start = read_timer(  );

  if( setupVals.getRunType() == "pot" )
  {
    if( nmol != 1 )
    {
      bool bInterXFS =  CMolecule::generateInterXFormsForPolarize_LowMemory(m_mols);
      if( !bInterXFS )
        cout <<"error in generateInterXFormsForPolarize_LowMemory"<<endl;
      CMolecule::polarize_mutual(m_mols, true, 1000);  // no gradient
    }
    string zlist = setupVals.getZlist() ;
    printPot( boxl , zlist );
    return 0;
  }

  char traj_file[MAX_CHARNUM]; ofstream traj;
  sprintf( traj_file, "traj_%d.xyz", traj_no);
  traj.open( traj_file );
  vector<CPnt> ion_pos;

  while( !btravel && istep < 1) //ssetupVals.getMaxTime() )
  {
    REAL minDist = computeMinSepDist(  );
    bool bWrite = ( istep % WRITEFREQ == 0 );

    //! Propagate the system, dt = simtime for each step
    REAL dt = propagate( minDist, bWrite, d_voltage,
                  setupVals.getIKbT(), setupVals.getFACT2() );
    t += dt;
    if( bWrite )
      //! Write out step timings and an xyz file of system
    {
      cout << istep << ") Timings " << t << endl;
      for( int i = 1; i < nmol; i++ )
			  ion_pos.push_back(m_mols[i]->getRCen());
      writeXYZ(traj, dt, WRITEFREQ, ion_pos);
    }

//*  For sim to die with first passage
    for( int i = 1; i < nmol; i++ ) //! Checking to see if an ion has left
      if( Isthrough(m_mols[i], CSystem::BOXL ))
      {
        btravel = true;
        for( int j = 1; j < nmol; j++ )
			    ion_pos.push_back(m_mols[j]->getRCen());
        writeXYZ(traj, t, WRITEFREQ, ion_pos);
        break;
      }  //! die if an ion has left the pore
//*/
    istep++; ion_pos.clear();
  } //! end while
  writeSim(start, t);
  return t * PS_TO_NS;
}  //! end runBD

/*!#########################################################*/
/*#########################################################*/
// Function to propagate the system with BD
/*#########################################################*/
/*#########################################################*/
double CBD::propagate( double minDist, bool bWriteTime,
             double voltage_drop, double IKbT, double fact2 )
{
  //! compute minimum distance to decide timestep and
  //! whether to compute forces
  bool bComputeForce = ( minDist < CMolecule::m_interactRcutoff  ) ;
  double dt = compute_dt( minDist );
  const int nmol = m_mols.size(  );
  vector<CPnt> force(nmol, CPnt(0.0,0.0,0.0)), torque(nmol, CPnt(0,0,0));
  vector<CPnt> dR(nmol), dO(nmol);

#ifndef __DIFF
  double t1 = read_timer(  );
  vector<double> intEnergy(nmol) ;
  computeFMpol( nmol, bComputeForce, force, torque, intEnergy);
#else
  diffusionSim(nmol, force, torque, dR, dO);
#endif

  double t2 = read_timer(  );
  //! move molecules
  for( int i=1; i<nmol; i++ )
  {
    int m = m_mols[i]->getMolType(  );
    if( voltage_drop != 0.0 )
      computeFText(nmol,voltage_drop, IKbT, fact2, force, torque);
    //! Computing dR from the Fext and the self polarization
    dR[i] = ( m_Dtr[m]*dt*( fact2*force[i] ));
    dO[i] = (  m_Dr[m]*dt*( fact2*torque[i] ) );
    //! Computing dO from the Fext and self polarization
    //!   Dr * dt  * IkBT  * ( Internal + Tor_ext )
    //! [=] [rad^2/ps] * [ps]  * [1/J] * ( Int*[coul_K] + [J/rad] )
    //! [=] [rad/ps]
    if( bWriteTime ) //! Write out step statistics
    {
      cout << "Molecule "<< nmol << " Disp, Force all: " <<  dR[i] << endl ;
      cout << "Interaction Energy = " << intEnergy[1]*COUL_K*JtoKCALperMOL << " kcal/mol" << endl ;
    }
  }//! end-i

  CPnt rand_dr = makeMove( dR, dO, dt );
  double t3 = read_timer(  );

  if( bWriteTime ) //! Write out step statistics
    cout << " Disp, random: " << rand_dr << endl;
  return dt;
}  //! end propagate

/*!#########################################################*/
/*#########################################################*/
// Function makeMove, will move the system forward
/*#########################################################*/
/*#########################################################*/
CPnt CBD::makeMove( const vector<CPnt> & dR,
    const vector<CPnt> & dO, REAL dt )
{
  int maxc = 500; //! stay put if it doesnt move after maxc trials
  CPnt extra = CPnt(0.0,0.0,0.0);

  bool bTranslated = translate(dR, dt, extra);
  bool bRotated = rotate( dO, dt );

  if  ( ( bRotated == false ) && ( bTranslated == false )
           && ( m_mols.size(  ) == 2 ) )
  {
    cout << "Molecule stuck, exiting trajectory" << endl;
    exit(0);
  }

  return extra;
}  //! end makeMove

/*!#########################################################*/
/*#########################################################*/
// For computing the separation distance between molecules
/*#########################################################*/
/*#########################################################*/
REAL CBD::computeMinSepDist(  )
{
  double minsepdist = DBL_MAX; double minM2Msepdist = DBL_MAX;
  const int nmol = m_mols.size(  ); bool bTrackingS2S = false;

  for( int i=0; i<nmol; i++ )
    for( int j=i+1; j<nmol; j++ )
    {
      double dM2M = CMolecule::getSepDist( m_mols[i], m_mols[j] );
      //! first check if they are close enough that we care about
      if( dM2M < MAXDIST  )
      {
        if( !bTrackingS2S ) bTrackingS2S = true;
        //! evaluate it until it reaches the small limit that we care about
        for( int ki=0; ki<m_mols[i]->getNKS( ); ki++)
          for( int kj=0; kj<m_mols[j]->getNKS( ); kj++)
          {
            double d = CMolecule::getSepDist(m_mols[i],ki,m_mols[j],kj);
            if(  d < minsepdist  )
            {
              if(  d < MINDIST  ) { return MINDIST; }
              else minsepdist = d;
            }
          }//! end kj
      }
      //! otherwise keep track of mol-to-mol distance just in case
      else if ( !bTrackingS2S )
      {
        if( dM2M < minM2Msepdist ) minM2Msepdist  = dM2M;
      }
    }//!end ( i,j )

  if( bTrackingS2S ) return  minsepdist;
  else  return ( minM2Msepdist > 0 ?  minM2Msepdist : 0 );
}  //! end computeMinSepDist


/*!#########################################################*/
/*#########################################################*/
// For cleaning up memory in CBD class
/*#########################################################*/
/*#########################################################*/
void
CBD::cleanupCBD()
{
/*
    for (int ii = 0 ; ii < m_mols[ii]->getNKS() ; ii++ )
    {
     delete [] m_iMats[ii] ;
     delete m_iF[ii] ;
     delete m_iH[ii] ;
     delete m_LFs_intraSelf[ii] ;
     delete m_LHs_intraSelf[ii] ;
     delete m_LFs_intraSelf_far[ii] ;
     delete m_LHs_intraSelf_far[ii] ;
    }
*/
  CMolecule::cleanupCMol(m_mols) ;
  for ( int jj = 0 ; jj < m_mols.size() ; jj++ )
  {
//    delete m_mols[jj] ;
  }
}

