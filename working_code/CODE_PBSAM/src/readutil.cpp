#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <cstring>
#include "readutil.h"
#include "util.h"
#ifdef __LEAK
#include "leaker.h"
#endif

/*!#########################################################*/
/*#########################################################*/
// split
// Split a input line and return a vector
/*#########################################################*/
/*#########################################################*/
vector<string> split(string str, char delimiter)
{
  vector<string> internal;
  stringstream ss(str); // Turn the string into a stream.
  string tok;

  while(getline(ss, tok, delimiter))
    internal.push_back(tok);

  return internal;
} // end split


/*!#########################################################*/
/*#########################################################*/
// findLines
// Read an input line and break up if not blank
/*#########################################################*/
/*#########################################################*/
void findLines(string fline, CSetup & inputRead)
{
  if (!fline.empty())
    findKeyword( split(fline, ' '), inputRead);

} // end findLines

/*!#########################################################*/
/*#########################################################*/
// findKeyword
// Read an input line and determine relevant parameters
/*#########################################################*/
/*#########################################################*/
void findKeyword(vector<string> fline, CSetup & inputRead)
{
  string keyword = fline[0];

  if (keyword == "system")
  {
    cout << "System command found" << endl;
    if(fline[1]=="nafion" || fline[1]=="multi" || fline[1] == "nam" )
    {
      inputRead.setSimType( fline[1] );
    }
    else
       cout << "Option not available, using default" << endl;

  } else if (keyword == "runname")
  {
    cout << "Runname command found" << endl;
    inputRead.setRunName( fline[1] );
  } else if (keyword == "runtype")
  {
    cout << "Runtype command found" << endl;
    if(fline[1]=="sim" || fline[1]=="pot" || fline[1]=="mba" ||
            fline[1]=="spol" || fline[1] =="imat" )
    {
      inputRead.setRunType( fline[1] );
      if (fline[1] == "sim")
      {
        if (fline.size() > 2)
          inputRead.setNTraj( atoi( fline[2].c_str() ) );
        if (fline.size() > 3)
          inputRead.setMaxTime( atoi( fline[3].c_str() ));
      }
      else if ( fline[1] == "mba" )
      {
        inputRead.setLevel( atoi( fline[2].c_str() ) ) ;
      }
      else if ( fline[1] == "pot" )
      {
        inputRead.setZfile( fline[2].c_str() ) ;
      }
    }
    else
       cout << "Option not available, using default" << endl;
  } else if (keyword == "omp")
  {
    cout << "OMP command found" << endl;
    inputRead.setOMP(atoi(fline[1].c_str()));
  } else if (keyword == "pbc")
  {
    cout << "PBC command found" << endl;
    inputRead.setPBCT( atoi(fline[1].c_str()) );
    if ( inputRead.getPBCs() > 0 )
      inputRead.setBoxl(atof(fline[2].c_str()));
  } else if (keyword == "salt")
  {
    cout << "Salt command found" << endl;
    inputRead.setSaltCon( atof(fline[1].c_str()) );
  } else if (keyword == "temp")
  {
    cout << "Temperature command found" << endl;
    inputRead.setTemp( atof(fline[1].c_str()) );
  } else if (keyword == "idiel")
  {
    cout << "Interior dielectric command found" << endl;
    inputRead.setIDiel( atof(fline[1].c_str()) );
  } else if (keyword == "sdiel")
  {
    cout << "Solvent dielectric command found" << endl;
    inputRead.setSDiel( atof(fline[1].c_str()) );
  } else if (keyword == "attypes")
  {
    cout << "Atom Types command found" << endl;
    inputRead.setNType( atoi(fline[1].c_str()) );
    inputRead.resizeVecs();
    cout << "done with attypes" << endl;
  } else if (keyword == "type")
  {
    cout << "Type def command found" << endl;
    int typeNo = atoi(fline[1].c_str())-1;
    if (typeNo > inputRead.getType()-1)
      return;
    if (fline.size() > 2)
      inputRead.setTypeNCount( typeNo, atoi(fline[2].c_str()) );
    if (fline.size() > 3)
    {
      inputRead.setTypeNDef( typeNo, fline[3].c_str() );
      if (inputRead.getTypeNDef(typeNo) == "rot" ||
           inputRead.getTypeNDef(typeNo) == "move")
        inputRead.setTypeNDrot( typeNo, atof(fline[5].c_str()));
      if (inputRead.getTypeNDef(typeNo) == "move")
        inputRead.setTypeNDtr( typeNo, atof(fline[4].c_str()));
    }
  } else if (keyword == "pqr")
  {
    cout << "PQR command found" << endl;
    int typeNo = atoi(fline[1].c_str())-1;
    if (typeNo > inputRead.getType()-1)
      return;
    inputRead.setTypeNPQR( typeNo, fline[2].c_str() );
  } else if (keyword == "imat")
  {
    cout << "IMAT command found" << endl;
    int typeNo = atoi(fline[1].c_str())-1;
    if (typeNo > inputRead.getType()-1)
      return;
    inputRead.setTypeNImat( typeNo, fline[2] );
  } else if (keyword == "spolDir")
  {
    cout << "SelfpolDir command found" << endl;
    int typeNo = atoi(fline[1].c_str())-1;
    if (typeNo > inputRead.getType()-1)
      return;
    inputRead.setTypeNSpolDir( typeNo, fline[2] );
  } else if (keyword == "spolExt")
  {
    cout << "SpolName command found" << endl;
    int typeNo = atoi(fline[1].c_str())-1;
    if (typeNo > inputRead.getType()-1)
      return;
    inputRead.setTypeNExp( typeNo, fline[2] );
  } else if (keyword == "xyz")
  {
    cout << "XYZ command found" << endl;
    int typeNo = atoi(fline[1].c_str())-1;
    if (typeNo > inputRead.getType()-1)
      return;
    inputRead.setTypeNXYZ( typeNo, fline[2] );
  } else if (keyword == "f_ext")
  {
    cout << "External force command found" << endl;
    inputRead.setFExt( atof(fline[1].c_str()), atof(fline[2].c_str()));
  } else if (keyword == "random")
  {
    cout << "RNG Seed command found" << endl;
    inputRead.setRand( atoi(fline[1].c_str()) );
  } else
    cout << "Keyword not found, read in as " << fline[0] << endl;
} // end findKeyword

/*!#########################################################*/
/*#########################################################*/
// readInputFile
// Read an input file and return parameters for various words
/*#########################################################*/
/*#########################################################*/
void readInputFile(char * fname, CSetup & readIn)
{
  cout << "Reading Input file " << fname << endl ;
  ifstream fin(fname);
  if (!fin.is_open())
  {
    cout << "Could not open file " << fname << endl;
    exit(0);
  }

  string inputLine;
	vector<vector <string> > keywordLines;
  getline(fin,inputLine);

  while (!fin.eof())
  {
    findLines(inputLine, readIn);
    getline(fin, inputLine);
  }
} // end readInputFile



/*!#########################################################*/
/*#########################################################*/
//!  computeAtomCenter function
/*! A function to compute the center
 of geometry for a set of coordinates
 \param pos a vector of atom positions */
/*#########################################################*/
/*#########################################################*/
CPnt computeAtomCenter(const vector<CPnt> &pos)
{
  CPnt cen;
  for(int i=0; i<pos.size(); i++) cen += pos[i];
  cen /= pos.size();
  return cen;
}


/*!#########################################################*/
/*#########################################################*/
//!  computeMaxRadius_ByAtom function
/*! A function to determine the furthest atom
 from a sphere center with a given tolerance
 \param pos a vector of atom positions
 \param cen a CPnt object for a CG sphere
 \param sphereTol a floating point of tolerance for maxRad */
/*!#########################################################*/
/*#########################################################*/
REAL computeMaxRadius_ByAtom(const vector<CPnt> &pos, CPnt cen, REAL sphereTol)
{
  REAL maxR = 0.0;
  for(int i=0; i<pos.size(); i++)
  {
    REAL distsq = (pos[i]-cen).normsq();
    if(distsq > maxR) maxR = distsq;
  }

  maxR = sqrt(maxR) + sphereTol;
  return maxR;
}

/*!#########################################################*/
/*#########################################################*/
//!  readpqr function
/*! A function to read in a PQR file that contains both atoms
 and CG spheres
 \param fname a character string to read in centers from
 \param pnt a vector of xyz coordinate objects to store coordinates in
 \param ch a vector of atom partial charges
 \param rad a vector of floating points of center radii
 \param cen a vector of CG sphere xyz coordinates */
/*!#########################################################*/
/*#########################################################*/
void readpqr( string fname, vector<CPnt> & pnt, vector<REAL> & ch,
              vector<REAL> & rad, vector<CPnt> & cen)
{
  cout << "Reading PQR file " << fname << endl ;
  ifstream fin(fname.c_str());
  if (!fin.is_open())
  {
    cout << "Could not open file " << fname << endl;
    exit(0);
  }

  char buf[100], temp[10];
  REAL sum = 0.0;
  vector<REAL> R;
  pnt.clear(); ch.clear(); rad.clear(); cen.clear();
  fin.getline(buf,99);
  int iCoord, iCen;

  // center files
  iCen = 18;
  iCoord = 31;
  while (!fin.eof())
  {
    double x,y,z,c,r;
    if (strncmp(&(buf[0]),"ATOM",4) == 0)
    {
      sscanf(&(buf[iCoord]), "%lf %lf %lf %lf %lf", &x, &y, &z, &c, &r);
      // read in as centers that specifies dielectric boundary
      if (string(buf).find(" CEN ") != string::npos)
      {
        rad.push_back(r);
        cen.push_back(CPnt(x,y,z));
      }

      // read in as atoms
      else
      {
        pnt.push_back(CPnt(x,y,z));
        ch.push_back(c);
        R.push_back(r);
        sum += c;
      }
    }

    fin.getline(buf,99);
  }
  cout << fname << ": atoms: " << ch.size() << ", net charge: "
  << sum <<endl;

  REAL sphereTol = 2.0;
  CPnt rcen = computeAtomCenter(pnt);
  REAL maxR = computeMaxRadius_ByAtom(pnt, rcen, sphereTol);
}

/*!#########################################################*/
/*#########################################################*/
//! readXYZ function
/*! A function to grab XYZ coordinates from XYZ file
  \param fname, a xyzfilename
  \param a vector of positions  */
/*!#########################################################*/
/*#########################################################*/
void readXYZ( string fname, int nmols, vector<CPnt> & pnt)
{
  string s; REAL px,py,pz; pnt.clear();

  ifstream fin( fname.c_str() );
  if ( !fin.is_open( ))
  {
    cout << "Could not open ion config file " << endl;
    exit(0);
  }

  //! Checking to ensure that there are enough coordinates
  //! for each ion in the system
  for( int i=0;i<nmols;i++ )
  {
    if( fin.eof( ))
    {
      cout << "There should be more lines in XYZ file" << endl;
      exit(0);
    }

    //! getting the next line of the xyz file
    getline( fin, s );
    istringstream ss( s );
    ss >> px >> py >> pz;
    pnt.push_back(  CPnt( px,py,pz ) );
  }
}// end readXYZ

/*!#########################################################*/
/*#########################################################*/
//! writeXYZ function
/*! A function to print XYZ coordinates to XYZ file
  \param fname, a xyzfilename
  \param a vector of positions  */
/*!#########################################################*/
/*#########################################################*/
void writeXYZ(ofstream & traj, double dt, int writefreq, vector<CPnt> & pnt)
{
  traj << pnt.size() << endl;
  traj << " Atom \t dt = " << dt
       << "   Write freq = " << writefreq << endl;
  for (int i=0;i<pnt.size();i++)
    traj << " 8 " << pnt[i]  <<  endl;
}// end writeXYZ


/*!#########################################################*/
/*#########################################################*/
//!  printM function
/*! A function to print out entries of a matrix
 \param A matrix to print
 \param m number of rows of the matrix
 \param n number of columns of the matrix */
/*!#########################################################*/
/*#########################################################*/
void printM(double * A, int m, int n)
{
  for (int i = 0; i < m; i++)
  {
    for (int j = 0; j < n; j++)
      cout << A[j*m+i] << " ";
    cout << endl;
  }
  cout << endl;
}

/*!#########################################################*/
/*#########################################################*/
//!  writeMat_Unformatted function
/*! A function to write out an unformatted matrix
 \param mat a matrix to print
 \param p number of poles of the matrix
 \param fname a file to write the matrix out to */
/*!#########################################################*/
/*#########################################################*/
void writeMat_Unformatted(double *mat, int p, char* fname)
{
  ofstream fout;
  fout.open(fname, ofstream::binary); //for writing
  if (!fout)
  {
    cout << "file "<< fname << " could not be opened."<< endl;
    exit(1);
  }
  int length = p*p*p*p;
  fout.write( reinterpret_cast<char const *> (&p), sizeof(p)); // pole order
  fout.write( reinterpret_cast<char const *>(mat),
                  length*sizeof(double) ); //mat

  fout.close();
}

/*!#########################################################*/
/*#########################################################*/
//!  readMat_Formatted function
/*! A function to read in a formatted matrix
 \param mat a matrix to read into
 \param maxp number of poles of the matrix
 \param fname a file to read the matrix from */
/*!#########################################################*/
/*#########################################################*/
void readMat_Unformatted(double *mat, int maxp, char* fname)
{
  const int psq = maxp * maxp;
  const int pquad = psq * psq;

  ifstream fin;
  fin.open(fname, ifstream::binary); //for reading
  if(!fin)
  {
    cout <<"Error reading Mat:  cannot open "<<fname<<endl;
    exit(1);
  }
  // read pole order
  int p;
  fin.read( reinterpret_cast<char*> (&p), sizeof(int));
  if( p < maxp )
  {
    cout <<"Error reading Mat:  read pole "<<p
            <<", should be at least N_POLES = "<<maxp<<endl;
    exit(1);
  }
  // read mat
  if(p==maxp) fin.read( reinterpret_cast<char*>(mat), pquad*sizeof(double));
  else
  {
    int matlen = p*p;
    streamsize readsize = psq*sizeof(double);
    streamsize skipsize = ( matlen - psq )*sizeof(double);

    for(int j=0; j<psq; j++)
    {
      fin.read( reinterpret_cast<char*>(mat + j*psq), readsize);
      fin.seekg( skipsize, ios::cur);
    }
  }
  fin.close();
  return;
}

/*!#########################################################*/
/*#########################################################*/
//!  readMat_Formatted function
/*! A function to read in a formatted matrix
 \param mat a matrix to read into
 \param maxp number of poles of the matrix
 \param fname a file to read the matrix from */
/*!#########################################################*/
/*#########################################################*/
void readMat_Formatted(double *mat, int maxp, char* fname)
{
  cout << "Reading Formatted imat from "<<fname<<endl;
  const int psq = maxp * maxp;
  const int pquad = psq * psq;

  ifstream fin(fname);
  if(!fin)
  {
    cout <<"Error reading Mat:  cannot open "<<fname<<endl;
    exit(1);
  }
  // pole order
  int p;
  fin >> p ;
  if( p < maxp)
  {
    cout <<"Error reading Mat:  read pole "<<p
               <<", should be at least N_POLES = "<<maxp<<endl;
    exit(1);
  }
  if(p==maxp)
  {
    for(int k=0; k<pquad; k++)
      fin >> mat[k];
  }
  else
  {
    int matlen = p*p;
    double dummy;

    for(int j=0; j<psq; j++)
    {
      int k = j * psq;
      int i;
      for(i=0; i<psq; i++, k++)
        fin >> mat[k];
      for(i=psq; i<matlen; i++)
        fin >> dummy;
    }
  }
  fin.close();
  return;
}

/*!#########################################################*/
/*#########################################################*/
//!  readMats function
/*! A function to read in an unformatted matrix
 \param iMats an array of matrices to read into
 \param p number of poles of the matrix
 \param imatpatj a path to the files to read imats from
 \param ncen the number of CG centers. Will have 1 file for each */
/*!#########################################################*/
/*#########################################################*/
void readMats(vector<REAL*> &iMats, int p, string imatpath, int ncen)
{
  iMats.resize(ncen);
  int pquad = p*p*p*p;

  for(int i=0; i<ncen; i++)
  {
    iMats[i] = new REAL[pquad];
    char fname[MAX_CHARNUM];
    int n = sprintf(fname, "%s/imat.sp%d.out.bin", imatpath.c_str(),i);
    assert(n <= MAX_CHARNUM);
    readMat_Unformatted(iMats[i],p, fname);
  }
}

/*!#########################################################*/
/*#########################################################*/
//!  writeMat_Formatted function
/*! A function to write out a formatted matrix
 \param mat a matrix to read into
 \param maxp number of poles of the matrix
 \param fname a file to write the matrix to */
/*!#########################################################*/
/*#########################################################*/
void writeMat_Formatted(double *mat, int p, char* fname)
{
  ofstream fout(fname);
  int len = p*p;
  fout <<p<<endl; // pole order

  for(int j=0; j<len; j++)
  {
    int k = j*len;
    for(int i=0; i<len; i++, k++)
      fout <<mat[k]<<" ";

    fout<<endl;
  }

  fout.close();
  return;
}

/*!#########################################################*/
/*#########################################################*/
//!  writeExpansion function
/*! A function to write out a multipole expansion
 \param E an expansion object to write out to file
 \param rcut a floating point of the cutoff of the expansion
 \param fname a file to write the expansion to */
/*!#########################################################*/
/*#########################################################*/
void writeExpansion(const CExpan &E, REAL rcut, char* fname)
{

  ofstream fout(fname);
  fout.precision(8);

  fout <<E.getRange().p2()<<endl; // pole order
  fout <<E.getScale()<<endl;
  fout <<rcut<<endl;
  fout<<E<<endl;

  fout.close();
  return;
}


/*!#########################################################*/
/*#########################################################*/
//!  readExpansion function
/*! A subfunction to read in a multipole expansion
 \param E an expansion object to read in from file
 \param maxp number of poles of the expansion
 \param fname a file to read the expansion from
 \param scale a floating point of the scaling factor of the expansion
 \param rcut a floating point of the cutoff of the expansion */
/*!#########################################################*/
/*#########################################################*/
void readExpansion(vector<double> &V, int maxp, char* fname,
            REAL & scale, REAL &rcut)
{
  const int psq = maxp * maxp;

  V.clear();
  V.resize(psq);

  ifstream fin(fname);
  fin.precision(8);

  if(!fin)
  {
    cout <<"Error reading expansion:  cannot open "<<fname<<endl;
    exit(1);
  }
  // pole order
  int pRead;
  fin >> pRead ;
  fin >> scale;
  fin >> rcut;

  if( pRead < maxp)
  {
    cout <<"Warning : reading expansion:  read pole "
             <<pRead<<", padding zeros to N_POLES = "<<maxp<<endl;
    int psqRead = pRead*pRead;
    for(int k=0; k<psqRead; k++) fin >> V[k];
    for(int k=psqRead; k<psq; k++) V[k] = 0.0;
  }
  else
  {
    for(int k=0; k<psq; k++)  fin >> V[k];
  }
  fin.close();
  return;
}

/*!#########################################################*/
/*#########################################################*/
/*!#########################################################*/
/*#########################################################*/
void readExpansions(vector<CMulExpan> &iF, vector<CMulExpan>  &iH,
       int p, char* exppath, char* runname, int ncen, REAL & intraRcutoff)
{
  iF.resize(ncen);
  iH.resize(ncen);

  vector<double> V;
  REAL scale;

  for(int ki=0; ki<ncen; ki++)
  {

    char fname1[MAX_CHARNUM], fname2[MAX_CHARNUM];
    int n;
    REAL rcut;

    n = sprintf(fname1, "%s/%s.%d.F.exp", exppath, runname, ki);
    assert(n <= MAX_CHARNUM);
    readExpansion(V, p, fname1, scale, rcut);
    if(ki==0) intraRcutoff = rcut;
    else assert( fabs(intraRcutoff-rcut) < 1e-5);
    iF[ki] = CMulExpan(V, CRange(p), scale);

    n = sprintf(fname2, "%s/%s.%d.H.exp", exppath, runname, ki);
    assert(n <= MAX_CHARNUM);
    readExpansion(V, p, fname2, scale, rcut);
    assert( fabs(intraRcutoff-rcut) < 1e-5);
    iH[ki] = CMulExpan(V, CRange(p), scale);
  }
}

/*!#########################################################*/
/*#########################################################*/
//!  readExpansionsP function
/*! A subfunction to read in a multipole expansion
 \param iF a multipole expansion object to read in
 \param iH a multipole expansion object to read in
 \param p number of poles of the expansion
 \param exppath a path of where the expansions are located
 \param runname a name of the run of the expansion files
 \param ncen an integer of the number of centers
 \param rcut a floating point of the cutoff between expansions */
/*!#########################################################*/
/*#########################################################*/
void readExpansionsP(vector<CMulExpan*> &iF, vector<CMulExpan*>  &iH,
      int p, string exppath, string runname, int ncen, REAL & intraRcutoff)
{

  iF.resize(ncen); iH.resize(ncen);
  vector<double> V; REAL scale;


  for(int ki=0; ki<ncen; ki++)
  {
    char fname1[MAX_CHARNUM], fname2[MAX_CHARNUM];
    int n;
    REAL rcut;

    n = sprintf(fname1, "%s/%s.%d.F.exp",exppath.c_str(),runname.c_str(), ki);
    assert(n <= MAX_CHARNUM);
    readExpansion(V, p, fname1, scale, rcut);
    if(ki==0) intraRcutoff = rcut;
    else assert( fabs(intraRcutoff-rcut) < 1e-5);
    iF[ki] = new CMulExpan(V, CRange(p), scale);

    n = sprintf(fname2, "%s/%s.%d.H.exp",exppath.c_str(),runname.c_str(), ki);
    assert(n <= MAX_CHARNUM);
    readExpansion(V, p, fname2, scale, rcut);
    assert( fabs(intraRcutoff-rcut) < 1e-5);
    iH[ki] = new CMulExpan(V, CRange(p), scale);
  }

}

/*!#########################################################*/
/*#########################################################*/
// Read in centers and radii from centers.out file
/*!#########################################################*/
/*#########################################################*/
void readCenters(const char* fname, vector<CPnt> &cens, vector<double> &radii)
{
  cens.clear();
  radii.clear();

  ifstream fin(fname);
  if (!fin.is_open())
  {
    cout << "Could not open file " << fname << endl;
    exit(0);
  }

  int j =-1;
  cout<<"Reading in centers and radius from "<<fname<<endl;
  while (!fin.eof())
  {
    REAL x,y,z,rad;
    CPnt cen;

    j++;
    fin>>x>>y>>z>>rad;
    cen = CPnt(x,y,z);
    cens.push_back(cen);
    radii.push_back(rad);
  }
  return;
}


//!<#########################################################
//!<#########################################################
//!< readZlist
//!< reads list of z positions at which to calculuate potential
//!<#########################################################
//!<#########################################################
void readZlist( string fname , vector<double> & zpos )
{
  string tmp;
  double rz ;

  ifstream fin( fname.c_str() );
  if ( !fin.is_open( ))
  {
    cout << "Could not open zlist file " << endl;
    exit(0);
  }

  while( !fin.eof() )
  {
    getline( fin , tmp ) ;
    istringstream ss(tmp) ;
    ss >> rz ;
    zpos.push_back( rz ) ;
  }

  //!< remove final two elements since they are repeats
  zpos.pop_back() ; zpos.pop_back() ;

  return ;
}

