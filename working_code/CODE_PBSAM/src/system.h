#ifndef _SYSTEM_H_
#define _SYSTEM_H_

#include "util.h"
#include <cfloat>

const CPnt dbl_max_vect(DBL_MAX, DBL_MAX, DBL_MAX) ;    // vector of DBL_MAX

class CSystem
{
 public:
  static void initConstants(double kappa, double sdiel, double temp,
          bool bPBC=false, CPnt boxl=dbl_max_vect);
  static void deleteConstants();
  static CPnt pbcPos(CPnt p);

  static double KAPPA, m_sdiel, m_temp;
  static CPnt BOXL ;
  static bool bPBC;
};

#endif
