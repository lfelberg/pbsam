#ifndef READUTIL_H
#define READUTIL_H

#include "util.h"
#include "setup.h"
#include "expansion.h"

const int MAX_CHARNUM = 300; // max# of char for fname (including path)

class CMolecule;

// various functions to read / write files
vector<string> split(string str, char delimiter);

void findKeyword(vector<string> fline, CSetup & inputRead);
void findLines(string fline, CSetup & inputRead);

void readInputFile(char * fname, CSetup & readIn);

void readpqr( string fname, vector<CPnt> & pnt, vector<REAL> & ch,
	     vector<REAL> & rad, vector<CPnt> & cen);
void readXYZ( string fname, int nmols, vector<CPnt> & pnt);
void writeXYZ(ofstream & traj, double dt, int writefreq, vector<CPnt> & pnt);
void printM(double * A, int m, int n);
void writeMat_Unformatted(double *mat, int p, char* fname);
void readMat_Unformatted(double *mat, int maxp, char* fname);
void readMat_Formatted(double *mat, int maxp, char* fname);
void readMats(vector<REAL*> &iMats, int p, string imatpath, int ncen);
void writeMat_Formatted(double *mat, int p, char* fname);
void readCenters(const char* fname, vector<CPnt> &cens, vector<double> &radii);
void readZlist( string zlist , vector<double> & zpos ) ;

void writeExpansion(const CExpan &E, REAL rcut, char* fname);
void readExpansion(vector<double> &V, int maxp, char* fname,
        REAL & scale, REAL &rcut);
void readExpansions(vector<CMulExpan> &iF, vector<CMulExpan>  &iH ,
		    int p, char* exppath, char* runname, int ncen, REAL & intraRcutoff);
void readExpansionsP(vector<CMulExpan*> &iF, vector<CMulExpan*>  &iH ,
		    int p, string exppath, string runname, int ncen, REAL & intraRcutoff);

CPnt computeAtomCenter(const vector<CPnt> &pos);

#endif
