#include "molecule.h"
#include "system.h"
#include "readutil.h"

/*##################################################################
 * #
 * # File: pbsam.h
 * #
 * # Date: June 2015
 * #
 * # Description: Header file for pbsam
 * #
 * # Author: Felberg
 * #
 * # Copyright ( c )
 * #
 * #################################################################*/

using namespace std;

int selfpol_main( CSetup setupVals, double intraRcutoff, int ncen, CPnt rcen,
         vector<CPnt> scen, vector<double> srad, vector<double> CHG,
         vector<CPnt> POS, vector<vector<CPnt> > SPxes, vector<int> nSPx,
         vector<vector<int> > neighs, vector< vector<int> > intraPolLists_near);

int pbsam_main( CSetup initAll );
