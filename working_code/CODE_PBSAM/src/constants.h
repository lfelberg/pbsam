#ifndef _CONSTS_H_
#define _CONSTS_H_

#include <cfloat>

/*####################################################################
 * #
 * # File: constants.h
 * #
 * # Date: June 2015
 * #
 * # Description: An attempt to get all system constants in one place
 * #
 * # Author: Felberg
 * #
 * # Copyright ( c )
 * #
 * #####################################################################*/

#define DISTCUTOFF_TIME 20 //! angstroms
#define WRITEFREQ 5 //! How many steps between each write of BD

// definitions from expansion files
#define N_POLES 20
#define PREC_LIMIT (1e-30)
#define EXPAN_EPS 1e-12

// definitions from molecule files
#define MAX_POLAR_DEV (1e-8)
#define MAX_POLAR_DEV_SQR (MAX_POLAR_DEV*MAX_POLAR_DEV)
#define MAXMONOPOLE 5

#ifdef __ACCURATE__
#define MAX_POL_ROUNDS 1500
#else
#define MAX_POL_ROUNDS 200
#endif

#define MINSEPDIST 5
#define PS_TO_NS 0.001

// SYSTEM SPECIFIC PARAMETERS
const double AVOGADRO_NUM = 6.0221415e23;               // molecules per mole
const double ELECT_CHG = 1.60217646e-19;                // [Coulombs]
const double PERMITTIVITY_VAC = 8.854187817e-12;        // [F/m] , F = Farad
const double KB = 1.3806503e-23;                        // [J/K]
const double ANGSTROM = 1e-10;                          // [m/A]
const double LITRE = 1e-3;                              // [m^3/L]
const double COUL_K = 2.30708e-18;       // ELECT_CHG^2/(4*PI*PERM_VAC * ANG) [C^2 A/F]
                                         // converts internal units to J ; F = C^2/J
const double JtoKCALperMOL = 1.439326e20 ;              // convert J to kcal/mol

#endif
