#include "pbsam.h"
#ifdef __LEAK
#include "leaker.h"
#endif

/*##################################################################
 * #
 * # File: pbsam_main.cpp
 * #
 * # Date: June 2015
 * #
 * # Description:  Contains main executables for self pol and make Imat
 * #
 * # Author: Felberg
 * #
 * # Copyright ( c )
 * #
 * #################################################################*/

using namespace std;

/*#########################################################*/
/*#########################################################*/
// INPUTS
// param 1 = flag ( mat )
// param 2 = number of threads
// param 3 = salt conc ( M )
// param 4 = pqr filename
/*#########################################################*/
/*#########################################################*/

/*#########################################################*/
/*#########################################################*/
// pbsam_main
// Initializes system for imat and spol
/*#########################################################*/
/*#########################################################*/
int pbsam_main( CSetup initAll )
{
  string system = initAll.getRunType();
  cout <<"Building " << system <<  " ..." <<endl;

  double start = read_timer(  );  // Timing run

  //////////////// read in charge + centers //////////////
  vector<CPnt> scen, POS; vector<double> srad, CHG;
  string configfile  = initAll.getTypeNPQR(0);
  readpqr( configfile, POS, CHG, srad, scen ); // as PQR

  //////////////// // define center as center of spheres ////////////////
  const int ncen = scen.size(  );// no of unique spheres
  CPnt rcen = computeAtomCenter( scen );
  cout <<"rcen : " <<rcen<<" ; no. of centers:  "<<ncen<<endl;
  for (int i=0; i < ncen; i++) cout << "This is center " << i << ": " << scen[i] << endl;
  //for (int i=0; i < CHG.size(); i++) cout << "This is cg " << CHG[i] << ": " << POS[i] << endl;

  /////////////// generate a copy of moltype values ///////////////
  vector<vector<CPnt> > SPxes;
  vector<int> nSPx;
  vector<vector<int> > neighs;
  CMolecule::generateMolSPX( scen, srad, SPxes, nSPx, neighs  );

  //////////////// create molecule  //////////////
  const double intraRcutoff = 600 ; //set it big so all sph are near
  cout << "intraRcutoff  " << intraRcutoff << endl ;

  vector< vector<int> > intraPolLists_far, intraPolLists_near;
  CMolecule::generateMolTypeIntraPolLists( scen, srad, intraRcutoff,
                intraPolLists_near, intraPolLists_far );

  assert ( intraPolLists_near.size( ) == ncen ); // all sphs 'near' for spol
  cout << "After creating molecules: "; printMem();

  if (system == "imat")
    CMolecule mol(rcen, scen, srad, CHG, POS, initAll.getIDiel(),
                 SPxes, nSPx, neighs, intraPolLists_near);
  else
    selfpol_main( initAll, intraRcutoff, ncen, rcen, scen, srad,
          CHG, POS, SPxes, nSPx, neighs, intraPolLists_near );

  // Finish timer, print out total run time
  double end = read_timer(  );
  cout <<"Total Time to compute " << system << " [s] = "<<end-start<<endl;

  CSystem::deleteConstants(  );
  return 0;
}// end pbsam_main

/*#########################################################*/
/*#########################################################*/
// selfpol_main
// The main for a selfpolarization routine
/*#########################################################*/
/*#########################################################*/
int selfpol_main( CSetup setupVals, double intraRcutoff, int ncen,
         CPnt rcen, vector<CPnt> scen, vector<double> srad, vector<double> CHG,
         vector<CPnt> POS, vector<vector<CPnt> > SPxes, vector<int> nSPx,
         vector<vector<int> > neighs, vector< vector<int> > intraPolLists_near)
{
  //////////////// read in interaction matrices //////////////
  vector<REAL*> iMats; string imatpath = setupVals.getTypeNMatDir(0);
  readMats( iMats, N_POLES, imatpath, ncen );

  CMolecule mol( rcen, scen, srad, CHG, POS, setupVals.getIDiel(), iMats,
  intraRcutoff, SPxes, nSPx, neighs, intraPolLists_near );

  //////////////// self polarization  //////////////
  bool bPot = true;
  mol.polarize_self( bPot );

  string expname = setupVals.getTypeNPolDir(0)+"/"+setupVals.getTypeNPolExp(0);
  mol.writeMolExpansions( expname );

  ////////////////  clean up memory    //////////////
  mol.deleteSpheres() ; 

  return 0;
} // end selfpol_main


