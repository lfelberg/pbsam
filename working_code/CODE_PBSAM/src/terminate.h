#ifndef TERMINATE_H
#define TERMINATE_H

#include <cfloat>
#include "float.h"
#include "util.h"

using namespace std;

class CTerminate
{
public:
  CTerminate();
  ~CTerminate() {};

private:
  string m_termType; // a string of the termination type: dock or pass

}// end CTerminate

#endif
