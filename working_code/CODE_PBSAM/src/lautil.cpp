#include <cstdlib>
#include <cstring>
#include <cassert>
#include <cmath>
#include <iostream>
#include "lautil.h"

#ifdef __ACML
#include "acml.h"
#include "clapack.h"
#include "cblas_forNonMac.h"
#endif

#ifdef __MKL
#include "mkl.h"
#include "clapack.h"
#endif

using namespace std;

// computes Y = alpha*A*X + beta*Y
//applyMMat(m_IMat, &(fx[0]), &(F[0]), 1.0, 0.0, p2, D, p2);
void applyMMat(const double * A, const double * X, double * Y,
	    const double alpha, const double beta, int ma, int nc, int na)

{
  const int M = ma;
  const int N = nc;
  const int K = na;
  const int lda = M;
  const int ldb = K;
  const int ldc = M;

#ifdef __ACML
  char transA = 'N';
  char transB = 'N';
  dgemm(transA, transB, M, N, &K, alpha, const_cast<double*>(A), lda,
	const_cast<double*>(X), ldb, beta, Y, ldc);
#endif // ifACML

#ifdef __MKL
  char transA = 'N';
  char transB = 'N';
  dgemm(&transA, &transB, &M, &N, &K, &alpha, const_cast<double*>(A), &lda,
	const_cast<double*>(X), &ldb, &beta, Y, &ldc);
#endif // ifMKL

/*int ct = 0;
  if( nc == 1)
  {
  for (int i = 0 ; i < ma ; i ++ )
  {
    for (int j = 0 ; j < ma ; j ++ ) 
    {
      cout << A[ct] << ", " ;
      ct ++;
    }
      cout << endl; 
  }
  cout << endl;
  }
*/

  return;
}

