#include "BD.h"

#ifdef __LEAK
#include "leaker.h"
#endif

using namespace std;



/*!###############################################################
 * #
 * # File: mba.cpp
 * # Date: Sept 2015
 * # Description: Multi-body approximations to full mutual polarization
 * # Author: Marielle Soniat
 * #
 * ################################################################*/



//!<#########################################################
//!<#########################################################
//!< mba_main
//!< called if mba keyword is specified in the input file
//!< calls mbaConvergence
//!<#########################################################
//!<#########################################################
int
mba_main( CSetup initAll, CPnt boxl )
{
  cout << "Precision limits : double " << DBL_EPSILON << " long double " << LDBL_EPSILON << endl ;
  cout << endl ;
  cout << endl ;
  cout << endl ;

  cout << "called MBA" << endl ;
  CBD bd = CBD( initAll );
  bd.setconfig( initAll , boxl );
  bd.mbaConvergence( initAll );
  cout << "returned to mba_main" << endl ;
  bd.cleanupMBA() ;
  cout << "Done with cleanup." << endl ;
  return 0 ;
} // end mba_main



//!<#########################################################
//!<#########################################################
//!< mbaConvergence
//!< Tests single point calculations of energy, force and torque
//!< for convergence of direct, 2-body, 3-body, and 4-body
//!< polarization to full mutual polarization
//!<#########################################################*/
//!<#########################################################*/
int
CBD::mbaConvergence( CSetup initAll )
{
  cout << endl ;
  cout << endl ;
  cout << "Test of MBA convergence to full mutual polarization" << endl ;
  cout << "Units :    Energy [J/mol]    Force  [J/m]    Torque [J/radian]" << endl ;

  // Writing out initial configurations and starting
  CMolecule::writeMolsPQR( "initial.pqr", m_mols );

  int nmol = initAll.getTypeNCount(0) ;
  int mbaLevelMax = initAll.getLevel() ;


  cout << "------------- Direct -----------" << endl ;
  double start1 = read_timer() ;
  polarize_direct( m_mols ) ;
  directPolarization( initAll ) ;
  double end1 = read_timer() ;
  cout << "Computation time for direct = " << end1 - start1 << " s" << endl ;
  cout << endl ;


  cout << "------------- N Body -----------" << endl ;
  start1 = read_timer() ;
  NBPolarization( initAll , mbaLevelMax ) ;
  end1 = read_timer() ;
  cout << "Computation time for MBA = " << end1 - start1 << " s" << endl ;
  cout << endl ;


  cout << "-------------  Full  -----------" << endl ;
  start1 = read_timer() ;
  fullPolarization( initAll ) ;
  end1 = read_timer() ;
  cout << "Computation time for full = " << end1 - start1 << " s" << endl ;
  cout << endl ;

  cout << "--------- Comparing Totals -----------" << endl ;
  printMBAtotals( mbaLevelMax ) ;


  //!< Adjust totals from nBody for over-counting
  if ( mbaLevelMax > 2 )
  {
    for ( int i = 0 ; i < nmol ; i++ )
    {
//    m_tripEnergy[i] = m_tripEnergy[i] - (nmol-4)*m_pairEnergy[i] - m_pairEtot ;
      m_tripEnergy[i] = m_tripEnergy[i] - (nmol-3)*m_pairEnergy[i] ;
      m_tripForc[i] = m_tripForc[i] - (nmol-3)*m_pairForc[i] ;
      m_tripTorq[i] = m_tripTorq[i] - (nmol-3)*m_pairTorq[i] ;
    }
    m_tripEtot = m_tripEtot - (nmol-3)*m_pairEtot ;
  }

  cout << "-------- Adjusted for overcounting --------" << endl ;
  printMBAtotals( mbaLevelMax ) ;

  return 0 ;
} // end mbaConvergence



//!<#########################################################
//!<*########################################################
//!< makeGroups
//!< make a list of all unique subsets of molecules
//!< nBodies specifies the number of molecules in each subset
//!<*########################################################
//!<*########################################################
// This could probably be more eloquent.
// only handles up to 4-body at the moment
void
CBD::makeGroups( vector<vector<int> > & groups , int nBodies )
{
  cout << "Making list of groups." << endl ;

  int nmols = m_mols.size() ;
  groups.clear() ;
  vector<int> set ;    set.clear() ;
  int nsets = 0 ;     int count = 0 ;

  if ( nBodies >= 2 )
  {
    nsets = nmols*(nmols-1) / (2) ;
    cout << nsets << " sets of 2 bodies" << endl ;
    assert ( nsets > 0 ) ;
    for ( int i = 0 ; i < nmols ; i++ )
    {
      for ( int j = i+1 ; j < nmols ; j++ )
      {
        if ( j != i )
        {
            set.clear() ;
            cout << "group : " << i << " " << j << endl;
            set.push_back( i ) ;
            set.push_back( j ) ;
            groups.push_back( set ) ;
            count += 1 ;
        }
      } // end j
    } // end i
    assert ( groups.size() == ( nsets ) ) ;
    m_pairs = groups ;
  } // end 2 body
  groups.clear() ;


  if ( nBodies >= 3 )
  {
    nsets = nmols*(nmols-1)*(nmols-2) / (3*2) ;
    cout << nsets << " set of 3 bodies" << endl ;
    assert ( nsets > 0 ) ;
    for ( int i = 0 ; i < nmols ; i++ )
    {
      for ( int j = i+1 ; j < nmols ; j++ )
      {
        if ( j != i )
        {
            for ( int k = j+1 ; k < nmols ; k++ )
            {
              if ( ( k != i ) && ( k != j ) )
              {
                set.clear() ;
                cout << "group : " << i << " " << j << " " << k << endl;
                set.push_back( i ) ;
                set.push_back( j ) ;
                set.push_back( k ) ;
                groups.push_back( set ) ;
                count += 1 ;
              }
            } // end k
        }
      } // end j
    } // end i
    assert ( groups.size() == ( nsets ) ) ;
    m_triplets = groups ;
  } // end 3 body
  groups.clear() ;


  if ( nBodies >= 4 )
  {
    nsets = nmols*(nmols-1)*(nmols-2)*(nmols-3) / (4*3*2) ;
    cout << nsets << " set of 4 bodies" << endl ;
    assert ( nsets > 0 ) ;
    for ( int i = 0 ; i < nmols ; i++ )
    {
      for ( int j = i+1 ; j < nmols ; j++ )
      {
        if ( j != i )
        {
            for ( int k = j+1 ; k < nmols ; k++ )
            {
              if ( ( k != i ) && ( k != j ) )
              {
                  for ( int m = k+1 ; m < nmols ; m++ )
                  {
                    if ( ( m != i ) && ( m != j ) && ( m != k ) )
                    {
                      set.clear() ;
                      cout << "group : " << i << " " << j << " " << k << " " << m << endl;
                      set.push_back( i ) ;
                      set.push_back( j ) ;
                      set.push_back( k ) ;
                      set.push_back( m ) ;
                      groups.push_back( set ) ;
                      count += 1 ;
                    }
                  } // end m
              }
            } // end k
        }
      } // end j
    } // end i
    assert ( groups.size() == ( nsets ) ) ;
    m_quartets = groups ;
  } // end 4 body
  groups.clear() ;

  if ( nBodies >= 5 )
  {  cout << "Error : cannot do more than 4-body approximation." << endl ; exit(1) ;  }

  cout << "List made." << endl ;
  return ;
} // end makeGroups



//!<#########################################################
//!<#########################################################
//!< polarize_direct
//!< polarizes each molecule based only on the permanent electrostatics
//!< (i.e. selfpol) of other molecules
//!<#########################################################
//!<#########################################################
void
CBD::polarize_direct( vector<CMolecule*> & mols )
{
  cout << "called polarize direct" << endl ;

  int nmols = m_mols.size() ;
  bool bOnlyPotential = false ;
  vector<CPnt> force(nmols);
  vector<CPnt> torque(nmols);
  vector<double> intEnergy(nmols) ;
  double eTot = 0.0 ;

  for ( int i = 0 ; i < nmols ; i++ )
  {
    intEnergy[i] = 0.0 ;
    force[i] = CPnt(0.0,0.0,0.0) ;
    torque[i] = CPnt(0.0,0.0,0.0) ;
  }

  if( nmols == 1 ) return ;

  bool bInterXFS =  CMolecule::generateInterXFormsForPolarize_LowMemory(mols);
  if( !bInterXFS )
  {
    cout <<"error in generateInterXFormsForPolarize_LowMemory"<<endl;
    return ;
  }
  CMolecule::polarize_direct(mols, bOnlyPotential, 1000);

// need polarize_direct working correctly before using this
/*
  //Compute forces and torques
  for (int i = 0; i < nmols ; i++)
  {
     mols[i]->computeMol_Force_and_Torque(force[i],torque[i],intEnergy[i]);


#if __DEBUGDIE__
     double f = force[i].norm();
     if(fabs(f) > 5 || isnan(f) )
     {
        cout <<"died in computeforces; mol "<<i<<endl;
        CMolecule::writeMolsPQR("died.compforces.pqr", mols);
        CMolecule::saveConfig("died.compforces.config", mols);
        return ; //exit(1);
     }
#endif
  }
*/

  return ;
}



//!<#########################################################
//!<#########################################################
//!< directPolarization
//!< calculates energy, force, and torque when direct
//!< polarization is used
//!< !! not working yet !!
//!<#########################################################
//!<#########################################################
void
CBD::directPolarization( CSetup setupVals )
{
  cout << "Entered directPolarization" << endl ;

  int nmol = m_mols.size() ;
  vector<CPnt> force(nmol);
  vector<CPnt> torque(nmol);
  vector<double> intEnergy(nmol) ;
  double eTot = 0.0 ;

  for ( int i = 0 ; i < nmol ; i++ )
  {
    intEnergy[i] = 0.0 ;
    force[i] = CPnt(0.0,0.0,0.0) ;
    torque[i] = CPnt(0.0,0.0,0.0) ;
  }

  m_directEnergy = intEnergy ;
  m_directForc = force ;
  m_directTorq = torque ;
  m_directEtot = eTot ;

  return ;
}



//!<#########################################################
//!<#########################################################
//!< fullPolarization
//!< calculation of single-point energy, force and torque
//!< with full mutual polarization
//!<#########################################################
//!<#########################################################
void
CBD::fullPolarization( CSetup setupVals )
{
  cout << "Entered fullPolarization" << endl ;

  int nmol = m_mols.size() ;
  bool bComputeForce = true ;
//  double INTtoJperMeter = setupVals.getFACT2() / ANGSTROM ;
  double INTtoJperMeter = COUL_K / ANGSTROM ;
  double eTot = 0.0 ;
  vector<CPnt> force(nmol), torque(nmol);
  vector<double> intEnergy(nmol) ;
  computeFMpol( nmol, bComputeForce, force, torque, intEnergy);

  for ( int i = 0 ; i < nmol ; i++ )
  {
    intEnergy[i] = 0.5*intEnergy[i] ;
    force[i] = 0.5 * INTtoJperMeter * force[i] ;
    torque[i] = 0.5 * ANGSTROM * INTtoJperMeter * torque[i] ;

    eTot += intEnergy[i] ;
    cout << "Interaction energy of " << i << " = " << intEnergy[i] << endl ;
    cout << "Force on " << i << " = " << force[i] << endl ;
    cout << "Torque on " << i << " = " << torque[i] << endl ;
  }
  cout << "Total energy = " << eTot << endl ;

  m_fullEnergy = intEnergy ;
  m_fullForc = force ;
  m_fullTorq = torque ;
  m_fullEtot = eTot ;

  return ;
} // end fullPolarization



//!<#########################################################
//!<#########################################################
//!< NBPolarization
//!< calculation of single-point energy, force and torque
//!< with n-body level of approximation
//!<#########################################################
//!<#########################################################
void
CBD::NBPolarization( CSetup setupVals , int nBodies )
{
  cout << "Entered " << nBodies << "-body polarization" << endl ;
  assert ( nBodies > 1 && nBodies < 5 ) ;

  int nmol = m_mols.size() ;
  bool bComputeForce = true ;
//  double INTtoJperMeter = setupVals.getFACT2() / ANGSTROM ;
  double INTtoJperMeter = COUL_K / ANGSTROM ;
  double eTot = 0.0 ;
  vector<CPnt> force(nmol), torque(nmol);
  vector<double> intEnergy(nmol) ;

  computeFMpolNB( nmol, nBodies, bComputeForce, force, torque, intEnergy, INTtoJperMeter);

/*
  cout << endl ;
  cout << "       Totals : " << endl ;
  for ( int i = 0 ; i < nmol ; i++ )
  {
    eTot += intEnergy[i] ;
    cout << "Interaction energy of " << i << " = " << intEnergy[i] << endl ;
    cout << "Force on " << i << " = " << force[i] << endl ;
    cout << "Torque on " << i << " = " << torque[i] << endl ;
  }
  cout << "Total energy = " << eTot << endl ;
  cout << "Finished " << nBodies << "-body polarization" << endl ;
*/

  return ;
} // end NBPolarization



//!<#########################################################
//!<#########################################################
//!< computeFMpolNB
//!< Compute electrostatic forces due to mutual polarization
//!< at the N-body level of approximation
//!<#########################################################
//!<#########################################################
void CBD::computeFMpolNB(int nmols, int nBodies, bool bComputeForce,
                      vector<CPnt> & forc, vector<CPnt> & torq,
                      vector<double> & intEnergy, double INTtoJperMeter )
{
// eventually, move this to initialization steps for use with nam, sim, etc.
  vector<vector<int> > groups , currentSet;
  double start1 = read_timer() ;
  //!< make list of sets of pairs, triplets, etc.
  makeGroups( groups , nBodies ) ;
  double end1 = read_timer() ;
  cout << "Computation time to make group lists : " << end1-start1 << " s" << endl ;

  //!< nBodies = level of approximation = 2, 3, 4, etc.
  //!< for each level of approximation
  for ( int kk = 2 ; kk <= nBodies ; kk++ )
  {
    double start3 = read_timer() ;

    //!< reset vectors to zero between each MBA approx
    for ( int ii = 0 ; ii < nmols ; ii++ )
    {
      forc[ii] = CPnt(0.0, 0.0, 0.0 ) ;
      torq[ii] = CPnt(0.0, 0.0, 0.0 ) ;
      intEnergy[ii] = 0.0 ;
    }

    //!< select list of pairs, triplets, etc.
    int setSize = kk ;
    if ( kk == 2 ) {  currentSet = m_pairs ;  }
    else if ( kk == 3 ) {  currentSet = m_triplets ;  }
    else if ( kk == 4 ) {  currentSet = m_quartets ;  }
    else { cout << "Level of approximation too large." << endl ; exit(1) ;  }
    int grpSize = currentSet.size() ;


// David's copy constructor
// /*
// Point* a;
// Point* b = new Point;
// b = a;
// */
// repeat for each pointer within a

// naive strategy : new copy of entire m_mols for each group in currentSet
// less naive : create one copy with length of longest group list
    CMolecule test ;
    test = *m_mols[0] ;
/*
    double startCopy = read_timer() ;
    vector<vector<CMolecule> > testVecVec ;    testVecVec.clear() ;
    for ( int i = 0 ; i < grpSize ; i++ )
    {
      vector<CMolecule> testVec ;    testVec.clear() ;
      for ( int j = 0 ; j < m_mols.size() ; j++ )
      {
//        CMolecule* test = new CMolecule ; // ==> new CMolecule*
        CMolecule test ;
        test = *m_mols[j] ;
        testVec.push_back(test) ;
//        delete test ;
// or delete at the end of this function instead?
      }
      assert( testVec.size() == m_mols.size() ) ;
      testVecVec.push_back( testVec ) ;
    }
    assert( testVecVec.size() == grpSize ) ;
    double endCopy = read_timer() ;
    cout << "Time to copy molecules = " << endCopy-startCopy << " s" << endl;
*/




//#ifdef __OMP
//#pragma omp parallel for shared(currentSet) //reduction(+:forc,torq,intEnergy) //
//#endif
    //!< make a subset of mols for each group
    for ( int i = 0 ; i < grpSize ; i++ )
    {
      double start2 = read_timer() ;
      //!< make a vector subsetMols from m_mols
      vector<CMolecule*> subsetMols ;    subsetMols.clear() ;

      //!< for each index in group, send that molecule to subsetMols
// MES : I think for parallel, I need to make subsetMols with
// a copy of m_mol[id] ==> need copy constructor because members of m_mol
// are pointers
// class data members not allowed to be shared --> make a copy before parallel and share the copy?
//     then each subsetMols copies from the copy??
      for ( int j = 0 ; j < setSize ; j++ )
      {
        assert( currentSet[i][j] < nmols ) ;
//#ifdef __OMP
     //   CMolecule* currentMol =  m_mols[ currentSet[i][j] ] ;  // for parallel
//        CMolecule currentMol = new CMolecule( m_mols[ currentSet[i][j] ] ) ;
//        currentMol = new CMolecule( m_mols[ currentSet[i][j] ] ) ;
//        CMolecule currentMol(  m_mols[ currentSet[i][j] ] ) ;
     //   subsetMols.push_back( currentMol ) ; // for parallel
//#else
        subsetMols.push_back( m_mols[ currentSet[i][j] ] ) ; // working for non-parallel
//#endif
// instead push_back new (copy of) mols
// need to do this here (not before parallel) because it contains pointers!
// if a new copy of molecule is created, may be able to push_back pointers instead of objects
      }

      cout << subsetMols.size() << " in subsetMols, for set size = " << setSize << endl ;
      cout << "Group : "  ;
        for ( int k = 0 ; k < currentSet[i].size() ; k++ )
        {  cout << currentSet[i][k] << " " ;  }
           cout << endl ;
      assert( subsetMols.size() == (setSize) ) ;

      // These are initialized later
      vector<CPnt> subsetForce(setSize), subsetTorque(setSize);
      vector<double> subsetIntEnergy(setSize) ;


      bool bBlown = CMolecule::computeForces( subsetMols,
                     subsetForce, subsetTorque, subsetIntEnergy ) ;
      assert ( subsetForce.size() == setSize ) ;
      if( !bBlown )
      {
        cout <<"died somewherein computeforces"<<endl;
        CMolecule::saveConfig( "died.config", m_mols );
        CMolecule::writeMolsPQR( "died.pqr", m_mols );
        exit( 1 );
      }


      cout << "Values for subset : " << endl ;
      for ( int k = 0 ; k < setSize ; k++ )
      {
        //!< Unit conversion
        //!< energy already in J/mol
        //!< Forces and torque in internal units - convert to J/m and J/rad
        subsetForce[k] = INTtoJperMeter * subsetForce[k] ;
        subsetTorque[k] = ANGSTROM * INTtoJperMeter * subsetTorque[k] ;


        cout << "   " << k << "  Interaction energy of " << currentSet[i][k]
           << " = " << subsetIntEnergy[k] << endl ;
        cout << "   " << k << "  Force on " << currentSet[i][k]
           << " = " << subsetForce[k] << endl ;
        cout << "   " << k << "  Torque on " << currentSet[i][k]
           << " = " << subsetTorque[k] << endl ;

        //!< map subset values to correct molecule
        //!< total energy is half the sum of interaction energies
#pragma OMP atomic
        forc[currentSet[i][k]] += 0.5*subsetForce[k] ;
#pragma OMP atomic
        torq[currentSet[i][k]] += 0.5*subsetTorque[k] ;
#pragma OMP atomic
        intEnergy[currentSet[i][k]] += 0.5*subsetIntEnergy[k] ;
      } // end k

// for parallel
//      for ( int j = 0 ; j < setSize ; j++ )
//      {  delete subsetMols[j] ;  }
// MES : may need to delete sub-objects too

      double end2 = read_timer() ;
      cout << "Computation time for group : " << end2-start2 << " s" << endl ;

    } // end i for each group within the list of pairs, triplets, etc.
// end parallel

    //!< sum intEnergy for each molecule to get eTot
    double eTot = 0.0 ;
    for ( int ii = 0 ; ii < m_mols.size() ; ii++ )
    {
      eTot += intEnergy[ii] ;
      cout << "Interaction energy of " << ii << " = " << intEnergy[ii] << endl ;
      cout << "Force on " << ii << " = " << forc[ii] << endl ;
      cout << "Torque on " << ii << " = " << torq[ii] << endl ;
    }
    cout << "Total energy = " << eTot << endl ;
    cout << endl ;
    cout << endl ;

    //!< copy energy,force,torque to m_pairEnergy, etc.
    //!< total energy is half the sum of interaction energies
    if ( kk == 2 )
    {
      m_pairEnergy = intEnergy ;
      m_pairForc = forc ;
      m_pairTorq = torq ;
      m_pairEtot = eTot ;
    }
    else if ( kk == 3 )
    {
      m_tripEnergy = intEnergy ;
      m_tripForc = forc ;
      m_tripTorq = torq ;
      m_tripEtot = eTot ;
    }
    else if ( kk == 4 )
    {
      m_quarEnergy = intEnergy ;
      m_quarForc = forc ;
      m_quarTorq = torq ;
      m_quarEtot = eTot ;
    }

/*
    double startDel = read_timer() ;
    for ( int i = 0 ; i < grpSize ; i++ )
    {
      for ( int j = 0 ; j < m_mols.size() ; j++ )
      {
        delete testVecVec[i][j] ;
      }
    }
    double endDel = read_timer() ;
    cout << "Time to delete molecules = " << endDel-startDel << " s" << endl;
*/

    double end3 = read_timer() ;
    cout << "Computation time for " << kk << "BA = " << end3 - start3 << " s" << endl ;
    cout << endl ;

  } // end kk for each level of approximation

} // end computeFMpolNB



//!<#########################################################
//!<#########################################################
//!< printMBAtotals
//!< prints energy, force and torque
//!< from different levels of multi-body approximation
//!<#########################################################
//!<#########################################################
void CBD::printMBAtotals( int mbaLevelMax )
{
  int nmol = m_mols.size() ;

  cout << "Method \t i \t Energy \t Force \t \t \t Torque" << endl ;
  for ( int i = 0 ; i < nmol ; i++ )
  {
//    cout << "Direct\t"
//         << i << "\t" << m_directEnergy[i] << " " << m_directForc[i] << " " << m_directTorq[i] << endl ;
    cout << "2Body\t"
         << i << "\t" << m_pairEnergy[i] << " " << m_pairForc[i] << " " << m_pairTorq[i] << endl ;
    if ( mbaLevelMax >= 3 )
    {
      cout << "3Body\t"
         << i << "\t" << m_tripEnergy[i] << " " << m_tripForc[i] << " " << m_tripTorq[i] << endl ;
    }
    if ( mbaLevelMax >= 4 )
    {
      cout << "4Body\t"
         << i << "\t" << m_quarEnergy[i] << " " << m_quarForc[i] << " " << m_quarTorq[i] << endl ;
    }
    cout << "Full  \t"
         << i << "\t" << m_fullEnergy[i] << " " << m_fullForc[i] << " " << m_fullTorq[i] << endl ;
  }
//  cout << "Direct\t" << m_directEtot << endl ;
  cout << "2Body\t" << m_pairEtot << endl ;
  if ( mbaLevelMax >= 3 )  {  cout << "3Body\t" << m_tripEtot << endl ;  }
  if ( mbaLevelMax >= 4 )  {  cout << "4Body\t" << m_quarEtot << endl ;  }
  cout << "Full  \t" << m_fullEtot << endl ;

  cout << endl ;
  cout << endl ;
}



//!<#########################################################
//!<#########################################################
//!< cleanupMBA
//!< releases memory assigned on heap with new
//!<#########################################################
//!<#########################################################
void
CBD::cleanupMBA()
{
  int totalMols = 0 ;

  for ( int i = 0 ; i < m_nMolType ; i++ )
  {
    totalMols += m_np[i] ;
//    for ( int j = 0 ; j < m_np[i] ; j++ )
    int j = 0 ;
    {
      if ( m_iF[i][j] != NULL )
      {
        delete m_iF[i][j] ;
      }
      // correct syntax but causing seg fault
      // all j refer to same location
      // ==> looping over j causes double delete
      // if statement for NULL does not work.

      if ( m_iH[i][j] != 0 )
      {  delete m_iH[i][j] ;  }
      if ( m_LFs_intraSelf[i][j] != 0 )
      {  delete m_LFs_intraSelf[i][j] ;  }
      if ( m_LHs_intraSelf[i][j] != 0 )
      {  delete m_LHs_intraSelf[i][j] ;  }
      if ( m_LFs_intraSelf_far[i][j] != 0 )
      {  delete m_LFs_intraSelf_far[i][j] ;  }
      if ( m_LHs_intraSelf_far[i][j] != 0 )
      {  delete m_LHs_intraSelf_far[i][j] ;  }
    }
  }


  for ( int ii = 0 ; ii < m_nMolType ; ii++ )
  {
//    for ( int jj = 0 ; jj < m_np[ii] ; jj++ )
    int jj = 0 ;
    {
      m_mols[ii][jj].deleteSpheres() ;
    }
  }


  for ( int k = 0 ; k < m_mols.size() ; k++ )
  {
    if ( m_mols[k] != 0 )
    {  delete m_mols[k] ;  }
  }
  // This is not deleting all m_mols

} // end cleanupMBA


