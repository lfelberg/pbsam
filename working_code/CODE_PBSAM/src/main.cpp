#include "BD.h"
#ifdef __LEAK
#include "leaker.h"
#endif

using namespace std;
/*!########################################################################
 * #
 * # File: main.cpp
 * # Date: Sept 2015
 * # Description: This file runs any system in the BD PB-SAM universe
 * # Author: Felberg
 * # Copyright ( c )
 * #
 * ########################################################################*/

void printCompile( char ** argVals );
void checkArgs( int argCount );
CPnt initSysConst( CSetup setupVals );
int  bd_main( CSetup initAll, CPnt boxl );
int mba_main( CSetup initAll, CPnt boxl );

/*#########################################################*/
/*#########################################################*/
/* Main body to run any PBSAM/BD */
/*#########################################################*/
/*#########################################################*/
int all_main( int argc, char ** argv )
{
  checkArgs( argc );
  printCompile( argv );
  CSetup initAll;
  initAll.printSetupClass( );

  readInputFile(argv[1], initAll);
  initAll.printSetupClass( );
  CPnt boxl = initSysConst( initAll );

#ifdef __OMP
    omp_set_num_threads( initAll.getThreads() );
#endif

  string system = initAll.getRunType();
  if( system == "sim" || system == "pot" )
    bd_main( initAll, boxl );
  else if ( system == "mba" )
//    mba_main( initAll, boxl ) ;
    cout << "No MBA right now" << endl ;
  else if ( system == "imat" || system == "spol" )
  {
    if ( initAll.getType() != 1)
    {
      cout << "PBSAM only uses one molecule type! " << endl;
      return 0;
    }
    pbsam_main( initAll );
  }
  else
    { cout << "Option not recognized!" << endl; return 0; }

  cout << "Program completing successfully." << endl ;
} // end all_main

/*!#########################################################*/
/*#########################################################*/
// Function printCompile
// Print out executable details and flags used in compile
/*#########################################################*/
/*#########################################################*/
void printCompile(char ** argVals)
{
  //!< printing out each input argument
  cout << "Compiled on " << __DATE__ << " " << __TIME__ << endl ;
  cout << "Exectutable name: "<< argVals[0] << endl ;

  // Printing flags:
  cout << "Printing flag options in makefile" << endl;
#ifdef __ACCURATE__
  cout << "Accurate flag used" << endl;
#endif
#ifdef __DIFF
  cout << "Diffusion only flag used" << endl;
#endif
#ifdef __LEAK
  cout << "Leaker memory checker flag used" << endl;
#endif
#ifdef __LOWMEM
  cout << "Lowmem flag used" << endl;
#endif
#ifdef __MKL
  cout << "MKL flag used" << endl;
#endif
#ifdef __NODIFF
  cout << "NODIFF flag used" << endl;
#endif
#ifdef __OMP
  cout << "OMP flag used" << endl;
#endif
#ifdef __DEBUG
  cout << "Debug flag used" << endl;
#endif
} // end printCompile

/*!#########################################################*/
/*#########################################################*/
// Function checkArgs
// Verify arguments of function are good
/*#########################################################*/
/*#########################################################*/
void checkArgs(int argCount)
{
  if ( argCount != 2 )
  {
    cout << "Correct input format: " << endl;
    cout << " ./exec run.inp " << endl;
    exit(0);
  }
} //end checkArgs

/*!#########################################################*/
/*#########################################################*/
// Function initSysConst
// Initialize system parameters and boxlength
/*#########################################################*/
/*#########################################################*/
CPnt initSysConst( CSetup setupVals )
{
  double boxlength = setupVals.getBLen();
  int dimensions = setupVals.getPBCs();
  //!< set up PBC dimensions
  CPnt boxl(boxlength, boxlength, boxlength);
  if ( dimensions == 2 )
      boxl.z() = 5*boxlength ;
  else if ( dimensions == 3 )
      cout << "PBC ok." << endl ;

  else { cout << "Default to no PBC" << endl ; }
  cout << dimensions << "D PBC. Boxl = "
       << boxl.x() << " " << boxl.y() << " " << boxl.z() << endl ;

  double kappa = setupVals.getKappa(); double sdiel = setupVals.getSDiel();
  double temperature = setupVals.getTemp(); int moltype = setupVals.getType();
  // Initializing the system's constants with PBC (true)
  if ( dimensions > 1 )
    CSystem::initConstants( kappa, sdiel, temperature, true, boxl );
  else
    CSystem::initConstants( kappa, sdiel, temperature, false, boxl );

  // Initializing the class BD.
  // nmoltype is the number of different molecule types
  CBD::initConstants( moltype );
  
  return boxl;
}// end initSysConst

/*!#########################################################*/
/*#########################################################*/
// Main!
/*#########################################################*/
/*#########################################################*/
int main( int argc, char ** argv )
{
  return all_main( argc, argv );
}
