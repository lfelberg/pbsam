#ifndef _BDNAF_H_
#define _BDNAF_H_
#include <vector>
#include "util.h"
#include "setup.h"
#include "readutil.h"
#include "molecule.h"
#include "system.h"
#include "pbsam.h"

/*####################################################################
 * #
 * # File: BD.h
 * #
 * # Date: June 2015
 * #
 * # Description:
 * #
 * # Author: Felberg
 * #
 * # Copyright ( c )
 * #
 * #####################################################################*/

using namespace std;

/*#########################################################*/
/*#########################################################*/
// general functions
/*#########################################################*/
/*#########################################################*/

class CBD
{
  public:
    // Constructor
    CBD( CSetup setupVals );
    ~CBD(  ){  }

    static void initConstants( int nMolType );

    //void setconfig( string configfname, CPnt edge );
    void setconfig( CSetup setupVals, CPnt edge );
    double runBD( int traj_no, CSetup setupVals, CPnt boxl );

    CMolecule* getMol( int i ) {return m_mols[i];}
    static CPnt getRandVec( REAL std )
      { return std*CPnt( normRand( ), normRand(), normRand()); }
    double getMembThickness ( ) { return m_memb_thick; }

    vector<CMolecule*> m_mols; // debug in public
    vector<vector<vector<CPnt> > > m_SPxes;
    vector<CMolCell>  m_molcells;

    void cleanupCBD() ;

// multi-body approximations
//    int mba_main( CSetup initAll, CPnt boxl );
    int mbaConvergence( CSetup initAll );
    void set4Bconfig( CSetup setupVals );
    void directPolarization( CSetup setupVals ) ;
    void fullPolarization( CSetup setupVals ) ;
    void NBPolarization( CSetup setupVals , int nBodies ) ;
    void polarize_direct( vector<CMolecule*> & mols ) ;
    double computeTotalEnergy( ) ;
    void computeFMpolNB(int nmols, int nBodies, bool bComputeForce,
                      vector<CPnt> & forc, vector<CPnt> & torq,
                      vector<double> & intEnergy ) ;

  private:
    void initMolTypes( CSetup setupVals );
		void initDiffusion( CSetup setupVals );
    void resizeVecs(int nparts);

    void printPot(CPnt boxl, string zlist);
    void writeSim(double start, double t);
    void diffusionSim(int nmolecules,
                   vector<CPnt> & forc, vector<CPnt> & torq);
    void computeFText(int nmols, double voltage_drop, double IKbT,
                   double fact2, vector<CPnt> & forc, vector<CPnt> & torq);
//    void computeFMpol(int nmols,  bool bComputeForce,
//                   vector<CPnt> & forc, vector<CPnt> & torq);
    void computeFMpol(int nmols,  bool bComputeForce,
                   vector<CPnt> & forc, vector<CPnt> & torq,
                   vector<double> & intEnergy );

    double propagate( double mindist, bool bWrite, double voltage_drop,
                   double IKbT, double fact2 );
    bool Isthrough( CMolecule* ion, CPnt boxl ) const;

    REAL computeMinSepDist(  );
    REAL compute_dt( double minsepdist ) const;
    CPnt makeMove( const vector<CPnt> & dR, const vector<CPnt> & dO, REAL dt );
    bool translate( vector<CPnt> dR, double dt, CPnt & extra );
    bool rotate( vector<CPnt> dO, double dt  );
    void accumulateMSD( int istep, vector<CPnt> rHinitial );


    //CPnt center_of_mass( int n_cen, vector<CPnt> molecule );

    static REAL MINDIST, MAXDIST;
    static int m_nMolType;

    // moltype variables
    vector<vector<REAL*> > m_iMats;
    vector<vector<CMulExpan*> > m_iF,m_iH;
    vector<vector<vector<double> > > m_qSolvedF, m_qSolvedH;
    vector<vector<double> > m_totalF, m_totalH;
    vector<vector<CLocalExpan*> > m_LFs_intraSelf, m_LHs_intraSelf,
          m_LFs_intraSelf_far, m_LHs_intraSelf_far;

    vector<vector<vector<int> > > m_neighs;
    vector<vector<int> > m_nSPx;
    vector<vector<vector<int> > > m_intraPolLists_near;
    vector<vector<CMolCell> >  m_molcell_all;

    // BD variables
    vector<int> m_np;
    double m_memb_thick;    // membrane thickness in Meters
    vector<REAL> m_Dtr;
    vector<REAL> m_Dr;
    vector<CQuat> m_rots;
    vector<CPnt> m_initialrcen;
};   // end class CBD

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
// Inline functions
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

/*!#########################################################*/
/*#########################################################*/
// Initialize correct size of all member vectors
/*#########################################################*/
/*#########################################################*/
inline void CBD::resizeVecs( int npart )
{
  m_nMolType = npart;
  m_np.resize(npart); m_initialrcen.resize(npart);
  m_iMats.resize(npart);
  m_iF.resize(npart); m_iH.resize(npart);
  m_SPxes.resize(npart); m_nSPx.resize(npart);
  m_neighs.resize(npart);
  m_qSolvedF.resize(npart); m_qSolvedH.resize(npart);
  m_totalF.resize(npart); m_totalH.resize(npart);
  m_molcell_all.resize(npart);
  m_intraPolLists_near.resize(npart);
  m_LFs_intraSelf.resize(npart); m_LHs_intraSelf.resize(npart);
  m_LFs_intraSelf_far.resize(npart);
  m_LHs_intraSelf_far.resize(npart);
}// end resizeVecs


/*!#########################################################*/
/*#########################################################*/
// Compute the delta timestep
/*#########################################################*/
/*#########################################################*/
inline REAL CBD::compute_dt( double d ) const
{
  //! assume DISTCUTOFF_TIME = CMolecule::m_interRcutoff
  if ( d - DISTCUTOFF_TIME > 0 )
    return 2.0 + ( d - DISTCUTOFF_TIME )/15;
  else
    return 2.0;
}  //! end compute_dt

/*!#########################################################*/
/*#########################################################*/
// check the proton has travelled through the pore
/*#########################################################*/
/*#########################################################*/
inline bool CBD::Isthrough( CMolecule* ion, CPnt boxl ) const
//CBD::Isthrough( CMolecule* ion, double boxlength ) const
{
  CPnt cenmass = ion->getRCenUR();
  // used for system with COM at 0,0,0
  if( ( cenmass.x() > boxl.x()/2.0 )
      || ( cenmass.x() < -1.0*boxl.x()/2.0)
      || ( cenmass.y() > boxl.y()/2.0 )
      || ( cenmass.y() < -1.0*boxl.y()/2.0)
      || ( cenmass.z() > boxl.x()/2.0 )
      || ( cenmass.z() < -1.0*boxl.x()/2.0) )
  // using boxl.x() in z direction because we want when it exits the pore
  // assumes cubic polymer system
  {
    return true;
  }
  else
    return false;
}  //! end Isthrough

/*!#########################################################*/
/*#########################################################*/
// writeSim
// Print out details about the simulation
/*#########################################################*/
/*#########################################################*/
inline void CBD::writeSim(double start, double t)
{
  double end = read_timer(  );
  cout << " comp time for this traj: " << end-start << endl;
  cout << " sim time for this traj: " << t << endl;
  cout << " sim time for this traj in NS: "<< t*PS_TO_NS << endl;
}// end writeSim

#endif

