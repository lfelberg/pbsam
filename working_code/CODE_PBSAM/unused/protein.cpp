#include <iostream>
#include <fstream>
#include "protein.h"
#include "pdb.h"

/*###############################################################################
 * #
 * # File: protein.cpp
 * #
 * # Date: June 2013
 * #
 * # Description: 
 * #
 * # Author: Yap, Felberg
 * #
 * # Copyright ( c )
 * #
 * ################################################################################*/

#define PARAM_FILE "$PBSAMHOME/charges_OPLS"

map<int, REAL> CProtein::CHARGES[NUM_AAS];

/*#########################################################*/
/*#########################################################*/
/*#########################################################*/
/*#########################################################*/

void
CProtein::loadChargeMap(  )
{
	ifstream fin;
	fin.open( PARAM_FILE );
	if ( !fin.is_open( ))
	{
		cout << "Could not open input file " << PARAM_FILE << endl;
		exit( 0 );
	}

	char buf[200];
	char rname[4], aname[5];
	float ch;
	while ( !fin.eof( ))
	{
		fin.getline( buf, 199 );
		if ( buf[0] == '#' )
			continue;

		int r = sscanf( buf, "%s %s %g", &rname, &aname, &ch );
		if ( r != 3 )
		{
			cout << "Bad input line in file " << PARAM_FILE 
			<< " : " << buf << endl;
			exit( 0 );
		}

		CHARGES[AA::getAACode( rname )][CAtom::getAtomCode(aname)] = (REAL)ch;
	}
}	// end loadChargeMap

/*#########################################################*/
/*#########################################################*/
/*#########################################################*/
/*#########################################################*/

CProtein::CProtein( const char * fname )
{
	vector<AA> aas;
	CPDB::loadFromPDB( fname, aas );

	for ( int i = 0; i < aas.size( ); i++)
		for ( int j = 0; j < aas[i].getNumAtoms( ); j++)
			m_atoms.push_back( aas[i][j] );

	for ( int i = 0; i < m_atoms.size( ); i++)
		if ( m_atoms[i].getCharge( ) != 0.0)
			m_chargedAtoms.push_back( &(m_atoms[i] ));

	m_center = computeCenter(  );
	for ( int i = 0; i < m_atoms.size( ); i++)
		m_atoms[i].setPos( m_atoms[i].getPos( ) - m_center);

	computeRadius(  );

	m_sumCharge = 0.0;
	for ( int i = 0; i < getNumCharges( ); i++)
		m_sumCharge += getCharge( i );

	cout << "net charge = " <<m_sumCharge <<" rad = "
			<< m_rad <<" center = "<<m_center <<endl;
}	// end CProtein

/*#########################################################*/
/*#########################################################*/
/*#########################################################*/
/*#########################################################*/

void
CProtein::readPDB( const char * fname, CPnt &rcen, double &rad, 
				vector<CPnt> &cpos, vector<double> &chg )
{
	vector<AA> aas;
	vector<CAtom> atoms;

	CPDB::loadFromPDB( fname, aas );

	// read in residues
	for ( int i = 0; i < aas.size( ); i++)
		for ( int j = 0; j < aas[i].getNumAtoms( ); j++)
			atoms.push_back( aas[i][j] );

	int numAtoms = atoms.size(  );

	// compute center //!!!!!!!! use compute_center_of_mass later
	rcen = CPnt(  ); 
	for ( int i = 0; i < numAtoms; i++ )
		rcen += atoms[i].getPos(  );
	rcen *= 1.0/double( numAtoms );

	// reset charge positons wrt to rcen
	for ( int i = 0; i < numAtoms; i++ )
		atoms[i].setPos( atoms[i].getPos( ) - rcen);

	// extract charged atoms
	for ( int i = 0; i < numAtoms; i++ )
		if ( atoms[i].getCharge( ) != 0.0)
		{
			cpos.push_back(  atoms[i].getPos( ) );
			chg.push_back(   atoms[i].getCharge( ) );
		}

	//  compute maximum radius
	REAL max = 0.0;
	for ( int i = 0; i < numAtoms; i++ )
	{
		REAL dist = atoms[i].getPos(  ).norm() + atoms[i].getRadius();
		if ( dist > max ) max = dist;
	}

	rad = max;

	// check total charge
	double sumCharge = 0.0;
	for ( int i = 0; i < chg.size( ); i++)
		sumCharge += chg[i];

	cout << "total # of charges "<< chg.size(  )<<" net charge = " <<sumCharge 
					<<" rad = "<< rad <<" center = "<<rcen <<endl;
}	 // end readPDB

/*#########################################################*/
/*#########################################################*/
/*#########################################################*/
/*#########################################################*/

CPnt
CProtein::computeCenter(  )
{
	CPnt p;

	for ( int i = 0; i < m_atoms.size( ); i++)
		p += m_atoms[i].getPos(  );

	p *= 1.0/m_atoms.size(  );
	return p;
} 	// computeCenter

/*#########################################################*/
/*#########################################################*/
/*#########################################################*/
/*#########################################################*/

void
CProtein::computeRadius(  )
{
	REAL max = 0.0;
	for ( int i = 0; i < m_atoms.size( ); i++)
	{
		REAL dist = m_atoms[i].getPos(  ).norm() 
						+ m_atoms[i].getRadius(  );
		if ( dist > max )
			max = dist;
	}

	m_rad = max;
}	// end computeRadius

/*#########################################################*/
/*#########################################################*/
/*#########################################################*/
/*#########################################################*/

const CAtom *
CProtein::getAtom( int resnum, int acode ) const
{
	for ( int i = 0; i < m_atoms.size( ); i++)
		if ( m_atoms[i].getResNum( ) == resnum 
					&& m_atoms[i].getCode(  ) == acode)
			return &( m_atoms[i] );

	return NULL;
}	// end getAtom

/*#########################################################*/
/*#########################################################*/
// outputs the positions ( in labframe ) of atoms in contact
/*#########################################################*/
/*#########################################################*/

void
CProtein::getInterfaceAtoms( const CProtein & P1, const CProtein & P2,
			vector <CPnt> & pos1, vector <CPnt> & pos2, double cutoff )
{
	const vector<CAtom> A1 = P1.getAtoms(  );
	const vector<CAtom> A2 = P2.getAtoms(  );

	vector<CAtom>::const_iterator it1 = A1.begin(  );
	vector<CAtom>::const_iterator it2;

	CPnt d = P1.getPosition(  ) - P2.getPosition();

	// find contact atoms for Protein 1
	pos1.clear(  );
	for ( ; it1 != A1.end( ); it1++)
	{
		if ( !it1->isHeavy( ))
			continue;

		it2 = A2.begin(  );
		for ( ; it2 != A2.end( ); it2++)
		{
			if ( !it2->isHeavy( ))
				continue;
			if ( ((it1->getPos( ) - it2->getPos()) + d).norm() <= cutoff)
			{
				pos1.push_back( it1->getPos( ) + P1.getPosition() );
				break;
			}
		}
	}

	// find contact atoms for Protein 2
	pos2.clear(  );
	it2 = A2.begin(  );
	for ( ; it2 != A2.end( ); it2++)
	{
		if ( !it2->isHeavy( ))
			continue;

		it1 = A1.begin(  );
		for ( ; it1 != A1.end( ); it1++)
		{
			if ( !it1->isHeavy( ))
				continue;
			if ( (it2->getPos( ) - it1->getPos() - d).norm() <= cutoff)
			{
				pos2.push_back( it2->getPos( ) + P2.getPosition());
				break;
			}
		}
	}
}	// end getInterfaceAtoms

