#include <cstdlib>
#include <iostream>
#include <fstream>
#include <cfloat>
#include <stdio.h>
#include <time.h>

#include "pore.h"
#include "readutil.h"

/*###############################################################################
 * #
 * # File: pore.cpp
 * #
 * # Date: June 2013
 * #
 * # Description:
 * #
 * # Author: EH Yap, L Felberg
 * #
 * # Copyright ( c )
 * #
 * ################################################################################*/

#define WRITEFREQ 500
#define DISTCUTOFF_TIME 10 // angstroms
REAL CComputePore::MINDIST, CComputePore::MAXDIST;
int CComputePore::m_nMolType;
vector< vector<CMolContact> > CComputePore::MOLCONTACTLIST;

using namespace std;

/*#########################################################*/
/*#########################################################*/
// static functions
/*#########################################################*/
/*#########################################################*/

/*#########################################################*/
/*#########################################################*/
/*#########################################################*/
/*#########################################################*/

void 
CComputePore::initConstants( int nMolType )
{
	if( DISTCUTOFF_TIME > CMolecule::m_interactRcutoff  )
	{
		MAXDIST = DISTCUTOFF_TIME; 
		MINDIST = CMolecule::m_interactRcutoff;
	} 
	else
	{
		MAXDIST = CMolecule::m_interactRcutoff;  
		MINDIST = DISTCUTOFF_TIME;
	} 
	m_nMolType = nMolType;
	MOLCONTACTLIST.resize( 1 ); // for 2 species, we just need one def for protein 1 
}	// end initConstants

/*#########################################################*/
/*#########################################################*/
/*#########################################################*/
/*#########################################################*/

void
CComputePore::addMolContact( int mol1type, 
				const vector<CMolContact> &molcontactlist )
{
	MOLCONTACTLIST[0] = molcontactlist ;
}	// end addMolContact

/*#########################################################*/
/*#########################################################*/
// member function
/*#########################################################*/
/*#########################################################*/

/*#########################################################*/
/*#########################################################*/
// construct from array placement with random orientation
/*#########################################################*/
/*#########################################################*/

CComputePore::CComputePore( int np1, int np2, 
			const vector<char*> &molfnames1, const vector<char*> &molfnames2, 
			REAL idiel ) : m_np1( np1), m_np2(np2)
{
	initMolTypes( molfnames1, molfnames2, idiel );

	cout <<"Initialization complete for "<<m_mols.size(  )<<" molecules." <<endl;
}	// end CComputPore

/*#########################################################*/
/*#########################################################*/
// read in and precompute values for each species
/*#########################################################*/
/*#########################################################*/
void
CComputePore::initMolTypes( const vector<char*> &molfnames1,
			const vector<char*> &molfnames2, REAL idiel )
{
	char* pqrfile1  = molfnames1[0];
	char* imatpath1 = molfnames1[1];
	char* exppath1  = molfnames1[2];
	char* expname1  = molfnames1[3];

	char* pqrfile2  = molfnames2[0];
	char* imatpath2 = molfnames2[1];
	char* exppath2  = molfnames2[2];
	char* expname2  = molfnames2[3];

	// read in charge + centers
	vector<CPnt> scen1, POS1, scen2, POS2;
	vector<double> srad1, CHG1,  srad2, CHG2;
	readpqr( pqrfile1, POS1, CHG1 ,srad1, scen1 );
	readpqr( pqrfile2, POS2, CHG2 ,srad2, scen2 );

	const int ncen1 = scen1.size(  );
	const int ncen2 = scen2.size(  );

	m_initialrcen1 = CPnt( 0,0,0 ), m_initialrcen2 = CPnt(0,0,0);
	for( int i=0; i<ncen1; i++ ) 
	{
		m_initialrcen1 += scen1[i]; 
		m_initialrcen1 /= scen1.size();
	}
	for( int i=0; i<ncen2; i++ ) 
	{
		m_initialrcen2 += scen2[i]; 
		m_initialrcen2 /= scen2.size();
	}
	cout <<"initialrcen1 : " <<m_initialrcen1<<endl;
	cout <<"initialrcen2 : " <<m_initialrcen2<<endl;

	//////////////// read in solved expansions /////////////
	readMats( m_iMats1, N_POLES, imatpath1, ncen1 );
	readMats( m_iMats2, N_POLES, imatpath2, ncen2 );
	REAL intraRcutoff_dum;
	readExpansionsP( m_iF1, m_iH1, N_POLES, exppath1, expname1, ncen1, intraRcutoff_dum );
	readExpansionsP( m_iF2, m_iH2, N_POLES, exppath2, expname2, ncen2, intraRcutoff_dum );

	/////////////// generate values for each moltype //////////
	const double intraRcutoff = 30;  //<<<
	vector< vector<int> > intraPolLists_far1, intraPolLists_far2;

	// protein 1
	CMolecule::generateMolSPX( scen1, srad1, m_SPxes1, m_nSPx1, m_neighs1  );
	CMolecule::generateMolExposedChargesFH( m_iF1, m_iH1, scen1, srad1, m_SPxes1, m_nSPx1,
									 m_qSolvedF1, m_qSolvedH1, m_totalF1, m_totalH1 );
	CMolecule::generateMolCells( scen1, srad1, m_initialrcen1, m_molcells1  );

	CMolecule::generateMolTypeIntraPolLists( scen1, srad1, intraRcutoff,
									  m_intraPolLists_near1, intraPolLists_far1 );

	CMolecule::computeMolTypeValues( m_initialrcen1, scen1, srad1, CHG1, POS1, idiel,
							  intraRcutoff,
							  m_SPxes1, m_nSPx1, m_neighs1,
							  m_iF1, m_iH1, m_qSolvedF1, m_qSolvedH1, m_totalF1, m_totalH1,
							  m_intraPolLists_near1,
							  intraPolLists_far1,
							  m_LFs_intraSelf1, m_LHs_intraSelf1,
							  m_LFs_intraSelf_far1, m_LHs_intraSelf_far1 );

	// protein 2
	CMolecule::generateMolSPX( scen2, srad2, m_SPxes2, m_nSPx2, m_neighs2  );
	CMolecule::generateMolExposedChargesFH( m_iF2, m_iH2, scen2, srad2, m_SPxes2, m_nSPx2,
									 m_qSolvedF2, m_qSolvedH2, m_totalF2, m_totalH2 );
	CMolecule::generateMolCells( scen2, srad2, m_initialrcen2, m_molcells2  );

	CMolecule::generateMolTypeIntraPolLists( scen2, srad2, intraRcutoff,
									  m_intraPolLists_near2, intraPolLists_far2 );

	CMolecule::computeMolTypeValues( m_initialrcen2, scen2, srad2, CHG2, POS2, idiel,
							  intraRcutoff,
							  m_SPxes2, m_nSPx2, m_neighs2,
							  m_iF2, m_iH2, m_qSolvedF2, m_qSolvedH2, m_totalF2, m_totalH2,
							  m_intraPolLists_near2,
							  intraPolLists_far2,
							  m_LFs_intraSelf2, m_LHs_intraSelf2,
							  m_LFs_intraSelf_far2, m_LHs_intraSelf_far2 );

	// create molecules
	m_mols.clear(  );

	for( int i=0; i < m_np1; i++ )
	{
		m_mols.push_back(  new CMolecule( 0, m_initialrcen1, scen1, srad1, CHG1, POS1, idiel,
					  m_iMats1, intraRcutoff,
					  m_SPxes1, m_nSPx1, m_neighs1, m_intraPolLists_near1,
					  m_iF1, m_iH1, m_LFs_intraSelf1, m_LHs_intraSelf1,
					  m_LFs_intraSelf_far1, m_LHs_intraSelf_far1,
					  m_qSolvedF1, m_qSolvedH1, m_totalF1, m_totalH1, m_molcells1 ) );
	}

	for( int i=0; i < m_np2; i++ )
	{
		m_mols.push_back(  new CMolecule( 1, m_initialrcen2, scen2, srad2, CHG2, POS2, idiel,
					  m_iMats2, intraRcutoff,
					  m_SPxes2, m_nSPx2, m_neighs2, m_intraPolLists_near2,
					  m_iF2, m_iH2, m_LFs_intraSelf2, m_LHs_intraSelf2,
					  m_LFs_intraSelf_far2, m_LHs_intraSelf_far2,
					  m_qSolvedF2, m_qSolvedH2, m_totalF2, m_totalH2, m_molcells2 ) );
	}

	assert( m_mols.size( ) == m_np1+m_np2);
	// =========================================================
	const REAL interRCutoff = 1000; // <<<<<<<<<<<<<<<<<<<<<<<<
	const REAL interactRCutoff = 100; // <<<<<<<<<<<<<<<<<<<<<<<<
	CMolecule::initMutualConstants( m_mols, interRCutoff, 
								interactRCutoff, true  );
	cout << "interRcutoff = "<<interRCutoff<<endl;
	cout << "interactRcutoff = "<<interactRCutoff<<endl;
	// =========================================================  

	return;
}	// end  initMolTypes

/*#########################################################*/
/*#########################################################*/
/*#########################################################*/
/*#########################################################*/

double
CComputePore::calculatePot( vector<CMolecule*>  m_mols )
{ 
	CMolecule::writeMolsPQR( "initial.pqr", m_mols );

	/*
	bool bInterXFS =  CMolecule::generateInterXFormsForPolarize_LowMemory( m_mols );
	if( bInterXFS )  cout<<"inter polarize list constructed"<<endl;
	bool bPot=true;
	CMolecule::polarize_mutual( m_mols,bPot, 1000 );
	*/
	//  int npmol=m_mols.size(  );
	//  vector<CPnt> forcell( npmol ),torquell(npmol);
	//  bool bhaha=CMolecule::computeForces( m_mols,forcell,torquell );
	//  cout << "force and torque calculated"<<endl;

	double potential= CMolecule::computeTotalIntEnergy( m_mols );
	cout<<"potential energy is"<<potential<<endl;
	return potential;
}	// end calculatePot

/*#########################################################*/
/*#########################################################*/
// initialize positions, orientation and moltypes on a lattice
/*#########################################################*/
/*#########################################################*/
void 
CComputePore::setconfig( int ix,double deltax )
{
	// generate all positions and orientation in the array, 
	// regardless of moltype
	vector<CPnt> rcens;
	vector<CQuat> rots;

	REAL px,py,pz;

	px = deltax*ix;
	py = 0;
	pz = 0;
	rcens.push_back(  CPnt(px,py,pz ) );
	rots.push_back (   CQuat::chooseRandom( ) );

	// assign molecules randomly to the generated positions/orientation
	m_mols[1]->setPos(  rcens[0]  );
	m_mols[1]->setOrient(  rots[0]  ); 

	// check that nothing collides ( use direct method instead of cell )
	/*  for( int i=0; i < number_of_monomers; i++ )
	if(  m_mols[i]->isCollided(m_mols ) )  
	{
	cout <<"Error : initial config clashes"<<i<<endl;
	exit( 1 );
	}
	*/
	return;
}	// end CComputePore::setconfig


/*#########################################################*/
/*#########################################################*/
// Commented out in old code
/*#########################################################*/
/*#########################################################*/

/*
double
CComputePore::propagate( double minDist, bool bWriteTime )
{
  // compute minimum distance to decide timestep and whether to compute forces
  bool bComputeForce = ( minDist < CMolecule::m_interactRcutoff  ) ;  
  double dt = compute_dt( minDist );
 
  // compute forces
  const int nmol = m_mols.size(  );
  vector<CPnt> force( nmol ), torque(nmol);
  vector<CPnt> dR( nmol ), dO(nmol); 
#ifndef __DIFF__

  double t1 = read_timer(  );

  if( bComputeForce ) {
  bool bBlown = CMolecule::computeForces( m_mols, force, torque );
  if( !bBlown ) 
    {
      cout <<"died somewherein computeforces"<<endl; 
      CMolecule::saveConfig( "died.config", m_mols );
      CMolecule::writeMolsPQR( "died.pqr", m_mols );
      exit( 1 );
    }
  }
#else
  force.clear(  ); force.resize(nmol);
  torque.clear(  ); torque.resize(nmol);
#endif

  double t2 = read_timer(  );      
  // move molecules
  for( int i=0; i<nmol; i++ )
    {
      int m = m_mols[i]->getMolType(  );
      dR[i] = ( m_Dtr[m]*dt*FACT2 )*force[i];
      dO[i] = ( m_Dr[m]*dt*IKbT*COUL_K )*torque[i];
      
    }// end-i

  makeMove( dR, dO, dt );
  double t3 = read_timer(  );

  if( bWriteTime ) cout <<"  computeForces "<<t2-t1<<" move "<<t3-t2<<endl;

  return dt;      
}


// assume DISTCUTOFF_TIME = CMolecule::m_interRcutoff to simplify
REAL 
CComputePore::computeMinSepDist(  )     
{ 
  double minsepdist = DBL_MAX;
  double minM2Msepdist = DBL_MAX;
  const int nmol = m_mols.size(  );
  bool bTrackingS2S = false;
  
  for( int i=0; i<nmol; i++ )
      for( int j=i+1; j<nmol; j++ )
	{
	  double dM2M = CMolecule::getSepDist( m_mols[i], m_mols[j] ); 

	  // first check if they are close enough that we care about
	  if( dM2M < MAXDIST  )
	    {
	      if( !bTrackingS2S ) bTrackingS2S = true;
	      // evaluate it until it reaches the small limit that we care about
	      for( int ki=0; ki<m_mols[i]->getNKS( ); ki++)
		{

		  for( int kj=0; kj<m_mols[j]->getNKS( ); kj++)
		    {
		      double d = CMolecule::getSepDist( m_mols[i], ki, m_mols[j], kj );
		      if(  d < minsepdist  )
			{
			  if(  d < MINDIST  ) { return MINDIST; }
			  else minsepdist = d;
			}
		    }// end kj 

		}// end k
	    }
	  // otherwise keep track of mol-to-mol distance just in case
	  else if ( !bTrackingS2S )
	    {
	      if( dM2M < minM2Msepdist ) minM2Msepdist  = dM2M;
	    }
	  
	}//end ( i,j )
    
  
  if( bTrackingS2S ) return  minsepdist;
  else  return ( minM2Msepdist > 0 ?  minM2Msepdist : 0 ); 
  
}
*/

/*
// assume DISTCUTOFF_TIME = CMolecule::m_interRcutoff to simplify
REAL 
CComputePore::computeMinSepDist(  )     
{ 
  double minsepdist = DBL_MAX;
  double minM2Msepdist = DBL_MAX;
  const int nmol = m_mols.size(  );
  bool bTrackingS2S = false;
  //bool bMinReached = false;
  
  //#pragma omp parallel shared( bMinReached, bTrackingS2S, minsepdist, minM2Msepdist )
  {
    for( int i=0; i<nmol; i++ )
      for( int j=i+1; j<nmol; j++ )
	{
	  double dM2M = CMolecule::getSepDist( m_mols[i], m_mols[j] ); 
	  // first check if they are close enough that we care about
	  if( dM2M < MAXDIST  )
	    {
	      if( !bTrackingS2S ) bTrackingS2S = true;
	      // evaluate it until it reaches the small limit that we care about
	      for( int ki=0; ki<m_mols[i]->getNKS( ); ki++)
		{
		  //#pragma omp for
		  for( int kj=0; kj<m_mols[j]->getNKS( ); kj++)
		    {
		      double d = CMolecule::getSepDist( m_mols[i], ki, m_mols[j], kj );
		      if(  d < minsepdist  )
			{
			  if(  d < MINDIST  ) { return MINDIST; }
			  else minsepdist = d;
			}
		    }// end kj ( parallel for )

		}// end k
	    }
	  // otherwise keep track of mol-to-mol distance just in case
	  else if ( !bTrackingS2S )
	    {
	      //#pragma omp master 
	      {
		if( dM2M < minM2Msepdist ) minM2Msepdist  = dM2M;
	      }
	    }
	  
	}//end ( i,j )
    
  }// end parallel
  
  if( bTrackingS2S ) return  minsepdist;
  else  return ( minM2Msepdist > 0 ?  minM2Msepdist : 0 ); 
  
}
*/

