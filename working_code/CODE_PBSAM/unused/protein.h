#ifndef _PROTEIN_H_
#define _PROTEIN_H_

#include <map>
#include "pdb.h"
#include "util.h"

/*###############################################################################
 * #
 * # File: protein.h
 * #
 * # Date: June 2013
 * #
 * # Description:
 * #
 * # Author: Yap, Felberg
 * #
 * # Copyright ( c )
 * #
 * ################################################################################*/

using namespace std;

/*#########################################################*/
/*#########################################################*/
/*#########################################################*/
/*#########################################################*/

class CProtein
{
	public:
		static void readPDB( const char * fname, CPnt &rcen, double &rad, 
					vector<CPnt> &cpos, vector<double> &chg );
		static void loadChargeMap(  );
		static void computeForces( vector<CPnt> & force, vector<CPnt> & torque );
		static void getInterfaceAtoms( const CProtein & P1, const CProtein & P2,
					vector <CPnt> & pos1, vector <CPnt> & pos2, double cutoff );

		CProtein( const char * fname );
		int getNumAtoms(  ) const
			{ return m_atoms.size(  ); }

		int getNumCharges(  ) const
			{ return m_chargedAtoms.size(  ); }

		REAL getCharge( int i ) const
			{ return m_chargedAtoms[i]->getCharge(  ); }

		const CPnt & getChargePos( int i ) const
			{ return m_chargedAtoms[i]->getPos(  ); }

		const CPnt & getAtomPos( int i ) const
			{ return m_atoms[i].getPos(  ); }

		REAL getRadius(  ) const
			{return m_rad; }
		REAL getSumCharge(  ) const
			{ return m_sumCharge; }
		const CPnt & getPosition(  ) const
			{return m_center; }
		const vector<CAtom> & getAtoms(  ) const
			{ return m_atoms; }
		const CAtom * getAtom( int resnum, int acode ) const;
		int getID(  ) const
			{ return m_id; }

		static map<int, REAL> CHARGES[NUM_AAS];

	private:
		CPnt computeCenter(  );
		void computeRadius(  );

		vector<CAtom> m_atoms;
		vector<const CAtom*> m_chargedAtoms;

		CPnt m_center;
		REAL m_rad;
		REAL m_sumCharge;
		int m_id;
};

#endif
