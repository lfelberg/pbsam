#ifndef _TBD_H_
#define _TBD_H_
#include <fstream>
#include <vector>

#include "leaker.h"
#include "readutil.h"
#include "main_util.h"
#include "setup.h"
#include "mpe.h"
#include "mcoeff.h"

void delete_mpe_sub(vector<CMPE*> & mpes);

void cutoffTBD(vector<CPnt *> &cen, double cutoffDist, int nMol, 
                vector<vector<int> > &dimer, vector<vector<int> > &trimer);

void computeKBod(CSetup initAll, vector<vector<int> > nmer, int num, 
         vector<CPnt *> cens, double radius, vector<double> charg, 
         vector<CPnt>  chargpos, vector<REAL> &potNb, 
         vector<CPnt> &forceNb, vector<CPnt> &torqueNb);

void runThreeBod(CSetup initAll, const char *PQRname, const char *XYZname, 
        int num, vector<CPnt *> &cen,
        double &potTot, vector<double> &pot, 
        vector<CPnt> &force, vector<CPnt> &torque);

#endif
