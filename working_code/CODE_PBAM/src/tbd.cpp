#include <fstream>
#include <vector>

#include "tbd.h"
#include "leaker.h"
#include "mcoeff.h"

/******************************************************************/
/******************************************************************//**
 * Clear up memory for 3body
 *****************************************************************/
void delete_mpe_sub(vector<CMPE*> & mpes)
{
  int counter=0;
  while(!mpes.empty())
  {
    int i = mpes.size() - 1;
    mpes[i]->cleanup(counter);
    delete mpes.back(); 
    mpes.pop_back(); 
    counter++;
  }
} // end delete_mpe_sub

/******************************************************************/
/******************************************************************//**
 * cutoffTBD: a function that creates a list of trimers and dimers
 *            and trimers that fall within the given cutoff
 *   \param cen a vector of pointers containing the positions
 *                 of all the molecules in the system
 *   \param cutoffDist a floating point of desired cutoff for
 *                kbody interactions
 *   \param nMol, the number of molecules in the system
 *   \param dimer, a vector of vectors containing all
 *             molecules in dimers of system
 *   \param trimer, a vector of vectors containing all
 *            molecules in trimers of system
 *   \return Nothing, just fill in the dimer and trimer vecs
 ******************************************************************/
void cutoffTBD(vector<CPnt *> &cen, double cutoffDist, int nMol, 
                vector<vector<int> > &dimer, vector<vector<int> > &trimer)
{
  vector<int> temp; int M2=nMol*(nMol-1)/2; int M3=nMol*(nMol-1)*(nMol-2)/6;

  for(int i=0;i<nMol;i++)
    for(int j=i+1;j<nMol;j++)
    {
      CPnt dist1 = (*cen[i]) - (*cen[j]);
      if (cutoffDist > dist1.norm())
      {
          temp.clear(); temp.resize(2);
          temp[0] = i; temp[1]=j;
          dimer.push_back(temp);
      }
      for(int k=j+1;k<nMol;k++)
      {
        CPnt dist2 = (*cen[i]) - (*cen[k]); CPnt dist3 = (*cen[j]) - (*cen[k]);
        if (cutoffDist*2.0 > (dist1.norm() + dist2.norm() + dist3.norm() ))
        {
          temp.clear(); temp.resize(3);
          temp[0] = i; temp[1] = j; temp[2] = k;
          trimer.push_back(temp);
        }
      }
    }

    cout << "Cutoffs implemented, using " << cutoffDist << endl;
    cout<<"Max # of di: "  <<M2<< "  Act used: " << dimer.size() <<endl;
    cout<<"Max # of tri: " <<M3<< "  Act used: " << trimer.size() <<endl;
} //end cutoffTBD

/******************************************************************/
/******************************************************************//**
 * Function the initializes nbody systems, makes mpes, solves and
 *    computes potential, forces and torques
 *   \param nmer
 *   \param num an integer number of molecules in kbody approx
 *   \param cens, a vector of all positions of molecules in system
 *   \param radius a floating point of the scale of system
 ******************************************************************/
void computeKBod(CSetup initAll, vector<vector<int> > nmer, 
         int num, vector<CPnt *> cens, 
         double radius, vector<double> charg, vector<CPnt>  chargpos, 
         vector<REAL> &potNb, vector<CPnt> &forceNb, vector<CPnt> &torqueNb)
{
  vector<CMPE *> mpes; vector<CPnt *> cen_kbod;
  vector<REAL> pottemp; vector<CPnt> forcetemp,torquetemp;

  for(int i=0;i<nmer.size();i++)
  {
    CProtein::initParameters(initAll.getKappa(), initAll.getIDiel(), initAll.getSDiel(),
                               num, radius);
    cout << "This nmer is: ";
    for (int j=0;j<num;j++)
    {
      cout << nmer[i][j] << " and " ;
      mpes.push_back( new CMPE( charg, chargpos, radius, j, 15 ));
      cen_kbod.push_back( new CPnt((*cens[nmer[i][j]]).x(),
                          (*cens[nmer[i][j]]).y(), (*cens[nmer[i][j]]).z()) );
    }
    cout << endl;

    CMPE::initXForms(mpes);
    CMPE::solve(mpes, cen_kbod, false);

    pottemp.clear(); pottemp.resize(num);
    forcetemp.clear(); forcetemp.resize(num);
    torquetemp.clear(); torquetemp.resize(num);
    CMPE::computeForce(mpes, cen_kbod, pottemp, forcetemp, torquetemp);
    cout << "Pot, force for all: ";

    for (int j=0;j<num;j++)
    {
      cout << pottemp[j] << "\t" << forcetemp[j] << "\t";
      potNb[nmer[i][j]]+=pottemp[j];
      forceNb[nmer[i][j]]+=forcetemp[j];
      torqueNb[nmer[i][j]]+=torquetemp[j];
    }
    cout << endl;
    delete_mpe_sub(mpes);
    while(!cen_kbod.empty()) delete cen_kbod.back(), cen_kbod.pop_back();
    mpes.clear(); cen_kbod.clear();
  }
  cout << num << "mers done " << endl;
} //end computeKBod


/******************************************************************/
/******************************************************************//**
 * Function that runs a threebody approximation
 *    computes potential, forces and torques
 *   \param num an integer number of molecules in kbody approx
 *   \param cens, a vector of all positions of molecules in system
 *   \param radius a floating point of the scale of system
 ******************************************************************/
void runThreeBod(CSetup initAll, const char *PQRname, const char *XYZname, 
                  int num,vector<CPnt *> &cen,
                  double &potTot,vector<double> &pot,
                  vector<CPnt> &force, vector<CPnt> &torque)
{
  double radius, cutoff = 50000.0; 
  vector<CPnt> chargpos; vector<REAL> charg;
  int M2=num*(num-1)/2; int M3=num*(num-1)*(num-2)/6;

  vector<REAL> pot2b(num, 0.0), pot3b(num, 0.0);
  vector<CPnt> force2b(num, CPnt(0,0,0)),torque2b(num, CPnt(0,0,0));
  vector<CPnt> force3b(num, CPnt(0,0,0)),torque3b(num, CPnt(0,0,0));
  vector<vector<int> > dimer, trimer;

  cout << "This is my KAPPA: " << initAll.getKappa() <<  " TEMP: " << initAll.getTemp() << endl;

  radius = getCoords(PQRname, num, true, charg, chargpos, cen, XYZname);
  initParm( initAll, num, radius );
  cutoffTBD(cen, cutoff, num, dimer, trimer);

  computeKBod(initAll, dimer, 2, cen, radius, charg, chargpos,
                 pot2b,force2b,torque2b);
  computeKBod(initAll, trimer, 3, cen, radius, charg, chargpos,
                 pot3b,force3b,torque3b);

  for(int i=0;i<num;i++)
  {
    pot[i]=(pot3b[i] - (num-3)*pot2b[i]);
    potTot+=pot[i];
    force[i]=force3b[i] - (num-3)*force2b[i];
    torque[i]=torque3b[i] - (num-3)*torque2b[i];
  }
} // end runThreeBod



