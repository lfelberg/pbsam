#ifndef _MAIN_UTIL_H_
#define _MAIN_UTIL_H_

#include "readutil.h"
#include "setup.h"
#include "protein.h"

void argCheck( int argCount );

void printPositions(vector<CPnt *> &cen);

void initParm( CSetup initAll, int num, double rad1 );

void printEnForTor( CSetup initAll, int num, vector<CPnt *> &cen,double potTot, 
               vector<double> pot,vector<CPnt> force,vector<CPnt> torque);

void buildGrid(const char *ifname, int num, REAL rad,
    const vector<CMPE *> &mpe, const vector<CPnt *> &cen, REAL fact);

void perturb(int ct, int num, ofstream &fout, REAL Dtr, REAL Dr, REAL dt,
    vector<CMPE *> &mpe, vector<CPnt *> &cen);

#endif
