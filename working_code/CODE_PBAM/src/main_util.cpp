#include "main_util.h"

/******************************************************************/
/******************************************************************//**
 * argCheck
 * \param argCount an int of the number of arg inputs
*******************************************************************/
void argCheck( int argCount )
{
  if ( argCount != 2 )
  {
    cout << "Correct input format: " << endl;
    cout << " ./exec run.inp " << endl;
    exit(0);
  }
}//end argCheck

/******************************************************************/
/******************************************************************//**
 * printPositions
 * \param cen a vector of molecule positions
*******************************************************************/
void printPositions(vector<CPnt *> &cen)
{
  for (int i = 0; i < cen.size(); i++) cout << *(cen[i]);
  cout << endl;
}// end printPositions


/******************************************************************/
/******************************************************************//**
 * initParm
 * \param 
*******************************************************************/
void initParm( CSetup initAll, int num, double rad1 )
{
  // Initialize parameters for the system, using hard-coded values,
  cout << "System parameters: Salt Conc [molar] : " << initAll.getSaltConc()
        << " \t Temp: " << initAll.getTemp() << " \t EPS_in: " << initAll.getIDiel()
        << " \t EPS_out: " << initAll.getSDiel() << endl;
  CProtein::initParameters(initAll.getKappa(), initAll.getIDiel(), 
														initAll.getSDiel(), num, rad1);

} // end initParm

/******************************************************************/
/******************************************************************//**
 * printEnForTor
 * \param 
 * \param 
*******************************************************************/
void printEnForTor( CSetup initAll, int num, vector<CPnt *> &cen,double potTot, 
               vector<double> pot,vector<CPnt> force,vector<CPnt> torque)
{
  string units = "internal";//"J per mole"; //"kCal per mole"; // "J per mole" // "kT"
  //string units = "J per mole"; //"kCal per mole"; // "J per mole" // "kT"
  //string units = "kCal per mole"; // "J per mole" // "kT"
  cout << "UNITS are : " << units << endl;
  double fact;
  // for units of kCal/mole

  if (units == "internal") fact = 1.0 / initAll.getSDiel();
  else if (units == "kCal per mole") fact = INT_TO_KCALMOL / initAll.getSDiel();  
  else if (units == "kT") fact = INT_TO_KCALMOL / initAll.getSDiel() * initAll.getIKbT();
  else if (units == "J per mole") fact = INT_TO_JMOL / initAll.getSDiel();  
  cout << "\tTotal ENERGY: " << fact*potTot << endl;
  cout << " Next 3 lines: Energy all mols, Force all, torq all" << endl;
  for (int i = 0; i < num; i++) 
    cout << fact*pot[i] << " \t " ;
  cout << endl;
  for (int i = 0; i < num; i++) 
    cout << fact*force[i] << " \t " ;
  cout << endl;
  for (int i = 0; i < num; i++) 
    cout << fact*torque[i] << " \t " ;
  cout << endl;
  /*for (int i = 0; i < num; i++) 
  {
    cout << "MOLECULE #" << i + 1 << endl;
    cout << "\tPOSITION: " << (*cen[i]) << endl;
    cout << "\tENERGY: " << fact *pot[i] << endl;
    force[i] *= fact; torque[i] *= fact;
    cout << "\tFORCE: " << force[i].norm() << " " << force[i] << endl;
    cout << "\tTORQUE: " << torque[i].norm() << " " << torque[i] << endl;
  } */
}// end printEnForTor

/******************************************************************/
/******************************************************************//**
 * getRandVec function
  *! \param std a floating point of random vector length
      \return an CPnt object of random orientation and lenght std
 ******************************************************************/
CPnt getRandVec(REAL std) 
{
	return std * CPnt(normRand(), normRand(), normRand());
}


/******************************************************************/
/******************************************************************//**
 * Building a grid centered at ( 0, 0, 0 ) of bounded from -bd to +bd.
 *
 *   \param ifname a character string of an input filename
 *   \param num an integer of the number of molecules in the system
 *   \param rad a floating point of the radius of the molecules
 *   \param mpe a vector of multipole expansions, one for each molecule
 *   \param cen a vector of molecule positions
 *   \param fact a floating point of the conversion factor for the system
 ******************************************************************/
void buildGrid(const char *ifname, int num, REAL rad,
    const vector<CMPE *> &mpe, const vector<CPnt *> &cen, REAL fact) {
  cout << "building grid" << endl;
  REAL scale = 2.0; // Scaling factor
  // int gsize = 301;  // Grid edge length
  int gsize = 151;  // Grid edge length
  int gcen = gsize / 2 + 1;
  int bd = gcen - 1;
  float *V = new float[gsize * gsize * gsize]; // Volume of the grid cell

  REAL iscale = 1.0 / scale;          // 1/Scaling factor
  REAL r = rad * rad * scale * scale; // Scaled radius, r = ract^2 / 2^2

  int gsizeq = gsize * gsize;
  cout << "here" << endl;
  // For all 3 Dimensions, i, j and k
  for (int i = -bd; i <= bd; i++) {
    for (int j = -bd; j <= bd; j++)
      for (int k = -bd; k <= bd; k++) {
        bool cont = false;

        // for each particle, compute the distance^2 between current
        // pos. and 2*center of each particle in the sys
        for (int n = 0; n < num; n++) {
          REAL dis_sq
                  = (CPnt(i, j, k) - (scale * (*cen[n]))).normsq();
          if (dis_sq < r) { // If the distance is less that scaled
                            // rad^2, then the point is within the
                            // molecule
            cont = true;
            break;
          }
        }

        if (cont) { // If the point is within the molecule, then the
                    // potential there is zero
          V[(k + bd) * gsizeq + (j + bd) * gsize + (i + bd)] = 0.0;
          continue;
        }
        // otherwise, the potential can be computed and stored
        CPnt P(i, j, k);
        V[(k + bd) * gsizeq + (j + bd) * gsize + (i + bd)]
            = (float)CMPE::computePotAt(mpe, cen, iscale * P)
              * fact;
      }

    cout << i << "..";
    cout.flush();
  }

  cout << endl << "done" << endl;
  char ofname[100];
  strcpy(ofname, ifname);
  int l = strlen(ifname);
  sprintf(&(ofname[l - 7]), "_%dP.gmpe", num);

  ofstream fd;
  fd.open(ofname);

  // For all 3 Dimensions, i, j and k
  for (int i = -bd; i <= bd; i++) {
    for (int j = -bd; j <= bd; j++) {
      for (int k = -bd; k <= bd; k++) {
        fd << V[(k + bd) * gsizeq + (j + bd) * gsize + (i + bd)]
            << "  ";
      }
      fd << endl;
    }
    fd << "\n\n";
  }
  fd.close();
  return;

} // end buildGrid

/******************************************************************/
/******************************************************************//**
 * Computing the perturbation
 ******************************************************************/
void perturb(int ct, int num, ofstream &fout, REAL Dtr, REAL Dr, REAL dt,
    vector<CMPE *> &mpe, vector<CPnt *> &cen) 
{
  CPnt rot;
  vector<CPnt> per(num); vector<CQuat> Q0(num);
  for (int j = 0; j < num; j++) Q0[j] = mpe[j]->getOrient();

  for (int k = 0; k < ct; k++) {
    cout << "------ " << k << " -------" << endl;
    for (int j = 0; j < num; j++) {
      per[j] = getRandVec(sqrt(2 * dt * Dtr));
      *(cen[j]) += per[j];

      rot = getRandVec(sqrt(2 * dt * Dr));
      CQuat Q(rot, rot.norm());
      mpe[j]->setOrient(Q * Q0[j]);
    }

    printPositions(cen);
    CMPE::updateSolve(mpe, cen);
    cout << "Done in perturb of setting orient " << endl;
    for (int i = 0; i < num; i++) mpe[i]->saveUndo();

    cout << "Done after saveUndo" << endl;
    CMPE::undoXForms();
    cout << "Done after undoXForms" << endl;
    for (int i = 0; i < num; i++) {
      mpe[i]->undo();
      *(cen[i]) -= per[i];
    cout << "Done after undo" << endl;
    }
  }
} // end perturb

