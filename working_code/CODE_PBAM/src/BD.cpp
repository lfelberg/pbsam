#include <fstream>
#include <vector>

#include "BD.h"

const char CBD::STATUS_STRING[3][10] = {"ESCAPED", "DOCKED", "RUNNING"};

/******************************************************************/
/******************************************************************//**
 * Initialize CBD class
 *****************************************************************/
CBD::CBD(const char *fname1, const char *fname2, REAL kappa, CSetup initAll) 
    : m_fcnt(0) 
{
	if (!CProtein::isInitialized())
		// Initialize the system parameters, w/ 2 molecules, and RS=lambda in
		// paper=30
		CProtein::initParameters(kappa,initAll.getIDiel(),
          initAll.getSDiel(), 2, 30);

	m_mol1 = new CProtein(fname1); // MOLECULE 1 = Barnase
	m_mol2 = new CProtein(fname2); // MOLECULE 2 = Barstar
	// Closest distance that the proteins may be to each other
	m_maxContDist = m_mol1->getRadius() + m_mol2->getRadius() + 1.0;

	m_Dtr = initAll.getDtr(0); //0.030;// Translational diffusion coefficient
	m_Dr1 = initAll.getDrot(0);//4e-5;// Rotational diffusion coefficient barnase
	m_Dr2 = initAll.getDrot(1);//4.5e-5;// Rotational diffusion coefficient barstar

  m_sdiel = initAll.getSDiel();
  m_idiel = initAll.getIDiel();
  m_iKT   = initAll.getIKbT();

	computeInterfaceVectors(fname1, fname2);
}

/******************************************************************/
/******************************************************************//**
 * run: Runs a BD simulation for 2 proteins.  Initializes one
 *      at the origin and the other at some distance b_DIST away with
 *      random orientation. While the status is RUNNING, the system
 *      computes forces and then moves the 2nd protein and rotates
 *      both while keeping the first fixed at the origin.  It prints
 *      out the position of each molecule, their distance and the
 *      force and torques of the system every 1000 steps.
 ******************************************************************/
CBD::STATUS CBD::run(int &count, char tempfile[]) 
{
	ofstream fout(tempfile);
	//m_mol1->setOrient(CQuat::chooseRandom());
	m_mol1->setOrient(CQuat());
	m_mol1->setPos(CPnt());
	//m_mol2->setPos(b_DIST * randOrient());
	m_mol2->setPos(CPnt(0,5,0));
	//m_mol2->setOrient(CQuat::chooseRandom());
	m_mol2->setOrient(CQuat());

  cout << " Prot 1 " << m_mol1->getPosition() << endl; 
  vector<CAtom> at = m_mol1->getAtoms();
  for (int i=0; i< at.size(); i++)
      cout << " At " << i << " pos: " << at[i].getPos() << endl;
  cout << " Prot 2 " << m_mol2->getPosition() << endl;
  at.clear(); at =  m_mol2->getAtoms();
  for (int i=0; i< at.size(); i++)
      cout << " At " << i << " pos: " << at[i].getPos() << endl;
	

  vector<CPnt> force(2), torque(2);
	vector<REAL> pot;
	// Conversion from internal units
	REAL fact = m_iKT * INT_TO_JMOL / AVOGADRO_NUM / m_sdiel;
	// Sum of protein radii
	REAL srad = m_mol1->getRadius() + m_mol2->getRadius();
	STATUS status = RUNNING;
	count = 0;
	CPnt dR2, dO1, dO2;
	// 2 steps to BD run, compute dt and force computation
	while (status == RUNNING) 
	//while (count < 10) 
  {
		REAL dt = compute_dt();
		REAL dist = CProtein::computeDistance(*m_mol1, *m_mol2);
		// If two proteins are within cutoff, compute forces
		if (dist < f_DIST) 
    {
			CProtein::computeForces(force, torque);

			dR2 = (m_Dtr * dt * fact) * force[1]; // Move 2nd protein
			// Rotate both proteins
			dO1 = (m_Dr1 * dt * fact) * torque[0];
			dO2 = (m_Dr2 * dt * fact) * torque[1];
      //cout << " Dr " << m_Dr1 << " and dt " << dt << " & To " << torque[0] << " and drot " << dO1 << endl;
      //cout << " Dr " << m_Dr2 << " and dt " << dt << " & To " << torque[1] << " and drot " << dO2 << endl;
		} else {
			dR2.zero();
			dO1.zero();
			dO2.zero();
		}

		if (count % 1000 == 0) 
    { // Print out details every 1000 steps
			CPnt t = m_mol2->getPosition() - m_mol1->getPosition();
			fout << count << ") "<< setprecision(9) << m_mol1->getPosition()
					<< m_mol2->getPosition() << "-> " << dist << endl;
			fout << force[1] << " " << torque[1] << endl;

			/*cout << " Prot 1 " << m_mol1->getPosition() << endl; 
			at.clear(); at = m_mol1->getAtoms();
			for (int i=0; i< at.size(); i++)
					cout << setprecision(9) <<" At " << i << " pos: " << at[i].getPos() << endl;
			cout << " Prot 2 " << m_mol2->getPosition() << endl;
			at.clear(); at =  m_mol2->getAtoms();
			for (int i=0; i< at.size(); i++)
					cout << setprecision(9) <<" At " << i << " pos: " << at[i].getPos() << endl; */

		}

		// Move system with given dr, d angle and dt
		status = makeMove(dR2, dO1, dO2, dt);
		count++;
	}

	fout << CBD::STATUS_STRING[status] << endl;
	return status;
} // end CBD::run

/******************************************************************/
/******************************************************************//**
 * makeMove
 ******************************************************************/
CBD::STATUS CBD::makeMove(const CPnt &dR2, const CPnt &dO1,
		const CPnt &dO2, REAL dt) {
	REAL bdist = CProtein::computeDistance(*m_mol1, *m_mol2);
	int c = 0;
	while (true) {
		c++;
		if (c > 500) cout << "stuck inside loop" << endl;
		CPnt dR2_ = dR2 + getRandVec(sqrt(2 * dt * m_Dtr));
		m_mol2->translate(dR2_);

		if (!m_mol1->inCollision(*m_mol2))
			break;
		else
			m_mol2->untransform();
	}

	if (escaped(q_DIST)) return ESCAPED;

	CPnt dO1_ = dO1 + getRandVec(sqrt(2 * dt * m_Dr1));
	CPnt dO2_ = dO2 + getRandVec(sqrt(2 * dt * m_Dr2));

	CQuat Q1(dO1_, dO1_.norm());
	CQuat Q2(dO2_, dO2_.norm());

	REAL adist = CProtein::computeDistance(*m_mol1, *m_mol2);
	if (adist > f_DIST) {
		m_rot1 = Q1 * m_rot1;
		m_rot2 = Q2 * m_rot2;
	} else {
		if (bdist > f_DIST) {
			m_mol1->rotate(m_rot1);
			m_mol2->rotate(m_rot2);

			m_orth1 = m_rot1 * m_orth1;
			m_patch1 = m_rot1 * m_patch1;
			m_orth2 = m_rot2 * m_orth2;
			m_patch2 = m_rot2 * m_patch2;

			m_rot1.identity();
			m_rot2.identity();
		} else {
			m_mol1->rotate(Q1);
			m_mol2->rotate(Q2);

			m_orth1 = Q1 * m_orth1;
			m_patch1 = Q1 * m_patch1;
			m_orth2 = Q2 * m_orth2;
			m_patch2 = Q2 * m_patch2;
		}
	}

	if (isDocked())
		return DOCKED;
	else
		return RUNNING;
}

/******************************************************************/
/******************************************************************//**
 * isDocked function to determine whether or not the moving protein has
 * docked on the other
 ******************************************************************/
bool CBD::isDocked() const {
	REAL d = CProtein::computeDistance(*m_mol1, *m_mol2);
	// cout << d << " " << m_maxContDist << endl;
	if (d > m_maxContDist) return false;

	CPnt v = (m_mol2->getPosition() - m_mol1->getPosition()).normalize();
	REAL a = dot(v, m_patch1);
	// cout << a << " " << PATCH_SIZE << endl;
	if (a < PATCH_SIZE) return false;

	a = -dot(v, m_patch2);
	// cout << a << " " << PATCH_SIZE << endl;
	if (a < PATCH_SIZE) return false;

	a = dot(m_orth1, m_orth2);
	// cout << a << " " << ROTATE_SIZE << endl;
	if (a < ROTATE_SIZE) return false;

	return true;
}

/******************************************************************/
/******************************************************************//**
 * computeInterfaceVectors: compute patches - vector between proteins
 * and compute orthogonal vectors to the two vectors
 ******************************************************************/
void CBD::computeInterfaceVectors(const char *fname1, const char *fname2) {
	CPnt mean;
	vector<CPnt> pnts;

	//! Normalized vector between 2 mols
	CPnt d = (m_mol2->getPosition() - m_mol1->getPosition()).normalize();
	m_patch1 = d;
	m_patch2 = -d;
	m_orth1 = (CPnt(-d.z(), 0.0, d.x())).normalize();
	       // (cross(m_patch1, m_patch2)).normalize();
	m_orth2 = m_orth1;
}

/******************************************************************/
/******************************************************************//**
 * computeRate
 ******************************************************************/
REAL CBD::computeRate(int numTraj, int nDocked) {
	REAL cnst = 4.0 * M_PI * m_Dtr;
	REAL maxR = q_DIST;
	REAL dR = 10.0;

	REAL k_b = 4 * M_PI * m_Dtr * b_DIST;
	REAL k_q = 4 * M_PI * m_Dtr * q_DIST;
	cout << k_b << " " << k_q << endl;

	REAL delta = ((REAL)nDocked) / numTraj;
	REAL gamma = k_b / k_q;

	REAL K = k_b * delta / (1 - (1 - delta) * gamma);

	cout << "The relative rate is: " << K << endl;
	return K;
}

