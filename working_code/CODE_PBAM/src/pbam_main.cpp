#include "BD.h"
#include "readutil.h"
#include "readinput.h"
#include "setup.h"
#include "tbd.h"
#include "inf.h"
#include "protein.h"

/******************************************************************/
/******************************************************************//**
 * Main for BD simulation of Barnase/Barstar
 * \param a floating point number of salt concentration
 * \param fout an output file
 * \param ind an integer for temporary file index
 ******************************************************************/
int mainSim( CSetup initAll )
{
  const char * prot1PQR = initAll.getTypeNPQR(0).c_str();
  const char * prot2PQR = initAll.getTypeNPQR(1).c_str();

  REAL kappa = initAll.getKappa(); // Inverse debye length
  CBD bd(prot1PQR, prot2PQR, kappa, initAll );
  ofstream fout( initAll.getRunName().c_str());
  
  char temf[100];
  sprintf(temf, "stepstat_%s", initAll.getRunName().c_str() );

  int scount, cdock = 0;
  for (int i = 0; i < 50; i++) 
  {
    CBD::STATUS status = bd.run(scount, temf);
    if (status == CBD::DOCKED) cdock++;
    fout << CBD::STATUS_STRING[status] << " " << (int)status << " "
        << scount << endl;
  }
  fout << cdock << " " << "sim" << endl;
  return 0;
} // end mainSim

/******************************************************************/
/******************************************************************//**
 * Main for computing energies, forces and torques on a replication
 * of a single molecule places equidistantly in a system.
 *   \param ifname an input file name
 *   \param num an integer number of molecules to include in the system.
 *              Values are: 2, 4, 6 or 8
 *   \param dist a floating point distance for molecule placement
 ******************************************************************/
int mainSlv( CSetup initAll )
{
  const char *ifname = initAll.getTypeNPQR(0).c_str();
  int num = initAll.getTypeNCount(0);
  const char *xyzfname = initAll.getTypeNXYZ(0).c_str();

  vector<CMPE *> mpe; vector<CPnt *> cen;
  // building a system of num equidistant mols
  double rad1 = buildSystem_fromXYZ(initAll, ifname, num, true, 
                                    mpe, cen, xyzfname);

  vector<REAL> pot(num); vector<CPnt> force, torque;
  CMPE::initXForms(mpe);
  CMPE::solve(mpe, cen, false);
  CMPE::computeForce(mpe, cen, pot, force, torque);
  printEnForTor( initAll, num, cen, accumulate(pot.begin(), pot.end(), 0.0),
                 pot, force, torque );
  return 0;
} // end mainSlv

/******************************************************************/
/******************************************************************//**
 * Main for perturbation run.
 *   \param ifname a character pointer holding input file name
 *   \param num an integer describing the number of iterations to
 *              run force calculations
 *   \param dist an int describing the distance for molecule placement
 *   \param Dtr a floating point number containing the translational
 *              diffusion coefficient of input molecule
 *   \param Dr a floating point number containing the rotational
 *             diffusion coefficient of input molecule
 *   \param a string containing the perturbation file name
 ******************************************************************/
int mainPer( CSetup initAll )
{
  cout << "PERTURB" << endl;

  const char *ifname = initAll.getTypeNPQR(0).c_str();
  int num = initAll.getTypeNCount(0);
  const char *xyzfname = initAll.getTypeNXYZ(0).c_str();
  REAL Dtr = initAll.getDtr(0);
  REAL Dr  = initAll.getDrot(0);

  vector<CMPE *> mpe; vector<CPnt *> cen;
  double dist = buildSystem_fromXYZ(initAll, ifname, num, false, 
                                       mpe, cen, xyzfname);
  printPositions(cen);

  char fn[100];
  sprintf(fn, "pert_%s_%d_%d.txt", initAll.getRunName().c_str(), num, dist);
  cout << fn << endl;
  ofstream fout(fn);
  CMPE::initXForms(mpe);
  for (int i = 0; i < 200; i++) 
  {
    for (int j = 0; j < num; j++) 
      mpe[j]->reset(12, CQuat::chooseRandom());

    CMPE::solve(mpe, cen, false); CMPE::solve(mpe, cen, false);
    perturb(25, num, fout, Dtr, Dr, dist / 2.0, mpe, cen);
    cout << i << "..";
    cout.flush();
  }
  cout << endl;

  return 0;
} // end mainPer

/******************************************************************/
/******************************************************************//**
 * Main for Computing the polarization forces, used for comparing the
 * effect of mutual polarization on force and torque.  The output is
 * a file named polar_[force/torque]_name_nmol_dist.txt.  The first line
 * indicates the force or torque computed per molecule in the absence of
 * mutual polarization.  The next line includes mutual polarization.
 * These two lines repeat for 1000 iterations.
 *   \param ifname is a character pointer for input file name
 *   \param num is an int of the number of molecules in the system
 *   \param dist is a floating point number indicating the
 *               distance for molecule placement
 *  \param a character string to describe the system
 ******************************************************************/
int mainPol( CSetup initAll )
{
  const char *ifname = initAll.getTypeNPQR(0).c_str();
  int num = initAll.getTypeNCount(0);
  const char *xyzfname = initAll.getTypeNXYZ(0).c_str();

  vector<CMPE *> mpe; vector<CPnt *> cen;
  double rad = buildSystem_fromXYZ(initAll, ifname, num, true, 
                                   mpe, cen, xyzfname);
  printPositions(cen);

  char fn1[100], fn2[100];
  sprintf(fn1, "polar_force_%s_%d_%d.txt", 
               initAll.getRunName().c_str(), num, rad);
  sprintf(fn2, "polar_torque_%s_%d_%d.txt",
               initAll.getRunName().c_str(), num, rad);
  ofstream fout1(fn1);
  ofstream fout2(fn2);

  CMPE::initXForms(mpe);
  for (int i = 0; i < 1000; i++) 
  {
    for (int j = 0; j < num; j++) 
    {
      CQuat Q = CQuat::chooseRandom();
      mpe[j]->reset(12, Q);
    }

    vector<CPnt> force, torque;
    vector<REAL> pot;
    CMPE::updateXForms(cen, mpe); // Compute forces and torques ignoring
    CMPE::reexpand(mpe);          // Mutual polarization effects
    CMPE::computeForce(mpe, cen, pot, force, torque);
    for (int j = 0; j < num; j++) {
      fout1 << mpe[j]->getOrder() << " " << force[j].x() << " "
          << force[j].y() << " " << force[j].z() << " ";
      fout2 << mpe[j]->getOrder() << " " << torque[j].x() << " "
          << torque[j].y() << " " << torque[j].z() << " ";
    }

    fout1 << endl; fout2 << endl;

    CMPE::polarize(mpe, false);
    CMPE::updateXForms(cen, mpe); // Compute forces and torques with
    CMPE::polarize(mpe, false);   // Mutual polarization effects included
    CMPE::computeForce(mpe, cen, pot, force, torque);

    for (int j = 0; j < num; j++) {
      fout1 << mpe[j]->getOrder() << " " << force[j].x() << " "
          << force[j].y() << " " << force[j].z() << " ";
      fout2 << mpe[j]->getOrder() << " " << torque[j].x() << " "
          << torque[j].y() << " " << torque[j].z() << " ";
    }

    fout1 << endl;
    fout2 << endl;
    if (i % 20 == 0) {
      cout << ".." << i;
      cout.flush();
    }
  }

  cout << endl;
  return 0;
} // end mainPol

/******************************************************************/
/******************************************************************//**
 * Main for computing the potential on a grid, given a number of num
 * identical molecules placed dist apart in a salt solution.
 *   \param ifname a character string containing an input file name
 *   \param num an integer number of molecules to introduce into system
 *   \param dist a floating point number for distance for molecule placement
 ******************************************************************/
int mainDif( CSetup initAll )
{
  const char *ifname = initAll.getTypeNPQR(0).c_str();
  int num = initAll.getTypeNCount(0);
  const char *xyzfname = initAll.getTypeNXYZ(0).c_str();

  vector<CMPE *> mpe; vector<CPnt *> cen;
  // building a system of num equidistant mols
  double rad = buildSystem_fromXYZ(initAll, ifname, num, true,
                                    mpe, cen, xyzfname);
  printPositions(cen);

  CMPE::initXForms(mpe);
  if (num > 1)
    CMPE::solve(mpe, cen, false);

  REAL fact = INT_TO_KCALMOL / initAll.getSDiel() * initAll.getIKbT();
  buildGrid(ifname, num, rad, mpe, cen, fact);

  return 0;
} // end mainDif

/******************************************************************/
/******************************************************************//**
 * Main for computing self-polarization and potential grid for a
 * single coarse-grained molecule positioned at (0,0,0).  Similar to
 * the option dif, or main 5, but only for a single molecule and
 *   \param ifname a character string describing the input file name
 *   \param fact a floating point scaling factor for the MPE radius
 ******************************************************************/
int mainRad( CSetup initAll )
{
  cout << "RAD" << endl;

  REAL fact = initAll.getScale();
  int num = initAll.getTypeNCount(0);
  const char *ifname = initAll.getTypeNPQR(0).c_str();

  seedRand(-1);

  REAL rad1;
  vector<CPnt> p1;
  vector<REAL> ch1;
  CPnt cn;
  readmdp(ifname, p1, ch1, rad1, cn);
  for (int i = 0; i < p1.size(); i++) p1[i] -= cn;

  CProtein::initParameters(initAll.getKappa(), initAll.getIDiel(),
      initAll.getSDiel(), 1, rad1);

  vector<CMPE *> mpe;
  vector<CPnt *> cen;

  CMPE *pm = new CMPE(ch1, p1, fact * rad1, 0, 15);
  mpe.push_back(pm);
  cen.push_back(new CPnt(0.0, 0.0, 0.0));

  char ofname[100], ofname_sp[100];
  int l = strlen(ifname);

  strcpy(ofname, ifname);
  strcpy(ofname_sp, ifname);
  sprintf(&(ofname[l - 7]), "_1P_%3.1f.mdp", fact);
  sprintf(&(ofname_sp[l - 7]), "_1P_%3.1f_sp.mdp", fact);

  char rm[200];
  sprintf(rm, "rm -f %s", ofname);
  system(rm);
  sprintf(rm, "rm -f %s", ofname_sp);
  system(rm);

  cout << "saving files:" << endl;
  cout << "\t" << ofname << endl;
  cout << "\t" << ofname_sp << endl;

  // print out a PQR file including the CG sphere
  writemdp(ifname, ofname_sp, p1, *(cen[0]), true, ' ');
  printPositions(cen);

  CMPE::initXForms(mpe);
  buildGrid(ifname, 1, rad1, mpe, cen, 1.0); // Build a potential grid

  return 0;
} // end of mainRad

/******************************************************************/
/******************************************************************//**
 * Main for computing the effect of a third molecule on the interaction
 * free energy of two molecules, as a function of separation distance.
 * The output file generated, [runname]_dist*10.txt has 900 lines, each line
 * representing the interaction free energy between two molecules.
 * The first value in each line represents the system of just two molecules,
 * the next 30 values represents including a 3rd molecule in the system at 30
 * different orientations.
 *   \param ifname a character string describing input file name
 *   \param dist a floating point of the distance between two molecules
 *   \param a string for output for runname
 ******************************************************************/
int mainCng( CSetup initAll )
{
  cout << "CHANGE" << endl;

  REAL dist = initAll.getMolD();
  const char *ifname = initAll.getTypeNPQR(0).c_str();

  REAL rad1; CPnt cn;
  vector<CPnt> p1; vector<REAL> ch1;
 
  readmdp(ifname, p1, ch1, rad1, cn); // read in the PQR file
  for (int i = 0; i < p1.size(); i++) p1[i] -= cn;

  CProtein::initParameters(initAll.getKappa(), initAll.getIDiel(),
				initAll.getSDiel(), 3, rad1);

  vector<CMPE *> mpe; vector<CPnt *> cen;
  REAL fact = rad1 + dist / 2.0; // desired distance between two molecules

  for (int n = 0; n < 3; n++) {
    CMPE *pm = new CMPE(ch1, p1, rad1, n, 12);
    mpe.push_back(pm);
  }

  cen.push_back(new CPnt(fact * CPnt(-1.0, -1.0 / sqrt(3.0), 0.0)));
  cen.push_back(new CPnt(fact * CPnt(1.0, -1.0 / sqrt(3.0), 0.0)));

  CPnt c3;
  cen.push_back(&c3); // create 3 molecules in an equilateral triangle

  CMPE::initXForms(mpe);
  REAL pi, pj;

  char fn[100];
  sprintf(fn, "%s_%dA.txt", initAll.getRunName().c_str(), 
                        (int)floor(1.1 * dist));
  ofstream fout(fn);
  REAL c = 2.0 / sqrt(3.0); int N = 30;
  REAL f[N], t[N];
  // Generate orientations for the spheres to gather data over
  for (int i = 1; i <= N; i++) 
  {
    REAL h = -1.0 + 2.0 * (i - 1.0) / (N - 1.0);
    t[i - 1] = acos(h);
    if (i == 1 or i == N)
      f[i - 1] = 0;
    else
      f[i - 1] = (f[i - 2] + 3.6 / sqrt(N * (1.0 - h * h)));

    while (f[i - 1] > 2 * M_PI) f[i - 1] -= 2 * M_PI;
    while (f[i - 1] < -2 * M_PI) f[i - 1] += 2 * M_PI;
    cout << t[i - 1] * 180 / M_PI << " " << f[i - 1] * 180 / M_PI
        << endl; // print out generated rotations
  }

  for (int i = 0; i < N; i++) {
    CQuat Qt(CPnt(0, 1, 0), t[i]); // for each orientation generated,
                                   // rotate first sphere by it
    CQuat Qf(CPnt(0, 0, 1), f[i]);
    mpe[0]->reset(12, Qf * Qt);

    for (int j = 0; j < N; j++) { // rotate second sphere by it as well
      Qt = CQuat(CPnt(0, 1, 0), t[j]);
      Qf = CQuat(CPnt(0, 0, 1), f[j]);
      mpe[1]->reset(12, Qf * Qt);

      mpe[2]->reset(12, CQuat());
      c3 = CPnt(0.0, 1000, 0.0); // place third sphere very far away so
                                 // as not to factor into computations
      CMPE::solve(mpe, cen, true);
      // compute a pairwise potential
      CMPE::computePairPot(mpe, 0, 1, pi, pj);
      fout << pi << " ";
      c3 = fact * CPnt(0, c, 0); // now place it close and at random
                                 // orientations like the other two
      for (int k = 0; k < N; k++) 
      {
        Qt = CQuat(CPnt(0, 1, 0), t[k]);
        Qf = CQuat(CPnt(0, 0, 1), f[k]);
        mpe[2]->reset(12, Qf * Qt);

        CMPE::solve(mpe, cen, true);
        CMPE::computePairPot(mpe, 0, 1, pi, pj);
        fout << pi << " ";
      }
      fout << endl;
    }
    cout << i << "..";
    cout.flush();
  }

  cout << endl;
  return 0;
} // mainCng

/******************************************************************/
/******************************************************************//**
 * Main for making an infinte grid.  It prints out the potential, the force
 * and the torque for 8 molecules in a lattice with a given number of lattice
 * layers
 *   \param ifname a character string with input file name
 *   \param layer an integer describing the number of neighbors to consider
 *                when performing energy, force and torque calculations
 *   \param stretch a floating point number describing the scaling of
 *                  the CG spheres
 ******************************************************************/
int mainInf( CSetup initAll )
{
  int layer = initAll.getLayer();
  REAL stretch = initAll.getScale();
  const char *ifname = initAll.getTypeNPQR(0).c_str();

  int N = 200; REAL f[N], t[N];
  // Generate orientations for the spheres to gather data over
  for (int i = 1; i <= N; i++) 
  {
    REAL h = -1.0 + 2.0 * (i - 1.0) / (N - 1.0);
    t[i - 1] = acos(h);
    if (i == 1 or i == N)
      f[i - 1] = 0;
    else
      f[i - 1] = (f[i - 2] + 3.6 / sqrt(N * (1.0 - h * h)));
    while (f[i - 1] > 2 * M_PI) f[i - 1] -= 2 * M_PI;
    while (f[i - 1] < -2 * M_PI) f[i - 1] += 2 * M_PI;
  }

  vector<REAL> fs, ts;
  // Save only orientations that are less than 180 deg
  for (int i = 0; i < N; i++) 
    if ((t[i] <= M_PI / 2.0) && (f[i] <= M_PI / 2.0)) 
    {
      fs.push_back(f[i]);
      ts.push_back(t[i]);
    }

  cout << fs.size() << " orientations" << endl; // printing them out

  char fon[100];
  strcpy(fon, ifname);
  // opening file to write out grid to
  sprintf(&(fon[strlen(fon) - 4]), "_88.pdb");

  vector<CMPE *> mpe; vector<CPnt *> cen;
  int m = 2 * layer + 1;
  CProtein::initParameters(initAll.getKappa(), initAll.getIDiel(),
       initAll.getSDiel(), m * m * m * 8, 26.0); // nmol=m^3*8

  buildUnitCell(ifname, fon, mpe, cen, true, stretch);
  REAL lx = stretch * 79.1, ly = stretch * 79.1, lz = stretch * 37.9;

  // make a grid of layer layers and place points at each location
  for (int i = -layer; i <= layer; i++) 
    for (int j = -layer; j <= layer; j++) 
      for (int k = -layer; k <= layer; k++) 
      {
        CPnt c(lx * i, ly * j, lz * k);
        for (int n = 0; n < 8; n++) 
        {
          if (i == 0 && j == 0 && k == 0) continue;
          cen.push_back(new CPnt(c + *(cen[n])));
          mpe.push_back(mpe[n]);
        }
      }

  int tot = cen.size();
  cout << "N_MOL = " << tot << endl;
  CMPE::m_bInfinite = true; CMPE::m_unit = 8;

  REAL k = INT_TO_KCALMOL / initAll.getSDiel() * initAll.getIKbT();
  vector<CPnt> force, torque; vector<REAL> pot;
  CMPE::initXForms(mpe);
  CMPE::solve(mpe, cen, false);
  CMPE::computeForce(mpe, cen, pot, force, torque);
  for (int n = 0; n < 8; n++)
    cout << k *pot[n] << " " << k *force[n] << " " << k *torque[n] << endl;

  return 0;
} // end mainInf

/******************************************************************/
/******************************************************************//**
 * Main for computing 3body energies, forces and torques on a replication
 * of a single molecule places equidistantly in a system.
 *   \param ifname an input pqr file name
 *   \param num an integer number of molecules to include in the system.
 *   \param xyzf a file name for xyz coords of each molecule
 ******************************************************************/
int main3bd( CSetup initAll )
{
  
  const char *ifname = initAll.getTypeNPQR(0).c_str();
  int num = initAll.getTypeNCount(0);
  const char *xyzfname = initAll.getTypeNXYZ(0).c_str();
  // The number of bodies in approx, for now set at 3
  int kbody = 3; double potTot = 0.0;
  
  REAL radius; vector<CMPE *> mpe; vector<CPnt *> cen;
  vector<REAL> pot(num); vector<CPnt> force(num), torque(num);
  
  if ( num < kbody ) 
  {
    // building a system of num equidistant mols
    radius = buildSystem_fromXYZ(initAll, ifname, num, true, 
                                  mpe, cen, xyzfname);
    CMPE::initXForms(mpe);
    CMPE::solve(mpe, cen, false);
    CMPE::computeForce(mpe, cen, pot, force, torque);
  }else 
    runThreeBod( initAll,ifname,xyzfname,num,cen,potTot,pot,force,torque);

  printEnForTor( initAll, num, cen, potTot, pot, force, torque );
  return 0;
} // end main3bd


/******************************************************************//**
 * Real Main!
   Checks for input file, reads it in and calls appropriate program
 ******************************************************************/
int pbam_main(int argc, char **argv) 
{
  cout.precision(5);
  argCheck( argc );
  CSetup initAll;
  //initAll.printSetupClass( );
  readInputFile(argv[1], initAll);
  initAll.printSetupClass( );

  string runType = initAll.getRunType();

  if ((runType != "sim") && (initAll.getType() > 1))
    cout << "Only sim equipped to use many mols" << endl;

  // For running 2 molecule BD simulation
  if (runType == "sim")
    return mainSim( initAll );

  // For computing energies, torques and forces of many of the same
  // molecules in solution
  else if (runType == "slv")
    return mainSlv( initAll );

  // For computing the energy of many molecules in solution as their
  // rotations and locations are perturbed
  else if (runType == "per")
    return mainPer( initAll );

  // For computing the forces/torques of many molecules  in solution
  // with and without mutual polarization
  else if (runType == "pol")
    return mainPol( initAll );

  // For computing a grid of potentials due to many molecules fixed
  // in solution
  else if (runType == "dif")
    return mainDif( initAll );

  // For computing a potential grid for a single molecule in solution
  else if (runType == "rad")
    return mainRad( initAll );

  // For computing the effect of a 3rd molecule of the total free energy
  // of a system at distance dist (in A)
  else if (runType == "cng")
    return mainCng( initAll );

  else if (runType == "inf")
    return mainInf( initAll );
 
  // For running a three body approximation
  else if (runType == "3bd")
    return main3bd( initAll );

  // else, bad option
  else 
  {
    cout << "bad option!!! " << runType << endl;
    return 1;
  }
} //end pbam_main

/******************************************************************//**
 * Main!
 ******************************************************************/
int main(int argc, char **argv) 
{
  return pbam_main( argc, argv );
}// end main
