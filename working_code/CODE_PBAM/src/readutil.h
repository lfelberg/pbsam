#ifndef _READUTIL_H_
#define _READUTIL_H_

#include <vector>
#include <fstream>
#include "mpe.h"
#include "main_util.h"
#include "setup.h"
#include "util.h"

/******************************************************************/
/******************************************************************//**
 * Initializing constants
 ******************************************************************/
/*#define b_DIST 100.0 //!< Initial distance between 2 proteins for BD run
#define q_DIST 500.0 //!< Distance for molecules to be considered escaped
#define f_DIST 100.0 //!< Cutoff for protein force interactions

#define DIELECTRIC_WATER 78.0 //!< The dielectric constant of water
#define DIELECTRIC_PROT 4.0   //!< Dielectric constant of the protein

#define SALT_CONCENTRATION 0.0100               //!< [ Molar ]
#define LITRE 1e-3                // [ m^3/L]
#define PI 3.141592654

#define COULOMB_CONSTANT (8.988e9)       //!< [ N*m^2/C^2 ]
#define ELECTRON_CHARGE (1.60217733e-19) //!<  [ coulombs ]
#define E2 (ELECTRON_CHARGE*ELECTRON_CHARGE) //[C2]
#define AVOGADRO_NUM (6.02209e23)
#define KCAL 4184.0      //!<  [ 1 kCal = 4184 Joules ]
#define ANGSTROM (1e-10) //!<  [ 1A = 1e-10 Meters ]
#define PICO_SEC (1e-12) //!<  [ 1 ps = 1e-12 s ]
#define PERMITTIVITY_VAC 8.854187817e-12  // [F/m]=[C2/J/m] , F = Farad
#define COUL_NUM (E2*AVOGADRO_NUM)
#define COUL_DEN (PERMITTIVITY_VAC*4.0*PI*ANGSTROM*KCAL)
#define INT_TO_KCALMOL (COUL_NUM/COUL_DEN) // Convert from int. units to kCal/mole
									// [C2/elec2][#/moles][mJ/C2][m/A][J/kCal] = [kCal/mole]/[elec2/A]

#define INTJ_DEN (PERMITTIVITY_VAC*4.0*PI*ANGSTROM)
#define INT_TO_JMOL (COUL_NUM/INTJ_DEN) // Convert from int. units to J/mole


#define Kb (1.380658e-23)        //!<  [ m^2 kg/ s^2 / K ] = [ J/K ]
#define TEMP 353.0               //!<  [ Kelvin ]
#define KbT (Kb*AVOGADRO_NUM*TEMP/KCAL)//!< [kCal/mol] = [J/K]*[#/mol]*[K]*[kCal/J]
#define IKbT (1.0 / KbT)         //!<  1/KbT = [kCal/mol]-1
#define KAP_NUM sqrt((2*SALT_CONCENTRATION*AVOGADRO_NUM*E2)) //sqrt([mol/L]*[#/mol]*[C2])
#define KAP_DEN sqrt(LITRE*DIELECTRIC_WATER*PERMITTIVITY_VAC*Kb*TEMP)//sqrt([m3/L]*[C2/J/m][J/K][K])
#define KAPPA (ANGSTROM * KAP_NUM / KAP_DEN ) //!< Inverse debye length [1/A]

#define TOL 2.5

#define PATCH_ANGLE 6.0
#define ROTATE_ANGLE 20.0
#define PATCH_SIZE (cos(PATCH_ANGLE * M_PI / 180.0))
#define ROTATE_SIZE (cos(ROTATE_ANGLE * M_PI / 180.0))
*/
/******************************************************************/
/******************************************************************/

void readqcd(const char *fname, vector<CPnt> &pnt,
        vector<REAL> &ch, REAL &rad);

void writemdp(const char *ifname, const char *ofname, const vector<CPnt> &pnt,
        const CPnt &cen, bool sp, char chid);

void readmdp(const char *fname, vector<CPnt> &pnt, vector<REAL> &ch,
        REAL &rad, CPnt &cen);

void readxyz(const char *fname, vector<CPnt> &pos);

REAL buildSystem_fromXYZ( CSetup initAll, const char *ifname, int num, 
                          bool bSave, vector<CMPE *> &mpe, 
                          vector<CPnt *> &cen, const char *xyzfname);

REAL getCoords(const char *ifname, int num, bool bSave, 
        vector<REAL > &chgs, vector<CPnt > &chgpos,
        vector<CPnt *> &cen, const char *xyzfname);

#endif

