#include "BD.h"
#include "readutil.h"
#include "inf.h"
#include "protein.h"
#include "mcoeff.h"

/******************************************************************/
/******************************************************************//**
 * argCheckinf
 * \param argCount an int of the number of arg inputs
*******************************************************************/
void argCheckInf(int argCount)
{
  if (argCount != 5) 
  {
    cout << "Correct input format: " << endl;
    cout << " ./exec inf [PQR file] [# layers] [stretch factor]" << endl;
    exit(0);
  }
  cout << "INFINITE GRID" << endl;
}

/******************************************************************/
/******************************************************************//**
 * findLargestBounds
 * \param V a vector of xyz coordinates for charges
 * \param Pmax a vector of indices of largest XYZ points
 * \param Pmin a vector of indices of smallest XYZ points
 * \param max a vector of values of largest XYZ points
 * \param min a vector of values of smallest XYZ points
 ******************************************************************/
void findLargestBounds(const vector<CPnt> &V, vector<int> &Pmax,
         vector<int> &Pmin)
{
  // find a large diameter to start with.
  // first get the bounding box and V[] extreme points for it
  double max[] = {V[0].x(),V[0].y(),V[0].z()}; 
  double min[] = {V[0].x(),V[0].y(),V[0].z()};
  vector <double> V_i(3); int n = V.size();

  for (int i = 1; i < n; i++) 
  {
    V_i[0] = V[i].x(); V_i[1] = V[i].y(); V_i[2] = V[i].z();
    for (int j = 0; j<3; j++)
		{
      if (V_i[j] < min[j]) 
      {
        min[j] = V_i[j];
        Pmin[j] = i;
      } else if (V_i[j] > max[j]) 
      {
        max[j] = V_i[j];
        Pmax[j] = i;
      }
		}
  }
} // end findLargestBounds


/******************************************************************/
/******************************************************************//**
 * initializeBallDiam
 * \param V a vector of xyz coordinates for charges
 * \param 
 * \param rad a floating point of the radius of the CG sphere
 ******************************************************************/
double initializeBallDiam(const vector<CPnt> &V, vector <int> Pmax,
         vector<int> Pmin, CPnt &cen)
{
  // select the largest extent as an initial diameter for the ball
  vector <CPnt> dV(3); vector <REAL> d2(3,0.0); REAL rad2;
  for (int j = 0; j<3; j++)
  {
    dV[j] = V[Pmax[j]]-V[Pmin[j]];
    d2[j] = dV[j].normsq(); // diffs squared
  }

  int dmax;// Find d2 with largest val
  if (d2[0] >= d2[1] && d2[0] >= d2[2])  dmax = 0; // x dir is largest
  else if (d2[1] >= d2[2])               dmax = 1; // y dir is largest
  else                                   dmax = 2; // z dir is largest
    
  cen = V[Pmin[dmax]] + (0.5 * dV[dmax]);     // Center = midpoint of extremes
  rad2 = (V[Pmax[dmax]] - cen).normsq(); // radius squared
  return rad2;
} // end initializeBallDiam

/******************************************************************/
/******************************************************************//**
 * allPointsInBall
 * \param V a vector of xyz coordinates for charges
 * \param cent an xyz coord for the CG sphere center
 * \param rad a floating point of the radius of the CG sphere
 ******************************************************************/
void allPointsInBall(const vector<CPnt> &V, CPnt &cen, REAL &rad, REAL rad2) 
{
  // now check that all points V[i] are in the ball
  // and if not, expand the ball just enough to include them
  CPnt dV; REAL dist, dist2; int n = V.size();
  for (int i = 0; i < n; i++) 
  {
    dV = V[i] - cen;
    dist2 = dV.normsq();
    if (dist2 <= rad2) // V[i] is inside the ball already
      continue;

    // V[i] not in ball, so expand ball to include it
    dist = sqrt(dist2);
    rad = (rad + dist) / 2.0; // enlarge radius just enough
    rad2 = rad * rad;
    cen = cen + ((dist - rad) / dist) * dV; // shift Center toward V[i]
  }
}

/******************************************************************/
/******************************************************************//**
 * approxBBall function to approximate.
 * \param V a vector of xyz coordinates for charges
 * \param cen an xyz coord for the CG sphere center
 * \param rad a floating point of the radius of the CG sphere
 ******************************************************************/
void approxBBall(const vector<CPnt> &V, CPnt &cen, REAL &rad) 
{
  // index of V[] at box extreme
  vector <int> Pmax(3,0); vector <int> Pmin(3,0);
  findLargestBounds( V, Pmax, Pmin );
  
  REAL rad2 = initializeBallDiam(V, Pmax, Pmin, cen);
  rad = sqrt(rad2);

  allPointsInBall(V, cen, rad, rad2);
  cout << "Ball: " << cen << " " << rad << endl;
  return;
} // end approxBBall

/******************************************************************/
/******************************************************************/ /**
 * buildUnitCell, function used to build a unit cell for use in
 * infinite grid calculation.
 *   \param ifname a character string describing the input file name
 *   \param ofname a character string describing the desired output file name
 *   \param mpe a vector of multipole expansions
 *   \param cen a vector of sphere centers
 *   \params bSave a boolean operator indicating whether or not to write
 *                 out grid
 *   \params stretch a floating point of how much to stretch the center by ??
 ******************************************************************/
void buildUnitCell(const char *ifname, const char *ofname, vector<CMPE *> &mpe,
    vector<CPnt *> &cen, bool bSave, REAL stretch) 
{
  REAL rad; CPnt cn, cn2; vector<CPnt> V; vector<REAL> ch;
  
  if (bSave) 
  {
    ofstream fout(ofname);
    fout.close();
  }
  readmdp(ifname, V, ch, rad, cn); // read in the molecule from PQR file,
                                   // save atom point xyzs in V
  approxBBall(V, cn, rad); // run through all the point charges and build a
                           // sphere large enough to enclose all of them.  it
                           // has a radius of rad

  REAL lx = 79.1, ly = 79.1, lz = 37.9;
  REAL sx = 0.012642, sy = 0.012642, sz = 0.026385;
  CPnt t;

  for (int i = 0; i < V.size(); i++) // for all point charges, move their
                                     // position by a scaling factor of s
    V[i] = CPnt(V[i].x() * sx, V[i].y() * sy, V[i].z() * sz);
  cn = CPnt(cn.x() * sx, cn.y() * sy, cn.z() * sz); // and scale the center
                                                    // by that as well
  vector<CPnt> U(V.size());

  // create a grid of 8 points and write out each one
  // ASU #1
  t = CPnt(0.0, 0.0, 0.0);
  cn2 = CPnt(cn.x() * lx, cn.y() * ly, cn.z() * lz) + t;
  for (int i = 0; i < V.size(); i++)
    U[i] = CPnt(V[i].x() * lx, V[i].y() * ly, V[i].z() * lz) + (t - cn2);
  if (bSave) writemdp(ifname, ofname, U, cn2, false, 'A');
  mpe.push_back(new CMPE(ch, U, rad, 0, 15));
  cen.push_back(new CPnt(stretch * cn2));

  // ASU #2
  t = CPnt(lx, ly, 0.5 * lz);
  cn2 = CPnt(-cn.x() * lx, -cn.y() * ly, cn.z() * lz) + t;
  for (int i = 0; i < V.size(); i++)
    U[i] = CPnt(-V[i].x() * lx, -V[i].y() * ly, V[i].z() * lz) + (t - cn2);
  if (bSave) writemdp(ifname, ofname, U, cn2, false, 'B');
  mpe.push_back(new CMPE(ch, U, rad, 1, 15));
  cen.push_back(new CPnt(stretch * cn2));

  // ASU #3
  t = CPnt(0.5 * lx, 0.5 * ly, -0.25 * lz);
  cn2 = CPnt(-cn.y() * lx, cn.x() * ly, cn.z() * lz) + t;
  for (int i = 0; i < V.size(); i++)
    U[i] = CPnt(-V[i].y() * lx, V[i].x() * ly, V[i].z() * lz) + (t - cn2);
  if (bSave) writemdp(ifname, ofname, U, cn2, false, 'C');
  mpe.push_back(new CMPE(ch, U, rad, 2, 15));
  cen.push_back(new CPnt(stretch * cn2));

  // ASU #4
  t = CPnt(0.5 * lx, 0.5 * ly, 0.25 * lz);
  cn2 = CPnt(cn.y() * lx, -cn.x() * ly, cn.z() * lz) + t;
  for (int i = 0; i < V.size(); i++)
    U[i] = CPnt(V[i].y() * lx, -V[i].x() * ly, V[i].z() * lz) + (t - cn2);
  if (bSave) writemdp(ifname, ofname, U, cn2, false, 'D');
  mpe.push_back(new CMPE(ch, U, rad, 3, 15));
  cen.push_back(new CPnt(stretch * cn2));

  // ASU #5
  t = CPnt(0.5 * lx, 0.5 * ly, 0.75 * lz);
  cn2 = CPnt(-cn.x() * lx, cn.y() * ly, -cn.z() * lz) + t;
  for (int i = 0; i < V.size(); i++)
    U[i] = CPnt(-V[i].x() * lx, V[i].y() * ly, -V[i].z() * lz) + (t - cn2);
  if (bSave) writemdp(ifname, ofname, U, cn2, false, 'E');
  mpe.push_back(new CMPE(ch, U, rad, 4, 15));
  cen.push_back(new CPnt(stretch * cn2));

  // ASU #6
  t = CPnt(0.5 * lx, 0.5 * ly, 1.25 * lz);
  cn2 = CPnt(cn.x() * lx, -cn.y() * ly, -cn.z() * lz) + t;
  for (int i = 0; i < V.size(); i++)
    U[i] = CPnt(V[i].x() * lx, -V[i].y() * ly, -V[i].z() * lz) + (t - cn2);
  if (bSave) writemdp(ifname, ofname, U, cn2, false, 'F');
  mpe.push_back(new CMPE(ch, U, rad, 5, 15));
  cen.push_back(new CPnt(stretch * cn2));

  // ASU #7
  t = CPnt(0.0, 0.0, lz);
  cn2 = CPnt(cn.y() * lx, cn.x() * ly, -cn.z() * lz) + t;
  for (int i = 0; i < V.size(); i++)
    U[i] = CPnt(V[i].y() * lx, V[i].x() * ly, -V[i].z() * lz) + (t - cn2);
  if (bSave) writemdp(ifname, ofname, U, cn2, false, 'G');
  mpe.push_back(new CMPE(ch, U, rad, 6, 15));
  cen.push_back(new CPnt(stretch * cn2));

  // ASU #8
  t = CPnt(lx, ly, 0.5 * lz);
  cn2 = CPnt(-cn.y() * lx, -cn.x() * ly, -cn.z() * lz) + t;
  for (int i = 0; i < V.size(); i++)
    U[i] = CPnt(-V[i].y() * lx, -V[i].x() * ly, -V[i].z() * lz)
        + (t - cn2);
  if (bSave) writemdp(ifname, ofname, U, cn2, false, 'H');
  mpe.push_back(new CMPE(ch, U, rad, 7, 15));
  cen.push_back(new CPnt(stretch * cn2));
} // end buildUnitCell

/******************************************************************/
/******************************************************************//**
 * Main for making an infinte grid.  It prints out the potential, the force
 * and the torque for 8 molecules in a lattice with a given number of lattice
 * layers
 *   \param ifname a character string with input file name
 *   \param layer an integer describing the number of neighbors to consider
 *                when performing energy, force and torque calculations
 *   \param stretch a floating point number describing the scaling of
 *                  the CG spheres
 ******************************************************************/
/*int mainInf(int argc, char **argv) 
{
  if (argc != 5) 
  {
    cout << "Correct input format: " << endl;
    cout << " ./exec inf [PQR file] [# layers] [stretch factor]" << endl;
    exit(0);
  }
  cout.precision(5); seedRand(-1);
  cout << "INFINITE GRID" << endl;

  const char *ifname = argv[2];
  int layer = atoi(argv[3]);
  REAL stretch = atof(argv[4]);

  int N = 200; REAL f[N], t[N];
  // Generate orientations for the spheres to gather data over
  for (int i = 1; i <= N; i++) 
  {
    REAL h = -1.0 + 2.0 * (i - 1.0) / (N - 1.0);
    t[i - 1] = acos(h);
    if (i == 1 or i == N)
      f[i - 1] = 0;
    else
      f[i - 1] = (f[i - 2] + 3.6 / sqrt(N * (1.0 - h * h)));
    while (f[i - 1] > 2 * M_PI) f[i - 1] -= 2 * M_PI;
    while (f[i - 1] < -2 * M_PI) f[i - 1] += 2 * M_PI;
  }

  vector<REAL> fs, ts;
  // Save only orientations that are less than 180 deg
  for (int i = 0; i < N; i++) 
    if ((t[i] <= M_PI / 2.0) && (f[i] <= M_PI / 2.0)) 
    {
      fs.push_back(f[i]);
      ts.push_back(t[i]);
    }

  cout << fs.size() << " orientations" << endl; // printing them out

  char fon[100];
  strcpy(fon, ifname);
  // opening file to write out grid to
  sprintf(&(fon[strlen(fon) - 4]), "_88.pdb");

  vector<CMPE *> mpe; vector<CPnt *> cen;
  int m = 2 * layer + 1;
  //CProtein::initParameters(KAPPA, DIELECTRIC_PROT, DIELECTRIC_WATER,
      m * m * m * 8, 26.0); // nmol=m^3*8

  buildUnitCell(ifname, fon, mpe, cen, true, stretch);
  REAL lx = stretch * 79.1, ly = stretch * 79.1, lz = stretch * 37.9;

  // make a grid of layer layers and place points at each location
  for (int i = -layer; i <= layer; i++) 
    for (int j = -layer; j <= layer; j++) 
      for (int k = -layer; k <= layer; k++) 
      {
        CPnt c(lx * i, ly * j, lz * k);
        for (int n = 0; n < 8; n++) 
        {
          if (i == 0 && j == 0 && k == 0) continue;
          cen.push_back(new CPnt(c + *(cen[n])));
          mpe.push_back(mpe[n]);
        }
      }

  int tot = cen.size();
  cout << "N_MOL = " << tot << endl;
  CMPE::m_bInfinite = true; CMPE::m_unit = 8;

  CMPE::initXForms(mpe);
  REAL k = INT_TO_KCALMOL / DIELECTRIC_WATER * IKbT;
  vector<CPnt> force, torque; vector<REAL> pot;
  CMPE::solve(mpe, cen, false);
  CMPE::computeForce(mpe, cen, pot, force, torque);
  for (int n = 0; n < 8; n++)
    cout << k *pot[n] << " " << k *force[n] << " " << k *torque[n] << endl;

  return 0;
} // end mainInf
*/
