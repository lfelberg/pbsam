#include "readutil.h"


/******************************************************************/
/******************************************************************//**
 * readqcd
 ******************************************************************/
void readqcd(const char *fname, vector<CPnt> &pnt,
    vector<REAL> &ch, REAL &rad) 
{
  ifstream fin(fname);
  char buf[100], temp;
  REAL sum = 0.0;

  pnt.clear();
  ch.clear();
  bool first = true;
  fin.getline(buf, 99);
  while (!fin.eof()) {
    double x, y, z, c, r;
    sscanf(buf, "ATOM\t1\tLTL\tTST\t%lf %lf %lf %lf %lf",
        &x, &y, &z, &c, &r);

    if (first) {
      rad = r;
      first = false;
    } else {
      pnt.push_back(CPnt(x, y, z));
      ch.push_back(c);
      sum += c;
    }
    fin.getline(buf, 99);
  }

  cout << fname << ": charges: " << ch.size() << ", net charge: " << sum
      << ", radius: " << rad << endl;
}

/******************************************************************/
/******************************************************************//**
 * writemdp: a function to write out a PQR file type.
 * \param ifname a character string that contains the input file name
 * \param ofname a character name that contains the output file name
 * \param pnt a vector of charge positions
 * \param cen a position of the CG sphere center
 * \param sp a boolean that tells function whether to print out the CG
 *           sphere in the PQR file or not
 * \param chid a character of the chain ID of the molecule
 ******************************************************************/
void writemdp(const char *ifname, const char *ofname, const vector<CPnt> &pnt,
		const CPnt &cen, bool sp, char chid) 
{
	ifstream fin(ifname);
	ofstream fout(ofname, ios::app);
	if (!fin.is_open()) {
		cout << "Could not open file " << ifname << endl;
		exit(0);
	}

	if (!fout.is_open()) {
		cout << "Could not open file " << ofname << endl;
		exit(0);
	}

	char buf[100], temp[30];
	fin.getline(buf, 99);
	int i = 0;
	while (!fin.eof()) {
		if (strncmp(&(buf[0]), "ATOM", 4) == 0) {
			buf[21] = chid;
			if (strncmp(&(buf[17]), "DUM", 3) == 0) {
				if (sp) {
					CPnt P = cen;
					sprintf(temp, "%8.3f%8.3f%8.3f", P.x(), P.y(), P.z());
					strncpy(&(buf[30]), temp, 24);
					fout << buf << endl;
				}
			} else {
				CPnt P = pnt[i] + cen;
				sprintf(temp, "%8.3f%8.3f%8.3f", P.x(), P.y(), P.z());
				strncpy(&(buf[30]), temp, 24);
				fout << buf << endl;
				i++;
			}
		}
		fin.getline(buf, 99);
	}
} // end writemdp

/******************************************************************/
/******************************************************************/ /**
 * readmdp: function that reads in an MDP file for a given molecule.
 * The file should contain the PQR data of a molecule and a dummy molecule
 * that approximates the CG version of the molecule.
 *   \param fname a character pointer to the filename
 *   \param pnt a vector of xyz coordinates for the charges within the molecule
 *   \param ch a vector of floating point numbers for the charges
 *   \param rad a floating point number that stores the radius of the molecule
 *   \param cen a vector of xyz coordinates for the molecule's center of geom
 ******************************************************************/
void readmdp(const char *fname, vector<CPnt> &pnt, vector<REAL> &ch,
		REAL &rad, CPnt &cen) {
	ifstream fin(fname);
	if (!fin.is_open()) { // Check for file
		cout << "Could not open file " << fname << endl;
		exit(0);
	}

	char buf[100], temp[10];
	REAL sum = 0.0; // Total charge of the molecule
	pnt.clear();
	ch.clear();
	fin.getline(buf, 99);

	while (!fin.eof()) {
		double x, y, z, c, r;
		if (strncmp(&(buf[0]), "ATOM", 4) == 0) {
			sscanf(&(buf[31]), "%lf %lf %lf", &x, &y, &z);
			strncpy(temp, &(buf[63]), 6);
			temp[6] = 0;
			if (strcmp(temp, "      ") == 0)
				r = 0.0;
			else
				sscanf(temp, "%lf", &r);
			sscanf(&(buf[55]), "%lf", &c);

			if (strncmp(&(buf[17]), "DUM", 3) == 0) {
				rad = r;
				cen = CPnt(x, y, z);
			} else // if (c != 0.0)
			{
				pnt.push_back(CPnt(x, y, z));
				ch.push_back(c);
				sum += c;
			}
		}
		fin.getline(buf, 99);
	}
	cout << fname << ": charges: " << ch.size() << ", net charge: " << sum
			<< ", radius: " << rad << ", cen: " << cen << endl;
} // end readmdp

/******************************************************************/
/******************************************************************/ /**
 * readxyz: function that reads in an XYZ file for positions of molecules
 *   \param fname a character pointer to the filename
 *   \param pos a vector of xyz coordinates for the molecule's positions
 ******************************************************************/
void readxyz(const char *fname, vector<CPnt> &pos) {
	ifstream fin(fname);
	if (!fin.is_open()) { // Check for file
		cout << "Could not open file " << fname << endl;
		exit(0);
	}

	char buf[100];
	pos.clear();
	fin.getline(buf, 99);

	while (!fin.eof()) {
		double x, y, z;
		sscanf(&(buf[0]), "%lf %lf %lf", &x, &y, &z);
		pos.push_back(CPnt(x, y, z));

		fin.getline(buf, 99);
	}
} //end readxyz

/******************************************************************/
/******************************************************************//**
 * buildSystem_fromXYZ: a function that builds a system of num molecules
 *              separated equidistantly in space
 *   \param ifname a character pointer that has the name of an
 *                 file of type MDP
 *   \param num an int that details the number of centers to
 *              include for a system of multiple molecules
 *   \param bSave a boolean that indicates whether or not the user
 *                desires to save the configuration
 *   \param mpe a vector of multipole expansion objects for use
 *              in mutual polarization
 *   \param cen a vector of positions to hold the position of each
 *              molecule in the system
 *   \param xyzfname a character pointer that has the name of an
 *                 file of type XYZ
 *   \return a floating point number that returns the radius of
 *           the molecule input into the system
 ******************************************************************/
REAL buildSystem_fromXYZ( CSetup initAll, const char *ifname, int num, 
                          bool bSave, vector<CMPE *> &mpe, 
                          vector<CPnt *> &cen, const char *xyzfname) 
{
  REAL rad1;        // size of the molecule
  vector<CPnt> p1;  // vector of positions of point charges in the molecule
  vector<CPnt> pos1;  // vector of positions of centers of mass of each molecule
  vector<REAL> ch1; // vector of point charges in the molecule
  CPnt cn; // original position of center of geometry of molecule in space
 
  readmdp(ifname, p1, ch1, rad1, cn);
  readxyz(xyzfname, pos1);
 
  for (int i=0;i<num;i++){
    cen.push_back(new CPnt(pos1[i]));
  }
 
  REAL dist = (pos1[0]-pos1[1]).norm();
  REAL fact = rad1 + dist / 2.0;
 
  for (int i = 0; i < p1.size(); i++){ // moving the positions of each charge
    p1[i] -= cn;                      // to be WRT the center of the mol
  }
 
  char ofname[100], ofname_sp[100];
	if (bSave) // print out molecules positions if desired
	{
		int l = strlen(ifname);

		strcpy(ofname_sp, ifname);
		sprintf(&(ofname_sp[l - 7]), "_%dP_sp.mdp", num);

		char rm[200];
		sprintf(rm, "rm -f %s", ofname_sp);
		system(rm);

		cout << "saving files:" << endl;
		cout << "\t" << ofname_sp << endl;
	}

	initParm( initAll, num, rad1 );
	for (int n = 0; n < num; n++) 
  {
		CQuat Q = CQuat(); //(num > 2 ? CQuat::chooseRandom() : CQuat());
		vector<CPnt> pr(p1.size());
		for (int i = 0; i < p1.size(); i++) {
			pr[i] = Q * p1[i]; // Rotate mol i by random orientation
			if (pr[i].norm() > rad1) cout << "ERROR" << endl;
		}

		// Make MPE for molecule i with 15 poles
		CMPE *pm = new CMPE(ch1, pr, rad1, n, 15);
		mpe.push_back(pm);
		if (bSave) { // If we wish to save configurations
		             // Write out a file with the CG sphere
			writemdp(ifname, ofname_sp, pr, *(cen[n]), true, ' ');
		}
	}

	cout << "Building system is complete" << endl;
	return rad1;
} // end buildSystem_fromXYZ

/******************************************************************/
/******************************************************************//**
 * getCoords: a function that builds a system of num molecules
 *              at positions given by XYZ file
 *   \param ifname a character pointer that has the name of an
 *                 file of type MDP
 *   \param num an int that details the number of centers to
 *              include for a system of multiple molecules
 *   \param bSave a boolean that indicates whether or not the user
 *                desires to save the configuration
 *   \param mpe a vector of multipole expansion objects for use
 *              in mutual polarization
 *   \param cen a vector of positions to hold the position of each
 *              molecule in the system
 *   \param xyzfname a character pointer that has the name of an
 *                 file of xyz coords for all spheres
 *   \return a floating point number that returns the radius of
 *           the molecule input into the system
 ******************************************************************/
REAL getCoords(const char *ifname, int num, bool bSave, vector<REAL > &chgs, 
		vector<CPnt > &chgpos,
		vector<CPnt *> &cen, const char *xyzfname) {
	REAL rad1;        // size of the molecule
	vector<CPnt> pos1;  // vector of positions of com of each molecule
	CPnt cn; // original position of center of geometry of molecule in space

	readmdp(ifname, chgpos, chgs, rad1, cn);
	readxyz(xyzfname, pos1);

	for (int i=0;i<num;i++){
		cen.push_back(new CPnt(pos1[i]));
	}

	CPnt distpnt = pos1[0]-pos1[1];
	REAL dist = distpnt.norm();
	REAL fact = rad1 + dist / 2.0;

	for (int i = 0; i < chgpos.size(); i++){ // moving positions of each charge
	  chgpos[i] -= cn;      // to be WRT the center of the mol
	}

	char ofname[100], ofname_sp[100];
	if (bSave) // print out molecules positions if desired
	{
		int l = strlen(ifname);
		strcpy(ofname_sp, ifname);
		sprintf(&(ofname_sp[l - 4]), "_%dP_sp.mdp", num);

		char rm[200];
		sprintf(rm, "rm -f %s", ofname_sp);
		system(rm);
		cout << "saving file:" << "\t" << ofname_sp << endl;
	}
	cout << "Building system is complete" << endl;
	return rad1;
} // end getCoords

