#include "BD.h"
#include "readutil.h"
#include "protein.h"
#include "mcoeff.h"

void argCheckInf(int argCount);

void findLargestBounds(const vector<CPnt> &V, vector<int> &Pmax,
         vector<int> &Pmin);

double initializeBallDiam(const vector<CPnt> &V, vector <int> Pmax,
         vector<int> Pmin, CPnt &cen);

void allPointsInBall(const vector<CPnt> &V, CPnt &cen, REAL &rad, REAL rad2);

void approxBBall(const vector<CPnt> &V, CPnt &cen, REAL &rad);

void buildUnitCell(const char *ifname, const char *ofname, vector<CMPE *> &mpe,
    vector<CPnt *> &cen, bool bSave, REAL stretch);
