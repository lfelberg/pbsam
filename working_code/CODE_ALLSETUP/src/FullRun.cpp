//
//  FullRun.cpp
//  all
//
//  Created by David Brookes on 8/27/15.
//  Copyright (c) 2015 David Brookes. All rights reserved.
//

#include "FullRun.h"

/*#########################################################*/
// Initializing the fullrun class
// With inputs and directory locs
/*#########################################################*/
FullRun::FullRun(string homedir, string msms_name, string tempdir, double pH)
:HOMEDIR_(homedir), msmsName_(msms_name), pH_(pH), TEMPDIR_(tempdir)
{
  /*if (*HOMEDIR_.rbegin() != '/') HOMEDIR_ += "/";
  if (*TEMPDIR_.rbegin() != '/') HOMEDIR_ += "/"; // Not working LF */
  
  MSMS_ = HOMEDIR_ + "/packages/msms/" + msmsName_;
  PDB2PQR_ = HOMEDIR_ + "/packages/pdb2pqr.2.0/pdb2pqr.py";
  SCRIPTSDIR_ = HOMEDIR_ + "/scripts/";
} // end FullRun::FullRun

/*#########################################################*/
//  run_pdb2pqr
// use APBS software PDB2PQR for conversion
/*#########################################################*/
string FullRun::run_pdb2pqr(string pdb_path) const
{
  string pqr_path = extract_filename_from_path(pdb_path);
  pqr_path = TEMPDIR_ + change_extension(pqr_path, string("pqr"));
  PDB2PQR_WRAP pdb2pqr_wrap (PDB2PQR_, pdb_path, pqr_path, pH_);
  pdb2pqr_wrap.run();
  return pqr_path;
} // end FullRun::run_pdb2pqr

/*#########################################################*/
//  run_msms
// Calculate surface mesh of pqr file
/*#########################################################*/
string FullRun::run_msms(string pqr_path) const
{
  string xyzr_path = change_extension(pqr_path, "xyzr");
  string out_name = change_extension(pqr_path, "p1.5d3");
  MSMS_WRAP msms_wrap (MSMS_, SCRIPTSDIR_, pqr_path, xyzr_path, out_name);
  string vert_path = msms_wrap.run();
  return vert_path;
} // end FullRun::run_msms

/*#########################################################*/
// run_makesphere
// Run the CG-MC method makesphere
/*#########################################################*/
string FullRun::run_makesphere(string pqr_path, string vert_path,
                  double tol_sp, bool make_contacts,
                  string conpdb_path, double tol_ip
                  ) const
{
  ostringstream t_sp;
  t_sp << tol_sp;
  string cent_str = "centers.sp" + t_sp.str(); 
  string cent_path = change_extension(vert_path, cent_str);
  
  mainMakesphere(pqr_path.c_str(), vert_path.c_str(),
           cent_path.c_str(), tol_sp, make_contacts,
           conpdb_path.c_str(), tol_ip);
  
  return cent_path;
}// end FullRun::run_makesphere

/*#########################################################*/
//  run_cen2pqr
// Run a perl file that converts a center file to a PQR
/*#########################################################*/
string FullRun::run_cen2pqr(string centpath)
{
  string pqr_path = centpath + ".pqr";
  string command = HOMEDIR_ + "/scripts/cen2pqr.pl " + centpath + 
                    " > " + pqr_path;
  cout << command << endl;
  system(command.c_str());
  return pqr_path;
} // end FullRun:: run_cen2pqr


/*#########################################################*/
//  cat_pqr_files
// Concatenate the CG and all atom versions of the PQRs
/*#########################################################*/
string FullRun::cat_pqr_files(string centers_pqr, string all_atom_pqr,
                              double tol_sp) const
{
  ostringstream t_sp;
  t_sp << tol_sp;
  string all_ext = "sp" + t_sp.str() + ".all.pqr";
  string out_file =change_extension(all_atom_pqr, all_ext);
  string command = "cat "  + centers_pqr + " " + all_atom_pqr + 
                   " > " + out_file;
  system(command.c_str());
  cout << "\nConcatenated centers and all atom pqr files into " <<  endl;
  cout << out_file << endl;
  return out_file;
} // end FullRun::cat_pqr_files

/*#########################################################*/
// run_find_contacts
// Program to read in an All atom list of contact molecules
// and map it to the CG molecules
/*#########################################################*/
string FullRun::run_find_contacts(string centers_pqr1, string centers_pqr2, 
                  string contact_file,
                  string allatom_pqr1, string allatom_pqr2) const
{
  string outfile_name = change_extension(allatom_pqr1, "contact_spheres.out");
  mainFindContacts(centers_pqr1.c_str(), centers_pqr2.c_str(),
                     contact_file.c_str(), allatom_pqr1.c_str(),
                     allatom_pqr2.c_str(), outfile_name.c_str());
  return outfile_name; 
} // end FullRun:run_find_contacts
