//
//  msms_wrap.h
//  all
//
//  Created by David Brookes on 8/24/15.
//  Copyright (c) 2015 David Brookes. All rights reserved.
//

#ifndef __all__msms_wrap__
#define __all__msms_wrap__

#include <stdio.h>
#include <string>
#include <fstream>
#include <cstdlib>
#include <iostream>
#include <sstream>  


using namespace std;

class MSMS_WRAP
{
protected:
  
  string EXE_PATH;
  string pqrPath_;
  string xyzrPath_;
  string scriptsPath_;
//  string outName_;
  string vertPath_;
  double probeRadius_;
  double density_;
  double hdensity_;
  string command_;
  
  const string generate_xyzr() const;
  
public:
  MSMS_WRAP(const string& exe_path,
            const string& scripts_path,
            const string& pqr_path,
            const string& xyzr_path,
            const string& out_name,
            const double probe_radius=1.5,
            const double density=3.0,
            const double hdensity=3.0
            );
  
  const string run() const;
  
};

#endif /* defined(__all__msms_wrap__) */
