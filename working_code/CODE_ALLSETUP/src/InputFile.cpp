//
//  InputFile.cpp
//  makespheres
//
//  Created by David Brookes on 8/25/15.
//  Copyright (c) 2015 David Brookes. All rights reserved.
//

#include "InputFile.h"

double DEFAULT_TOLSP = 5;
double DEFAULT_TOLIP = 1;

/*#########################################################*/
//  Constructor for BaseInputFile
/*#########################################################*/
BaseInputFile::BaseInputFile(string path)
:path_(path)
{
}

/*#########################################################*/
//  read
// function to read a file, search for keywords
/*#########################################################*/
void BaseInputFile::read()
{
  
  FILE* file = fopen(path_.c_str(), "r"); /* should check the result */
  cout << "\nReading input file: " << path_.c_str() << endl;
  
  char line[256];
  char delim = ' ';
  vector<string> tokens;
  string key, val, s;
  char* line_str = (char*)malloc(256);

  while (fgets(line, sizeof(line), file))
  {
    
    line_str = &strtok(line, "\n")[0]; // remove new line
    if (line_str == NULL) continue;
    vector<string> tokens = line_tokenize(line_str, delim);
    key = tokens[0];
    val = tokens[1];
    for(int i = 0; i < KEYWORDS.size(); i++)
    {
      s = KEYWORDS[i];
      if (key == s) search_keywords(s, val);
    }
  }
  fclose(file);
} // end BaseInputFile::read

/*#########################################################*/
//  search_keywords
// Reads a string and searches for keyword in it
/*#########################################################*/
void AllInputFile::search_keywords(string s, string val)
{
    if (s == KEYWORDS[0])
    {
      cout << "Homedir command found" << endl;
      homedir_ = strip_quotes(val);
      home_sat = true;
    }
    else if (s == KEYWORDS[1])
    {
      cout << "PDB command found" << endl;
      pdb_ = strip_quotes(val);
      in_sat = true;
    }
    else if (s == KEYWORDS[2])
    {
      cout << "PQR command found" << endl;
      pqr_ = strip_quotes(val);
      in_sat = true;
      makePQR_ = false;
    }
    else if (s == KEYWORDS[4])
    {
      cout << "Contact PDB command found" << endl;
      conpdb_ = strip_quotes(val);
      con_sat = true;
    }
    else if (s == KEYWORDS[5])
    {
      cout << "Contact PQR command found" << endl;
      conpqr_ = strip_quotes(val);
      con_sat = true;
      makeConPQR_ = false;
    }
    else if (s == KEYWORDS[7])
    {
      cout << "MSMS executable command found" << endl;
      msmsName_ = strip_quotes(val);
      msms_sat = true;
    }
    else if (s == KEYWORDS[10])
    {
      cout << "Atom contact list command found" << endl;
      atomContactFile_ = strip_quotes(val);
      atom_con_sat = true;
    }
    else if (s == KEYWORDS[3])
    {
      cout << "Make contacts command found" << endl;
      if (val == "1" || val=="True" || val=="true") makeContacts_ = true;
      else makeContacts_ = false;
    }
    else if (s == KEYWORDS[6]) 
    {
      cout << "pH command found" << endl;
      pH_ = strtod(val.c_str(), NULL);
    }
    else if (s == KEYWORDS[8]) 
    {
      cout << "Sphere tolerance command found" << endl;
      tolSP_ = strtod(val.c_str(), NULL);
    }
    else if (s == KEYWORDS[9]) 
    {
      cout << "Interface tolerance command found" << endl;
      tolIP_ = strtod(val.c_str(), NULL);
    }
    else if (s == KEYWORDS[11]) 
    {
      cout << "Temp file dir command found" << endl;
      tempFileDir_ = strip_quotes(val);
    }
} // end AllInputFile::search_keywords

/*#########################################################*/
// AllInputFile printVals
// Prints the current values of the AllInputFile object
/*#########################################################*/
void AllInputFile::printVals()
{
  cout << endl;
  cout << "Printing values of system variables" << endl;
  cout << "pH: " << pH_ << " tolSp: " << tolSP_ << " tolIP: " << tolIP_ <<endl;
  cout << " Temp file dir: " << tempFileDir_ << " Make PQR? " << makePQR_ <<
          " Make contacts? " << makeContacts_ << " Make Con PQR? " << 
            makeConPQR_ << endl;
  if (home_sat) cout << "Home dir: " << homedir_ << endl;
  if (in_sat) cout << "PDB file: " << pdb_ << endl;
  if (atom_con_sat) cout << "Atom contact file: " << atomContactFile_ << endl;
  if (msms_sat) cout << "MSMS executable: " << msmsName_ << endl;
  cout << endl;
} // end AllInputFile::printVals

/*#########################################################*/
// AllInputFile constructor
// identifies list of accepted keywords, and their default
// values
/*#########################################################*/
AllInputFile::AllInputFile(string path)
:BaseInputFile(path)
{
  KEYWORDS.push_back("homedir");//"HOMEDIR");
  KEYWORDS.push_back("pdb");//"PDB");
  KEYWORDS.push_back("pqr");//"PQR");
  KEYWORDS.push_back("makecon");//"MAKECON");
  KEYWORDS.push_back("conpdb");//"CONPDB");
  KEYWORDS.push_back("conpqr");//"CONPQR");
  KEYWORDS.push_back("ph");//"PH");
  KEYWORDS.push_back("msms_name");//"MSMS_NAME");
  KEYWORDS.push_back("tolsp");//"TOLSP");
  KEYWORDS.push_back("tolip");//"TOLIP");
  KEYWORDS.push_back("atomcon");//"ATOMCON");
  KEYWORDS.push_back("outfiledir");//"OUTFILEDIR");
  homedir_ = "../../src";
  pH_=8; //default
  msmsName_ = "msms.linux";
  tempFileDir_ = "None";
  home_sat = true;
  in_sat = false;
  con_sat = false;
  msms_sat = true;
  atom_con_sat = false;
  makeContacts_ = false;
  makePQR_ = true;
  makeConPQR_ = true;
  
  tolSP_ = DEFAULT_TOLSP;
  tolIP_ = DEFAULT_TOLIP;
  
  read();
  
  if (tempFileDir_ == "None") tempFileDir_ = homedir_;
  else if (!in_sat) 
    throw NotEnoughInputsException("need an input .pdb or .pqr file with the \
    keywords PDB or PQR");
  else if (makeContacts_ && !con_sat)
    throw NotEnoughInputsException("must input a contact pqr or pdb file \
    (keywords CONPDB or CONPQR) or set MAKECON as false");
  else if (makeContacts_ && !atom_con_sat)
    throw NotEnoughInputsException("must input an atom contact list file \
    (keyword ATOMCON) or set MAKECON as false");
  
}// end AllInputFile::AllInputFile

