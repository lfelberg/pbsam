//
//  FullRun.h
//  all
//
//  Created by David Brookes on 8/27/15.
//  Copyright (c) 2015 David Brookes. All rights reserved.
//

#ifndef __all__FullRun__
#define __all__FullRun__

#include <stdio.h>
#include <string>
#include <vector>
#include "util.h"
#include "pdb2pqr_wrap.h"
#include "msms_wrap.h"
#include "makesphere.cpp"
#include "findContacts.cpp"


using namespace std;

/*#########################################################*/
//  Full run class, calls functions/objects that run all
// parts of PBSAM CG 
/*#########################################################*/
class FullRun
{
protected:
    
    string HOMEDIR_; //path to $PBSAMHOME
    
    string MSMS_; //path to msms executable
    string PDB2PQR_; //path to .py file
    string TEMPDIR_; //path to dir where intermed files are (e.g. .xyzr files)
    string SCRIPTSDIR_; //path to dir where scripts are (e.g. pqr_to_xyzr.py)
    string msmsName_; //name of msms executable
    double pH_;
    
public:
    FullRun(string homedir, string msms_name, string tempdir, double pH);
    
    //runs pdb2pqr.py and returns path to .pqr file
    string run_pdb2pqr(string pdb_path) const;
    
    //runs MSMS and returns path to .vert file
    string run_msms(string pqr_path) const;
    
    //runs makesphere and returns path to output pqr file
    string run_makesphere(string pqr_path, string vert_path,
                          double tol_sp, bool make_contacts=false,
                          string conpdb_path = "", double tol_ip=-1
                          ) const;
    
    //converts a makespheres out file to pqr using scripts/cen2pqr.pl script
    string run_cen2pqr(string centpath);
    
    //concatenates the center and all-atom pqr files and returns path to file
    string cat_pqr_files(string centers_pqr, string all_atom_pqr,
                         double tol_sp) const;
    
    //runs the findContacts programs and returns the path to the outout file
    string run_find_contacts(string centers_pqr1, string centers_pqr2, 
                             string contact_file,
                             string allatom_pqr1, string allatom_pqr2) const;
    
    
};

#endif /* defined(__all__FullRun__) */
