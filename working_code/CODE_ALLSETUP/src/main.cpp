//
//  main.cpp
//  all
//
//  Created by David Brookes on 8/24/15.
//  Copyright (c) 2015 David Brookes. All rights reserved.
//

#include <iostream>
#include "pdb2pqr_wrap.h"
#include "msms_wrap.h"
#include "util.h"
#include "InputFile.h"
#include "FullRun.h"

using namespace::std;

/*#########################################################*/
//  mainFullRun
// reads in a input file and runs all initialization steps
// of the coarse-graining procedure for PBSAM
// this includes: PDB2PQR, MSMS, Makesphere and FindContacts
/*#########################################################*/
int mainFullRun(int argc, const char * argv[])
{
  // read input file
  string infile_path = argv[1];
  AllInputFile infile (infile_path);
  infile.printVals();
  FullRun full_run (infile.get_homedir(), infile.get_msms_name(), 
                    infile.get_tempfile_dir(), infile.get_ph());
  string pqr_path;
  
  // if we have PDB input, make PQR with PDB2PQR
  if (infile.get_make_pqr())
    pqr_path = full_run.run_pdb2pqr(infile.get_pdb());
  else
    pqr_path = infile.get_pqr();
  
  // Run msms
  string vert_path = full_run.run_msms(pqr_path);
  string cent_path;
  string cen_pqr_path;

  // If flag is wanted to make contacts, read in other
  // molecule, CG it and make a list of contacts
  if (infile.get_make_contacts())
  {
    string conpqr_path;
    if (infile.get_make_conpqr())
      conpqr_path = full_run.run_pdb2pqr(infile.get_conpdb());
    else
      conpqr_path = infile.get_conpqr();
    
    cent_path = full_run.run_makesphere(pqr_path, vert_path,
                                        infile.get_tolsp(), true,
                                        conpqr_path, infile.get_tolip());
    
    string con_vert_path = full_run.run_msms(conpqr_path);
    string con_cent_path = full_run.run_makesphere(conpqr_path, con_vert_path,
                                                infile.get_tolsp(), true,
                                                pqr_path, infile.get_tolip());
    
    cen_pqr_path = full_run.run_cen2pqr(cent_path);
    string con_cen_pqr_path = full_run.run_cen2pqr(con_cent_path);
    
    full_run.run_find_contacts(cen_pqr_path, con_cen_pqr_path,
                               infile.get_atomcon_file(),
                               pqr_path, conpqr_path);
    
    full_run.cat_pqr_files(con_cen_pqr_path, conpqr_path, infile.get_tolsp());
    
  }
  else
  {
    cent_path = full_run.run_makesphere(pqr_path, vert_path, 
                                        infile.get_tolsp());
    cen_pqr_path = full_run.run_cen2pqr(cent_path);
  }
  full_run.cat_pqr_files(cen_pqr_path, pqr_path, infile.get_tolsp());
  
  return 0;
} // end mainFullRun


/*#########################################################*/
//  MAIN!!
/*#########################################################*/
int main(int argc, const char * argv[])
{
    mainFullRun(argc, argv);
    return 0;
} // end main
