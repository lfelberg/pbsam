//
//  InputFile.h
//  makespheres
//
//  Created by David Brookes on 8/25/15.
//  Copyright (c) 2015 David Brookes. All rights reserved.
//

#ifndef __makespheres__InputFile__
#define __makespheres__InputFile__

#include <stdio.h>
#include <string>
#include <vector>
#include "util.h"
#include <sstream>
#include <algorithm>
#include <iterator>
#include <iostream>
#include <cstring>
#include "readutil.h"

using namespace std;

class BaseInputFile
{
protected:
  class NotEnoughInputsException : public std::exception
  {
    public:
    string message_;
    NotEnoughInputsException(string message=" ")
    :message_(message)  {  }
  
    virtual const char* what() const throw()
    {
      string full = "Not enough inputs: " + message_;
      return full.c_str();
    }
    virtual ~NotEnoughInputsException() throw() {}
  };
  
  string path_;
  vector<string> KEYWORDS;
  
  void read();
  virtual void search_keywords(string s, string val)=0;
//    vector<string> line_tokenize(string line, char delim);
  
  BaseInputFile(string path);
  
  const std::string& get_path() const { return path_; }
}; // end BaseInputFile


class AllInputFile : public BaseInputFile
{
    
protected:
  
  string homedir_; //path to $PBSAMHOME
  string pdb_;
  string pqr_; //if the pqr is pre-made
  bool makeContacts_; //true if this is a contact system
  bool makePQR_; //true if pdb2pqr needs to be run
  bool makeConPQR_;
  string conpdb_; //contact pdb file (if conpqr_ is not defined)
  string conpqr_; //contact pqr file
  double pH_;
  string msmsName_; //name of msms executable (for different operating systems)
  double tolSP_;
  double tolIP_;
  string atomContactFile_; //path to the file containing atom-atom contact IDs
  string tempFileDir_; //dir to store intermed files from progs. Def: homedir_
  
  bool home_sat; //homedir condition satisfied
  bool in_sat; //input file condition satisfied
  bool con_sat; //contact file conditions satisfied
  bool msms_sat; //msms extension conditions satisfied
  bool tolsp_sat;
  bool atom_con_sat;
  
public:
  
  AllInputFile(string path);
  void search_keywords(string s, string val);
  void printVals();
  
  const string& get_homedir() const           { return homedir_ ;         }
  const string& get_pdb() const               { return pdb_;              }
  const string& get_pqr() const               { return pqr_;              }
  const string& get_conpdb() const            { return conpdb_;           }
  const bool get_make_contacts() const        { return makeContacts_;     }
  const bool get_make_pqr() const             { return makePQR_;          }
  const string& get_conpqr() const            { return conpqr_;           }
  const double get_ph() const                 { return pH_;               }
  const string& get_msms_name() const         { return msmsName_;         }
  const bool get_make_conpqr() const          { return makeConPQR_;       }
  const double get_tolsp() const              { return tolSP_;            }
  const double get_tolip() const              { return tolIP_;            }
  const string& get_atomcon_file() const      { return atomContactFile_;  }
  const string& get_tempfile_dir() const      { return tempFileDir_;      }
  
}; // end AllInputFile

#endif /* defined(__makespheres__InputFile__) */

