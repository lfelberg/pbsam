//
//  msms_wrap.cpp
//  all
//
//  Created by David Brookes on 8/24/15.
//  Copyright (c) 2015 David Brookes. All rights reserved.
//

#include "msms_wrap.h"

using namespace std;

MSMS_WRAP::MSMS_WRAP(const string& exe_path,
                     const string& scripts_path,
                     const string& pqr_path,
                     const string& xyzr_path,
                     const string& out_name,
                     const double probe_radius,
                     const double density,
                     const double hdensity
                     )
:EXE_PATH(exe_path), pqrPath_(pqr_path), 
probeRadius_(probe_radius), density_(density),
hdensity_(hdensity), xyzrPath_(xyzr_path), scriptsPath_(scripts_path)
{
  vertPath_ = out_name + ".vert";
  
  ostringstream strs;
  strs << probeRadius_;
  std::string str1 = strs.str();
  strs.clear();
  strs << density_;
  std::string str2 = strs.str();
  strs.clear();
  strs << hdensity_;
  std::string str3 = strs.str();
  
  command_ = EXE_PATH + " -if " + xyzrPath_;
  command_ += " -probe_radius " + str1;
  command_ += " -density " + str2;
  command_ += " -hdensity " + str3;
  command_ += " -no_area -of " + out_name;
}

const string MSMS_WRAP::generate_xyzr() const
{
  string gen_command = "python " + scriptsPath_ + "pqr_to_xyzr.py ";
  gen_command += pqrPath_ + " > " + xyzrPath_;
  system(gen_command.c_str());
  return xyzrPath_;
}

const string MSMS_WRAP::run() const
{
  generate_xyzr();
  system(command_.c_str());
  return vertPath_;
}
