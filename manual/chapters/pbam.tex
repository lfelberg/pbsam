
\chapter{Poisson-Boltzmann Analytical Model (PB-AM)}


\section{The Model}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{PB-AM formulation}

PB-AM is an analytical solution to the linearized Poisson-Boltzmann equations 
for multiple spherical objects of arbitrary charge distribution in an ionic solution.  
Exploiting fast-multipole methods, 
the boundary value problem described in Section~\ref{sec:lpbe} 
can be reduced to the following system of linear equations.  

\begin{equation}
% Lotan 2006 Eqn 21
	\label{eq:pbam_solve}
	 \underline{A} = \underline{\underline{\Gamma}} 
	 	\cdot (\underline{\underline{\Delta}} \cdot \underline{\underline{T}} \cdot \underline{A} + \underline{E})
\end{equation}

$\underline{A}$ represents a vector of the effective multipole expansion of the charge distributions of each molecule. 
$\underline{E}$ is a vector of the fixed charge distribution of all molecules. 
$\underline{\underline{\Gamma}}$ is a dielectric boundary-crossing operator. 
$\underline{\underline{\Delta}}$ is a cavity polarization operator. 
$\underline{\underline{T}}$ an operator that transforms the multipole expansion 
from the global (lab) coordinates to a local coordinate frame.  
More details on each of these matrices is given below (Section~\ref{sec:matricesPBAM}).
More details on the method are available in Lotan and Head-Gordon (2006).\cite{Lotan2006}

From this formulation, computation of the interaction energies ($\Omega^{(i)}$) is given as follows:

\begin{equation}
% Lotan 2006 Eqn 28
	\label{eq:energy_pbam}
	\Omega^{(i)}=\frac{1}{\epsilon_s}  \langle  \underline{\underline{T}} \cdot \underline{A}^{(i)} ,  \underline{A}^{(i)} \rangle
\end{equation}

Where $\langle  M, N \rangle$ denotes the inner product. 
When energy is computed, forces follow as:

\begin{equation}
% Lotan 2006 Eqn 37, with correction of minus sign
	\label{eq:force}
	\underline{F_i} = - \nabla_i \Omega^{(i)} = -\frac{1}{\epsilon_s} [ \langle \nabla_i \,\underline{\underline{T}} \cdot \underline{A}^{(i)}  ,  \underline{A}^{(i)}  \rangle +  \langle \underline{\underline{T}} \cdot \underline{A}^{(i) } ,   \nabla_i \, \underline{A}^{(i) } \rangle ]
\end{equation}

\clearpage



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{PB-AM algorithm}

The goal of PB-AM is to solve equation \ref{eq:pbam_solve}, 
for the unknown effective multipole expansion vector, \underline{A}. 
The implementation of this is as follows: \\

\begin{itemize}
\item[1.     ]  Precompute $\underline{\underline{\Gamma}}$, $\underline{\underline{\Delta}}$, 
			and $\underline{\underline{T}}$.
\item[2.     ]  Compute initial guess of $\underline{A}$, $\underline{A}_{t=0}$, 
			using the following: $\underline{A}_{t=0} = \underline{\underline{\Gamma}} \cdot \underline{E} $
\item[3.     ]  Using this initial guess, $\underline{A}_{t=0}$, recompute $\underline{A}$ iteratively:

				\begin{equation}
					\label{eqn:selfconsistent}
					 \underline{A}_{t=1} = \underline{\underline{\Gamma}} \cdot (\underline{\underline{\Delta}} \cdot 						\underline{\underline{T}} \cdot \underline{A}_{t=0} +  \underline{E} )
				\end{equation}

\item[4.     ]  Repeat step 3 until the difference between $\underline{A}_{t}$ and $\underline{A}_{t-1}$ 
			is below an acceptable tolerance. \\
\end{itemize} 

The function that computes the difference between the current iteration 
of \underline{A} and the previous is  \texttt{CMCoeff::computeDev}, 
which is called from \texttt{CMPE::recompute}. \\ \\


The structure of each component of equation \ref{eq:pbam_solve} is as follows:
%\subsubsection{Within the code}
\label{sec:matricesPBAM}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsubsection{ \underline{A} and \underline{E} vectors}
$\underline{A}$ is a vector of vectors with dimension $N$, 
where $N$ is the total number of molecules:

\begin{equation}
% % Lotan 2006 Eqn 22
	\label{eq:a_details}
	\underline{A} = \left[ \begin{array}{c}
						\underline{A}^{(1)} \\
						\underline{A}^{(2)}\\
						...\\
						\underline{A}^{(N)}  \end{array} \right]
\end{equation}

There is a matrix \(\underline{A}^{(i)}\) for each molecule that represents a set of coefficients for the series: 

\begin{equation}
	\label{eq:a_ser}
	\sum_{n=0}^{n<P} \sum_{m=-n}^{m=n} A^{(i)}_{m,n}
\end{equation}

Therefore, \(\underline{A}^{(i)}\) has dimensions $(P-1) \times (2P-1)$,
where $P$ is the maximum multipole order.

\begin{equation}
	\label{eq:ai_details}
	\underline{A}^{(i)} = \left[ \begin{array}{cccccccc}
			\underline{A}^{(i)}_{(n=0,m=0)} & 0 & 0 & ... & & & ... & 0 \\
			\underline{A}^{(i)}_{(n=1,m=-1)} & \underline{A}^{(i)}_{(n=1,m=0)} & \underline{A}^{(i)}_{(n=1,m=1)} & 0 & ... & & ... & 0 \\
			\underline{A}^{(i)}_{(n=2,m=-2)} & \underline{A}^{(i)}_{(n=2,m=-1)} & \underline{A}^{(i)}_{(n=2,m=0)} & \underline{A}^{(i)}_{(n=2,m=1)} & \underline{A}^{(i)}_{(n=2,m=2)} & 0 & ... & 0 \\
... & & & & & & & ... \\
			\underline{A}^{(i)}_{(n=P-1,m=P-1)} & ... & & & & & ... &\underline{A}^{(i)}_{(n=P-1,m=P-1)} \\\end{array} \right]
\end{equation}

The same format represents \underline{E} and its constituent vectors: \(\underline{E}^{(i)}\). \\




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsubsection{ \(\underline{\underline{\Gamma}}\) and \(\underline{\underline{\Delta}}\) matrices}

\(\underline{\underline{\Gamma}}\) and \(\underline{\underline{\Delta}}\) are diagonal matrices 
of matrices with dimension $ N \times N $:

\begin{equation}
% Lotan 2006 Eqn 22
	\label{eq:gam_details}
	\underline{\underline{\Gamma}} = \left[ \begin{array}{cccc}
			\underline{\underline{\Gamma}}^{(1)} & 0 & ... & 0\\
			0 & \underline{\underline{\Gamma}}^{(2)} &  ... & 0 \\
			... & ... &  ... & ... \\
			0 &  ... & 0 & \underline{\underline{\Gamma}}^{(N)} \\ \end{array} \right]
\end{equation}

Each element $\underline{\underline{\Gamma}}^{(i)}$, is a diagonal matrix with dimensions $ P \times P $ 
for each molecule that represents a set of coefficients for the series: 

\begin{equation}
% Lotan 2006 Eqn 23
	\label{eq:gami_details}
	\underline{\underline{\Gamma}}^{(i)} = \left[ \begin{array}{cccc}
 \gamma^{(i)}_{n=0} & 0 & ... & 0\\
0 & \gamma^{(i)}_{n=1} &  ... & 0 \\
0 & ... &  ... & 0 \\
0 &  ... & 0 & \gamma^{(i)}_{n=P-1}  \end{array} \right]
\end{equation}


%\begin{equation}
%	\label{eq:gam_ser}
%	\sum_{n=0}^{n<P} \gamma^{(i)}_{n}
%\end{equation}

And each \(\gamma^{(i)}_{n}\) is of the form:

\begin{equation}
% Lotan 2006 Eqn 19
	\label{eq:gam_little}
	\gamma^{(i)}_{n} = \frac{(2n+1)e^{\kappa a_i}}{(2n+1) \hat{k}_{n+1}(\kappa a_i) + n \hat{k}_n (\kappa a_i) (\frac{\epsilon_p}{\epsilon_s} -1)}
\end{equation}



The same format represents $\underline{\underline{\Delta}}$ and its constituent vectors $\underline{\underline{\Delta}}^{(i)}$. Each $\delta^{(i)}_{n}$ is of the form:

\begin{equation}
% Lotan 2006 Eqn 20
	\label{eq:delta_little}
	\delta^{(i)}_{n} = \frac{a^{2n+1}_i}{2n+1} \left [ \kappa^2 a^2_i \left(\frac{\hat{i}_{n+1}(\kappa a_i) }{2n+3} \right) + n \, \hat{i}_n(\kappa a_i) \left(1-\frac{\epsilon_p}{\epsilon_s}\right) \right]
\end{equation}




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsubsection{ \underline{\underline{T}} matrix}
\label{sec:reexpan}

The \underline{\underline{T}} matrix is an off-diagonal matrix of dimension $ N \times N $. 
It has the following coefficients:

\begin{equation}
% Lotan 2006 Eqn 22
	\label{eq:T_details}
	\underline{\underline{T}} = \left[ \begin{array}{cccc}
0 & \underline{\underline{T}}^{(1,2)} & ... & \underline{\underline{T}}^{(1,N)}\\
\underline{\underline{T}}^{(2,1)} & 0 &  ... & ... \\
... & ... &  ... & ... \\
... & ... &  0 & \underline{\underline{T}}^{(N-1,N)}  \\
 \underline{\underline{T}}^{(N,1)} &  ... &  \underline{\underline{T}}^{(N,N-1)} & 0\\ \end{array} \right]
\end{equation}

Each of the terms within this matrix are linear re-expansion operators, $ T^{(i,j)} $ that transform the multipole expansion at one molecule (j) in the system to the local expansion at a different molecule (i). It is computed as follows:

\begin{equation}
% Lotan 2006 Eqn 44
	\label{eq:t_ij_def}
	T^{(i,j)} = ( R^{(i,j)} )^H \cdot   S^{(i,j)}  \cdot   R^{(i,j)}  
\end{equation}

Where $S^{(i,j)}$  is a re-expansion along the Z axis, and 
$R^{(i,j)}$ is a rotation operator that orients the desired direction of re-expansion with the Z axis. 
The result of $\underline{\underline{T}} \cdot \underline{A} $ is $\underline{L}$, 
which has the same dimensions of  $\underline{A}$.
For each molecule $i$ :

\begin{equation}
% Lotan 2006 Eqn 16
	\label{eq:t_ij_a_j_prod}
	\underline{L}^{(i)}  = \sum_{j=1, j\ne i}^{j = N} T^{(i,j)} \cdot \underline{A}^{(i)} 
\end{equation}

Instead of calculating $T^{(i,j)} \cdot \underline{A}^{(i)}$, 
this is implemented using the following decomposition with $ S^{(i,j)} $ and $ R^{(i,j)} $ :

\begin{equation}
% Lotan 2006 Eqn 46
	\label{eq:t_ij_a_j_prod}
	L^{(i)}_{n,m} =   \sum_{s=-n}^{s=n} \overline{R}_{n,s,m} \, 
					\sum_{l=|m|}^{l=P} S_{n,m,l} \,  	
					\sum_{s=-n}^{s=n} R_{n,m,s} \, A^{(i)}_{n,s}
\end{equation}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{ S and R Re-expansion Operators}

The reexpansion operators are a form of coordinate transform.
They take a multipole expansion from the global coordinate frame 
and alter it to the local coordinate frame centered at a particular molecule $i$. 
The reexpansions are expressed as functions of the translation vector

\begin{equation}
	\label{eqn:translationVector}
	\bold{v}^{(i,j)} = \bold{c}^{(i)} - \bold{c}^{(j)} = [ r, \theta, \phi ] 
\end{equation}

from the center of molecule $j$ to the center of the molecule of interest $i$.
The equations for the rotational ($R$) and translational ($S$) re-expansion operators
are adapted from Gumerov and Duraswami. \cite{Gumerov2004}

The following is Eqn.~5.54 of Gumerov and Duraiswami :
\begin{equation}
% Gumerov 2004 Eqn 5.54
	\label{eqn:gd5.54}
	T^{v,m+1}_{n-1} = \frac{ e^{ \imath \chi} }{ b_n^m } 
				\Big\{ \frac{1}{2} \Big[ b^{-v-1}_n e^{ \imath \psi^{\prime} } ( 1 - \cos{ \theta^{\prime} } ) T^{v+1,m}_n
				- b^{v-1}_n ( 1 + \cos{ \theta^{\prime} } ) e^{ -\imath \psi^{\prime}} T^{v-1,m}_n \Big] 
				- a^v_{n-1} ( \sin{ \theta^{\prime} } ) T^{v,m}_n \Big\}
\end{equation}

Assuming $ \chi = 0 $, a slight changes in notation 
(from $T$ to $R$, $v$ to $s$, $a_{n-1}$ to $a_n$, removal of prime signs, $\psi$ to $\phi$; 
Note also the reversal of indices and the change in sign.) 
bring us directly to Eqn.~1.1 of Lotan and Head-Gordon 2006. 

\begin{equation}
% Lotan 2006 Eqn 1.1
	\label{eqn:lotan1.1}
	R^{m+1,s}_{n-1} = \frac{ 1 }{ b_n^m } 
				\Big\{ \frac{1}{2} \Big[ - b^{-s-1}_n e^{ \imath \phi^{\prime} } ( 1 - \cos{ \theta } ) R^{m,s+1}_n
				+ b^{s-1}_n ( 1 + \cos{ \theta } ) e^{ -\imath \phi} R^{m,s-1}_n \Big] 
				+ a^s_{n} ( \sin{ \theta^{\prime} } ) R^{m,s}_n \Big\}
\end{equation}

For $ m < 0 $, $ R^{m,s}_n =  \overline{ R^{-m,-s}_n } $. \\

The constants $a_n^m$ and $b_n^m$ are taken directly 
from Gumerov and Duraiswami Eqn.s 3.3 and 3.5, respectively.

\begin{equation}
% Lotan 2006 Eqn 1.2 ; Gumerov 2004 Eqn 3.3
	\label{eqn:anm}
	a_n^m = \sqrt{ \frac{ ( n + m + 1 ) ( n - m + 1 ) }{ ( 2n + 1 ) ( 2n + 3 ) } }
\end{equation}

\begin{equation}
% Lotan 2006 Eqn 1.2 ; Gumerov 2004 Eqn 3.5
	\label{eqn:bnm}
	b^m_n = \text{sign}(m)  \sqrt{ \frac{ ( n - m - 1 ) ( n - m ) }{ ( 2n - 1 ) ( 2n + 1 ) } }
\end{equation}

where sign is whether $m$ is positive or negative (not a typo of sin).

To compute $R^{m,s}_n$ recursively,
we start with 

\begin{equation}
% Lotan 2006 just before Eqn 1.1
	\label{eqn:R_m0}
	R^{0,s}_n = Y_{n,-s} ( \theta , \phi )
\end{equation}

where $Y_{n,-s} ( \theta , \phi )$ are the spherical harmonics defined in Eqn. ?? 
Because we only know $R^{0,s}_n$, and $R$ is calculated by decrementing $n$, 
to fill a reexpansion matrix of dimension $R^{m,s}_P$,
we must start with $R^{0,s}_{2P-1}$, 
even though only the matrices up to $R^{m,s}_{P}$
will be used in the calculation.

Starting from $R^{0,s}_{2P-1}$, 
we fill the matrix using the following recursion in Eqn.~\ref{eqn:lotan1.1}






The coefficients have the following ranges: \( R_{n,m,s} \) for \((0 \le n < P)\) and \((-n \le m,s \le n)\) and \( S_{n,l,m} \) for \((0 \le n,l < P)\) and \((-n \le m \le n)\). The details of this are given on page 553 of Lotan, 2006 \cite{Lotan2006}






\clearpage





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{PB-AM Installation}
\label{sec:installPBAM}

To install PB-AM from the source code, first pull the latest version from the github/bitbucket site and type the following into the command line: \\
\begin{lstlisting}[style = MyBash]
>> make pbam
\end{lstlisting}
This should make the executable, \texttt{pbam}, and place it in the \texttt{bin} directory of the source code

\clearpage


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Running PB-AM}

\subsection{PB-AM: Example input files and input file information}

From the single \texttt{pbam} executable, multiple types of calculations can be performed. Generally, all the programs require an input file containing a few keywords, and a PDB or PQR file name.  If a PDB file is chosen the input is read and atoms are assigned partial charges according to the AMBER force field. A PQR file can be generated from the online site or the software can be downloaded:  \\

http://nbcr-222.ucsd.edu/pdb2pqr\_1.9.0/  \\
http://www.poissonboltzmann.org/docs/pdb2pqr-installation/ \\

It may also be formatted manually. The general format of a PQR file is as follows, and is whitespace-delimited: 

\centerline{\textbf{recName  serial  atName  resName  chainID  resNum  X  Y  Z  charge rad } } 


\vspace{10pt}
  \begin{tabular}{ c | l  }
    \textbf{Delimiter} & \textbf{Description} \\ \hline
recName 	&	A string that should either be ATOM or HETATM. \\
serial 	&	An integer that provides the atom index \\
atName 	&	A string that provides the atom name.\\
resName	&	A string that provides the residue name. \\
chainID	&	An optional string that provides the chain ID of the atom.\\
residueNumber  & An integer that provides the residue index.\\
X Y Z	& Three floats that provide the atomic coordinates.\\
charge	& A float that provides the atomic charge (in electrons). \\
Rad		& A float that provides the atomic radius (in \AA…).\\
    \hline
  \end{tabular}

\subsubsection{Program runfile}

The inputs for the program vary depending on the type of computation that is desired. The \texttt{runtype} keyword indicates which type of computation is run. The following is a list of types currently available: \\

  \begin{tabular}{ c | p{0.85 \textwidth}  }
    \textbf{Keyword} & \textbf{Description} \\ \hline
3bd 	&	Runs a three body approximation and prints out computed forces, torques and energies. \\
cng 	&	Computes the effect of a third molecule on the interaction of free energy of two molecules placed equidistantly. \\
dif 	&	Executes the computation of the potential at points on a grid in space from -75 to +75.\\
inf	&	Executes the creation of an infinite grid approximation. \\
per	&	Perturbs the system, by small moves and rotations to compute changes in the energy, forces and torques.\\
pol    &      Executes a routine the compares the force and torque of identical molecules in a salty environment, with and without mutual polarization effects.\\
rad	&      Executes the self-polarizarion and potential calculation on a grid for a single CG molecule, positioned at (0,0,0).\\
sim	&      Executes a BD simulation. \\
slv    &      Computes the energies, forces and the torques on a system of multiple molecules, in a salty solution.\\
    \hline
  \end{tabular}


\subsubsection{3bd}

The required inputs for this run are the following keywords: \\

  \begin{tabular}{ c | p{0.65 \textwidth}  }
    \textbf{Keyword} & \textbf{Description} \\ \hline
\texttt{runtype} 				&  3bd \\
\texttt{atttypes }   		 		&  Right now can only handle 1 \\
\texttt{type }	[ ID \# ] [ Count ]    	&  The number of molecules in type [ID] \\
\texttt{type }	[ ID \# ] [ PQR ]    	&  PQR file location for type [ID] \\
\texttt{type} 	[ ID \# ] [ XYZ ]    	&  XYZ file location [ID] \\
    \hline
  \end{tabular}
  \vspace{10pt}
  
Additional possible keywords: \texttt{runname}, \texttt{salt}, \texttt{temp}, \texttt{idiel}, \texttt{sdiel}, \texttt{random}


\subsubsection{cng}

The required inputs for this run are the following keywords: \\

Executes a program that computes the effect of a third molecule on the interaction of free energy of two molecules placed equidistantly. It requires the following input:

		./exec cng [PQR file] [distance between molecles] [runname]
	The following files should be in the CWD: *pqr and charges\_OPLS

  And it outputs the following:
	[runname]\_dist.txt 
		This file has 900 lines, each line representing the interaction free energy between two molecules. The first value in each line represents the system of just two molecules, the next 30 values represents including a 3rd molecule in the system at 30 different orientations. \\

  \begin{tabular}{ c | p{0.65 \textwidth}  }
    \textbf{Keyword} & \textbf{Description} \\ \hline
\texttt{runtype} 				&  3bd \\
\texttt{atttypes }   		 		&  Right now can only handle 1 \\
\texttt{type }	[ ID \# ] [ Count ]    	&  The number of molecules in type [ID] \\
\texttt{type }	[ ID \# ] [ PQR ]    	&  PQR file location for type [ID] \\
\texttt{type} 	[ ID \# ] [ XYZ ]    	&  XYZ file location [ID] \\
    \hline
  \end{tabular}
  \vspace{10pt}
  
Additional possible keywords: \texttt{runname}, \texttt{salt}, \texttt{temp}, \texttt{idiel}, \texttt{sdiel}, \texttt{random}


\subsubsection{dif}

Executes the computation of the potential at points on a grid in space from -75 to +75, for a system of 2, 4, 6, or 8 molecules placed equidistantly in a salty solution. It requires the following input:

		./exec dif [PQR file] [2, 4, 6 or 8 molecules] [distance between molecules]
  The following files should be in the CWD: *pqr and charges\_OPLS

  And it outputs the following:
	pqrname\_[\# part]P.mdp
		A PQR file of file of molecules in their positions created by dif run with original OPLS partial charges
	pqrname\_[\# part]P\_sp.mdp
		Same as first PQR file, but with CG spheres included as well
	pqrname\_[\# part]P.gmpe
		A file of gridded potentials at 151 points for the X, Y and Z dimension.

\subsubsection{inf}

Executes the creation of an infinite grid approximation. It requires the following input:

		./exec inf [PQR file] [\# lattice layers] [stretch factor]
	  The following files should be in the CWD: *pqr and charges\_OPLS

  And it outputs the following:
	pqrname\_88.pdb 
		A PDB file of the input molecule with [\# lat lay] layers in a grid
	*.out
		The program prints out the potential, the XYZ force and the XYZ torque
		for the eight molecules placed at the center of the infinite grid


\subsubsection{per}



\subsubsection{pol}

Executes a routine the compares the force and torque of identical molecules places equidistantly in a salty environment, with and without mutual polarization effects. It requires the following input:

		./exec pol [PQR file] [2, 4, 6 or 8 molecules] [distance between molecules] [runname]
		The following files should be in the CWD: *pqr and charges\_OPLS

  And it outputs the following:

	polar\_[force/torque]\_name\_nmol\_dist.txt

The first line indicates the force or torque computed per molecule in the absence of mutual polarization. The next line includes mutual polarization. These 2 lines repeat 1000 times.


\subsubsection{rad}

Executes the self-polarizarion and potential calculation on a grid for a single CG molecule, positioned at (0,0,0).  Similar to dif option, but for only one molecule. It requires the following input:

		 ./exec rad [PQR file] [scaling factor]
		The following files should be in the CWD: *pqr and charges\_OPLS

  And it outputs the following:
	pqrname\_[\# part]P.mdp
			A PQR file of file of molecules in their positions created by dif run with
			original OPLS partial charges
	pqrname\_[\# part]P\_sp.mdp
			Same as first PQR file, but with CG spheres included as well
	pqrname\_[\# part]P.gmpe
			A file of gridded potentials at 151 points for the X, Y and Z dimension.

\subsubsection{sim}

Executes the BD simulation part of the code.  It requires the following input:
	
		./exec sim [Salt conc] [outfile] [temp file \#]
		The following files should be in the CWD: *pqr and charges\_OPLS

	And it outputs the following:

	temp[temp file \#].out 
		A temp file that outputs parameters while running each BD run

	[outfile]
		A File that contains the outcome of each BD run. the first 
		column is the status of the simulation at the end, the second
is the trajectory number and the last column is the step number

\subsubsection{slv}

Computes the energies, forces and the torques on a system of multiple molecules, placed equidistantly in a salty solution.  It requires the following input:

		./exec slv [PQR file] [2, 4, 6 or 8 molecules] [distance between molecules]
		The following files should be in the CWD: *pqr and charges\_OPLS

  And it outputs the following:		

	*.out 

A list of each molecule in the system and the energy, force and torque acting on it



\clearpage

