#!/bin/bash

rm ./chapters/*.aux
rm *.aux *.bbl *.bcf *.blg *.log *.out *.xml *.toc

pdflatex doc_main
biber doc_main
pdflatex doc_main
pdflatex doc_main
